<?php
            date_default_timezone_set("America/Sao_Paulo");
			setlocale(LC_ALL, 'pt_BR');
			
			include('conexao.php'); 
			if(isset($_POST['dataag'])){
				$data = $_POST['dataag'];	
			}else{
				$data = date('Y-m-d');	
			}
			
			$idsede = $_POST['idsede'];
			
			$espmed = "";
			if(isset($_POST['idmedico']) && $_POST['idmedico']!=''){ $espmed = 'AND id = '.$_POST['idmedico']; }
			$sql = "SELECT * FROM login WHERE tipo = 2 $espmed ORDER BY nome";
			//echo $sql;
            $resultado = mysqli_query($conexao, $sql);
            while($res = mysqli_fetch_assoc($resultado)){
                $idmed = $res['id'];
                $nome = $res['nome'];
        ?>
			<div class="agmedico" med="<?php echo $idmed; ?>">
            	<?php
				$hini = 6;
				$hfim = 24;
				$totalh = $hfim;
				$numh = $hini;
				$conta = 0;
				$qporconsulta = 4; //15 min
				while($conta < $qporconsulta && $numh < $totalh){
					$conta++;
					$minutoscons = 60 / $qporconsulta;
					$minini = ($conta - 1) * $minutoscons;
					$minfim = $conta * $minutoscons;
					$hini = $numh;
					$hfim = $numh;
					
					if($minfim == 60){ $hfim = $hini+1; $minfim = '00'; }
					if($minini == 0){ $minini = '00'; }
					
					if($hini < 10){ $hini = '0'.$hini; }
					if($hfim < 10){ $hfim = '0'.$hfim; }
					
					?>
                    <div class="minutosconsulta" horaini="<?php echo $hini; ?>" horafim="<?php echo $hfim; ?>" minini="<?php echo $minini; ?>" minfim="<?php echo $minfim; ?>" med="<?php echo $idmed; ?>"><div class="exibehm"><?php echo $hini . ":" . $minini ." - " . $hfim . ":" . $minfim; ?></div></div>
                    <?php
					if($conta==$qporconsulta){
						if($numh < $totalh){
							$numh++;	
						}
						$conta = 0;
					}
				}
				?>
            	<div class="nomemedico"><?php echo $nome; ?></div>
             	<?php
					if($_COOKIE['idusu'] == 38){ $addseg = " AND data >= date(now())"; }
					else{  $addseg = ""; }
					$sql2 = "SELECT cons.*, cli.nome as nomepaciente, (SELECT foto FROM fotos_pacientes WHERE id_paciente = cli.id AND foto != '' AND foto is not null order by id DESC limit 1) as fotopaciente FROM consulta as cons inner join clientes as cli on cons.id_cliente = cli.id WHERE id_medico = $idmed  AND data between '".$data." 00:00' and '".$data." 23:59' $addseg";
					//echo $sql2;
					//$sql2 = "SELECT cons.*, cli.nome as nomepaciente, (SELECT foto FROM fotos_pacientes WHERE id_paciente = cli.id AND foto != '' AND foto is not null order by id DESC limit 1) as fotopaciente FROM consulta as cons inner join clientes as cli on cons.id_cliente = cli.id WHERE id_medico = $idmed AND id_sede = ".$idsede."  AND data between '".$data." 00:00' and '".$data." 23:59'";
					$resultado2 = mysqli_query($conexao, $sql2);
					while($res2 = mysqli_fetch_assoc($resultado2)){
						$idcons = $res2['id'];
						$nomepaciente = $res2['nomepaciente'];
						$foto = $res2['fotopaciente'];
						$hini = $res2['hini'];
						$hfim = $res2['hfim'];
						$status = $res2['status'];
						$idpaciente = $res2['id_cliente'];
						$idsala = $res2['id_sala'];
						$parcelamento = $res2['parcelamento'];
						$forma_pagamento = $res2['forma_pagamento'];
						$nota = $res2['nota_fiscal'];
						$valor = $res2['valor'];
						$dinheiro = $res2['valor_dinheiro'];
						$cartao = $res2['valor_cartao'];
						$valormedico = $res2['valor_medico'];
						$valorclinica = $res2['valor_clinica'];
						$justificativa = $res2['justificativa'];
						$tipoconsulta = $res2['tipoconsulta'];
						$planosaude = $res2['plano_saude'];
						$obs = $res2['obs'];
						
						$escnome = "<div class='intconsulta intnome'>" . $nomepaciente . "</div>";
						$eschorario = "<div class='intconsulta' style='border:none;'>" . $hini . " - ". $hfim . "</div>";
						
						if(trim($foto)!=''){
							$urlfoto = 'upload/fotos/'.$foto;
						 }
						 else{
							$urlfoto = 'img/icones/persona_ico.png'; 
						 }
						 
						 if($tipoconsulta=='RETORNO'){
							$addtipo = "<span class='tipocons'>RET</span>";
						 }else{
							$addtipo = ""; 
						 }
						
						/////////////cores
						$cor = '#f9ffa0';
						if($status==1){$cor  = '#f9ffa0';}
						if($status==2 || $status==9){$cor  = '#adffaf';}
						if($status==3){$cor  = '#ffd68c';}
						if($status==4){$cor  = '#94baff';}
						if($status==5){$cor  = '#ff8a8a';}
						if($status==6){$cor  = '#ffb7fe';}
						if($status==7){$cor  = '#FFF';}
						if($status==8){$cor  = '#e46900';}
				?>
                
                <div class="consulta" idconsulta='<?php echo $idcons; ?>' horai='<?php echo $hini; ?>' horaf='<?php echo $hfim; ?>' idpaciente="<?php echo $idpaciente; ?>" nomepaciente="<?php echo $nomepaciente; ?>" statusconsulta="<?php echo $status; ?>" idmed="<?php echo $idmed; ?>" idsala="<?php echo $idsala; ?>" parcelamento="<?php echo $parcelamento; ?>" forma_pagamento="<?php echo $forma_pagamento; ?>" planosaude="<?php echo $planosaude; ?>" nota="<?php echo $nota; ?>" valor="<?php echo $valor; ?>", dinheiro="<?php echo $dinheiro; ?>", cartao="<?php echo $cartao; ?>", valormedico="<?php echo $valormedico; ?>" valorclinica="<?php echo $valorclinica; ?>" justificativa="<?php echo $justificativa; ?>" motivo="<?php echo $obs; ?>" tipoconsulta="<?php echo $tipoconsulta; ?>" foto="<?php echo $urlfoto; ?>" style="background-color:<?php echo $cor; ?>;">
					
                	<?php
					$tipousu = $_POST['tipousu'];
					if($tipousu==2){
					?>
                    <div class="txtconsulta"><?php echo $eschorario . $escnome . $addtipo . $obs; ?></div>
                    <?php }else{ ?>
                     <div class="txtconsulta"><?php echo $eschorario . $escnome . $addtipo; ?></div>
                    <?php } ?>
                </div>
                
                <?php } ?>
            </div>
            
            <?php } ?>
			
            
            <script type="text/javascript" src="js/consultas.js?51"></script>