<?php 
	date_default_timezone_set("America/Sao_Paulo");
	setlocale(LC_ALL, 'pt_BR');
	include('_include_token.php');
	include('conexao.php'); 
	if(isset($_GET['data'])){
		$data = $_GET['data'];	
	}else{
		$data = date('Y-m-d');
		//echo $data;	
	}
	$idpaciente = $_GET['idpaciente'];
	
	$sql = "SELECT *, TIMESTAMPDIFF (YEAR, data_nascimento ,CURDATE()) as idade FROM clientes WHERE id=".$idpaciente;

			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$nome = $res['nome'];
				$idade = $res['idade'];

				if(trim($res['foto'])!=''){
				
					$foto = 'upload/fotos/'.$res['foto'];
				 }
				 else{
					$foto = 'img/icones/persona_ico.png'; 
				 }
			 }
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Pacientes</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<link href="css/imgbox.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js?2"></script>
<script type="text/javascript" src="js/qrcode.js"></script>
<script type="text/javascript" src="js/imgbox.js"></script>
<script>
$(document).ready(function(){
	
	function loader(n){
		if(n==1){
			$('.mascara, .loader').fadeIn(400);	
		}
		if(n==0){
			$('.mascara, .loader').fadeOut(400);
		}
	}
	
	
	$('textarea, input').attr('disabled','disabled');
	$('#tiporeceituario, .inputtable, #salvarreceituario,#laudoeco,#salvarecopaciente,#addmedicacao,#medicacao_cronica').removeAttr('disabled');
	
	$('.btreceituario').click(function(){
		$('#salvarreceituario').attr('idc',$('#salvarprontuario').attr('idc'));
	});
	
	setTimeout(function(){
		$('.fotocliente img').fadeIn(400);
		
		$.ajax({
			url : "ajax_historico.php",
			data : {
				acao : 'listarhistorico',
				idc : <?php echo $idpaciente; ?>,
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.historico').html(data);
				$('.abreprontuario').click(function(){
					idp = $(this).attr('idp');
					window.open('prontuario.php?idp='+idp);
				});
				$('.abrereceituario').click(function(){
					idr = $(this).attr('idr');
					window.open('receituario.php?idr='+idr);
				});
				$('.abreconsulta').click(function(){
					idc = $(this).attr('idc');
					window.open('consulta.php?idc='+idc);
				});
				$('.abrelaudo').click(function(){
					cod = $(this).attr('cod');
					window.open('laudo_ecografia.php?cod='+cod);
				});
			}
		});
		
	},1000);
	
	
	$('.circulotempo').click(function(){
		
		if($(this).attr('status')=='1'){
			$('#termina').click();
			$(this).attr('status','2');
		}
		
		if($(this).attr('status')=='0'){
			$('#comeca').click();
			$(this).attr('status','1');	
			$('.atender').fadeOut(400);
		}
		
	});
	
	$('.atender').click(function(){
		$('.circulotempo').click();
	});
	
	$('#comeca').click(function(){
		$('textarea, input').removeAttr('disabled','disabled');
		$('.circulotempo').css({'border': '4px solid #00bd12'});
		$('.tempo').css({'color':'#00dc00'});
		
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'andamento',
					idconsulta : $('#idconsulta').val()
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
								
				}
			});
			
			$.ajax({
			url : "ajax_core.php",
				data : {
					acao : 'cadastroprontuario',
					idc : $('#salvarprontuario').attr('idc'),
					idconsulta : $('#idconsulta').val(),
					motivo : $('#motivo').val(),
					doenca : $('#doencapront').attr('cid'),
					subjetivo : $('#subjetivo').val(),
					objetivo : $('#objetivo').val(),
					exames : $('#exames').val(),
					impressao : $('#impressao').val(),
					conduta : $('#conduta').val(),
					inicio : $('#inicio').val(),
					fim : $('#fim').val(),
					tempo : $('#tempofinal').val()
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					setInterval(function(){
						tempprontuario();
					},30000);
					
				}
			});
		
		segundo = 0;
			minuto=0;
			hr = 0;
			rodando = setInterval(function(){
				segundo++;
				if(segundo==60){
					minuto++;
					segundo=0;	
				}
				if(minuto==60){
					hr++;
					minuto=0;
				}
				if(segundo<10){escseg = '0'+segundo.toString();}
				else{escseg=segundo;}
				if(minuto<10){escmin = '0'+minuto.toString();}
				else{escmin=minuto;}
				if(hr<10){
					eschr = '0'+hr.toString();
					tempo = escmin+':'+escseg;
				}
				else{
					eschr=hr;
					tempo = eschr + ':' + escmin+':'+escseg;
				}
				
				
				
				
				$('.tempo').html(tempo);
				
			},1000);
			
			var data = new Date();
			var dia     = data.getDate();           // 1-31
			var mes     = data.getMonth();          // 0-11 (zero=janeiro)
			var ano4    = data.getFullYear();       // 4 dígitos
			var hora    = data.getHours();          // 0-23
			var minu     = data.getMinutes();        // 0-59
			var seg     = data.getSeconds();        // 0-59
			
			if(seg<10){seg='0'+seg;}
			if(minu<10){minu='0'+minu;}
			if(hora<10){hora='0'+hora;}
			if(dia<10){dia='0'+dia;}
			if(mes<10){mes='0'+mes;}
			
			//var mseg    = data.getMilliseconds();   // 0-999
			//var tz      = data.getTimezoneOffset(); // em minutos
			
			agora = ano4 + '-' + mes + '-' + dia + ' ' + hora + ':'+minu+':'+seg;
			$('#inicio').val(agora);
			
	});
	$('#termina').click(function(){
		clearInterval(rodando);
		$('.circulotempo').css({'border': '4px solid #bd2800'});
		$('.tempo').css({'color':'red'});
		
		var data = new Date();
		var dia     = data.getDate();           // 1-31
		var mes     = data.getMonth();          // 0-11 (zero=janeiro)
		var ano4    = data.getFullYear();       // 4 dígitos
		var hora    = data.getHours();          // 0-23
		var minu     = data.getMinutes();        // 0-59
		var seg     = data.getSeconds();        // 0-59
		
		if(seg<10){seg='0'+seg;}
		if(minu<10){minu='0'+minu;}
		if(hora<10){hora='0'+hora;}
		if(dia<10){dia='0'+dia;}
		if(mes<10){mes='0'+mes;}
		
		//var mseg    = data.getMilliseconds();   // 0-999
		//var tz      = data.getTimezoneOffset(); // em minutos
		
		agora = ano4 + '-' + mes + '-' + dia + ' ' + hora + ':'+minu+':'+seg;
		$('#fim').val(agora);
		$('#tempofinal').val($('.tempo').text());
	});
	
	function tempprontuario(acao){
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'tempprontuario',
				idc : $('#salvarprontuario').attr('idc'),
				idconsulta : $('#idconsulta').val(),
				motivo : $('#motivo').val(),
				doenca : $('#doencapront').attr('cid'),
				subjetivo : $('#subjetivo').val(),
				objetivo : $('#objetivo').val(),
				exames : $('#exames').val(),
				impressao : $('#impressao').val(),
				conduta : $('#conduta').val(),
				inicio : $('#inicio').val(),
				fim : $('#fim').val(),
				tempo : $('#tempofinal').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				console.log(data);
				if(acao == 'finaliza'){
					//location.href='prontuario.php?idp='+$('#salvarprontuario').attr('idc');
					loader(0);
					//window.open('prontuario.php?idp='+$('#salvarprontuario').attr('idc'));
					location.href = 'agenda.php';
						
				}
			}
		});	
		salvaorcamento();
	}
	
	
	
	
	function carregafotospt(){
		$.ajax({
				url : "ajax_fotos_prontuario.php",
				data : {
					idconsulta : $('#idconsulta').val()
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					$('.exibefotospt').html(data);
				}
			});
	}
	
	//carregafotospt();
	
	$('.btcarregafotospt').click(function(){
		carregafotospt();
	});
	
	$('#salvarprontuario2').click(function(){
		$('#salvarprontuario').click();
	});
	
	
	$('#salvarprontuario').click(function(){
		$('.circulotempo').click();
		
		//alert('a');
		loader(1);
		setTimeout(function(){
			tempprontuario('finaliza');
		},300);
		
	});
	
	var qrcode = new QRCode(document.getElementById("qrcode"), {
		width : 140,
		height : 140
	});
	
	

	function makeCode () {			
		qrcode.makeCode('https://www.systemcdc.com.br/fotos_prontuario.php?idc='+$('#idconsulta').val()+'&idp='+$('#salvarprontuario').attr('idc'));
	}
	makeCode();
	setTimeout(function(){
		carregafotospt();
	},1000);
	
	//////////////CARREGAR MODELO DE ECOGRAFIA
	$('#modeloeco').change(function(){
		$('.mascara2,.loader').fadeIn(300);
		idm = $(this).val();
		setTimeout(function(){
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'buscamascaraeco',
					idm : idm,
				},
				type : "POST",
				async: true,
				dataType:"json",
				success : function(data) {
					$('#laudoeco').val(data[0].texto);
					CKEDITOR.instances.laudoeco.setData(data[0].texto);
					$('#nomeecopaciente').val(data[0].nomeimpressao);
					$('.mascara2,.loader').fadeOut(300);
				}
			});
		},500);
	});
	
	
	//////////// CADASTRAR LAUDO DE ECOGRAFIA
	
	$('.btlaudoeco').click(function(){
		$('.modaleco,.mascara').fadeIn(400);
	});
	
	$('#salvarecopaciente').click(function(){
		$('.mascara2,.loader').fadeIn(300);
		setTimeout(function(){
			$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'salvalaudoeco',
						idpaciente :  $('#salvarprontuario').attr('idc'),
						nome : $('#nomeecopaciente').val(),
						texto : CKEDITOR.instances.laudoeco.getData()
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						alert('Laudo de ecografia cadastrado!');
						$('.mascara2,.loader').fadeOut(300);
						$('.fecharmodal').click();
						$('#nomeecopaciente,#laudoeco,#modeloeco').val('');
						location.href="laudo_ecografia.php?cod="+data;
					}
				});
		},500);
	});
	
	$('#addmedicacao').click(function(){
		$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'salvamedcronica',
						idpaciente :  $('#salvarprontuario').attr('idc'),
						medicacao : $('#medicacao_cronica').val()
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						alert('Medicação crônica salva');
						carregamedicacoes();
					}
				});
	});
	
	function deletamedicac(){
		$('.deletamedcron').click(function(){
			idmedcron = $(this).attr('idmedcron');
			$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'deletamedcronica',
						idmedcron :  idmedcron
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						alert('Medicação crônica deletada');
						carregamedicacoes();
					}
				});	
		});
	}
	
	function carregamedicacoes(){
		$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'carregamedcronica',
						idpaciente :  $('#salvarprontuario').attr('idc')
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						$('.medicacoes').html(data);
						deletamedicac();
					}
				});	
	}
	carregamedicacoes();
	
	
	function carregaorcamento(){
		$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'carregaorcamento',
						idconsulta :  $('#idconsulta').val()
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						$('#orcamentocirurgico').val(data);
					}
				});	
	}
	if($('#orcamentocirurgico')){
		carregaorcamento();
	}
	
	
	orcamento = '';
	orcamentoatual = '';
	function salvaorcamento(){
		if($('#orcamentocirurgico')){
			orcamento = $('#orcamentocirurgico').val();	
			if(orcamento.trim()!='' && orcamento != orcamentoatual){
				$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'addorcamento',
						idconsulta :  $('#idconsulta').val(),
						orcamento : $('#orcamentocirurgico').val()
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						$('.medicacoes').html(data);
						deletamedicac();
					}
				});	
				orcamentoatual = orcamento;	
			}
		}
	}
	
	
	
});
</script>
<style>
.circulotempo{
    position: absolute;
    left: 5%;
    top: 85px;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    border: 4px solid #CCC;
    cursor: pointer;
    z-index: 30;
}
.tempo{
	width: 50px;
    text-align: center;
    font-size: 1em;
    margin-top: 16px;
    font-weight: 600;
    color: #ccc;	
}
#comeca, #termina{
display:none;	
}
#qrcode{
    position: absolute;
    right: 10%;
    top: 85px;	
}
.rodafoto{
    width: 30px;
    position: absolute;
    left: 50%;
    cursor: pointer;
    z-index: 50;
    background-color: #FFF;
    padding: 5px;
    border-radius: 50%;
    box-shadow: 0px 0px 5px #444;
}
.fotopt{
	text-align:center;	
}
</style>
</head>

<body>

<div class="mascara"><img src="img/loader1.gif" /></div>
<div class="mascara2"><img src="img/loader1.gif" /></div>
<div class="corpoprontuario">
	
    	<input id="comeca" type="button" value="comeca" />
        <input id="termina" type="button" value="termina" />
        <input type="hidden" id="inicio" value="" />
        <input type="hidden" id="fim" value="" />
        <input type="hidden" id="tempofinal" value="" />
            
            <div class="circulotempo" status="0">
                <div class="tempo">00:00</div>
            </div>
            <div class="atender">Atender</div>
            <div id="qrcode"></div>
    		
    <div class="linha titulomodal">
        	Prontuário
            <div class="btreceituario">Novo receituário</div>
            <div class="btlaudoeco">Novo laudo</div>
    </div>
    
        <div class="corpomodal">
        	
            <div class="linha top10 fotocliente" style="height:130px;">
                <img src="<?php echo $foto; ?>" class="imgpaciente" style="right:180px;"/>
            </div>
            
        	<div class="linha top10">
            	<div class="linha">
                    <div class="label">Paciente</div>
                    <div class="inputform"><div class="exibenomepaciente"><?php echo $nome; ?> (<?php echo $idade; ?> anos)</div></div>
                </div>
                
                
                <?php
				
					$motivo = '';
					$idconsulta = $_GET['idconsulta'];
					
					$subjetivo = '';
					$objetivo = '';
					$exames = '';
					$impressao = '';
					$conduta = '';
					$pcid = '';
					$doenca = '';
					
					$sql0 = "SELECT p.*, d.doenca FROM prontuario as p left join doencas as d on p.cid = d.cid WHERE id_consulta = " . $_GET['idconsulta'];
					$resultado0 = mysqli_query($conexao, $sql0);
					if($res0 = mysqli_fetch_assoc($resultado0)){
						$motivo = $res0['motivo'];
						$subjetivo = $res0['subjetivo'];
						$objetivo = $res0['objetivo'];
						$exames = $res0['exames'];
						$impressao = $res0['impressao'];
						$conduta = $res0['conduta'];
						$pcid = $res0['cid'];
						$doenca = $res0['doenca'];
						
					}else{
				
						if(isset($_GET['idconsulta']) && $_GET['idconsulta'] != ''){
							$idconsulta = $_GET['idconsulta'];
							$sql = "SELECT obs FROM consulta WHERE id = " . $_GET['idconsulta'];
							//echo $sql;
							$resultado = mysqli_query($conexao, $sql);
							if($res = mysqli_fetch_assoc($resultado)){
								$motivo = $res['obs'];
							}
						}
						else{
							$motivo = '';	
							$idconsulta = '';
						}
					}
				?>
                <div class="linha top20"><input type="button" id="salvarprontuario2" class="btnsalva" value="Salvar" /></div>
                
                <div class="linha top20">
                    <div class="label">Medicações crônicas</div>
                    <div class="linha top20 medicacoes"></div>
                </div>
                <div class="linha top20">
                    <div class="label">Adicionar medicação crônica</div>
                    <div class="inputform"><input type="text" id="medicacao_cronica" value="" style="width: 30%;" /></input><input type="button" id="addmedicacao" value="Adicionar" /></div>
                </div>
                 <?php
					$sql = "SELECT permite_orcamento FROM login WHERE id = " . $_COOKIE['idusu'];
					$resultado = mysqli_query($conexao, $sql);
					//echo $sql;
					if($res = mysqli_fetch_array($resultado)) {
						$permiteorc = $res['permite_orcamento'];
					}
					if($permiteorc==1){
				?>
				<div class="linha top10">
					<div class="label">Orçamento Cirugico</div>
					<textarea id="orcamentocirurgico"></textarea>
				</div>
				<?php } ?>
                <div class="linha top20">
                    <div class="label">Motivo da Consulta</div>
                    <div class="inputform"><input type="text" id="motivo" value="<?php echo $motivo; ?>" /></input></div>
                </div>
                <div class="linha top10">
                    <div class="label">Doença / CID</div>
                    <div class="inputform">
					<?php
                        if($pcid!=''){
                    ?>
                    	<input type="text" id="doencapront" cid="<?php echo $pcid; ?>" value="<?php echo $pcid . " - " . $doenca; ?>" />
                    <?php }else{ ?>
                    	<input type="text" id="doencapront" />
                    <?php } ?>
                        <div class="sugestoesdoenca"></div>
                    </div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Subjetivo</div>
                    <div class="inputform"><textarea id="subjetivo" /><?php echo $subjetivo; ?></textarea></div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Objetivo</div>
                    <div class="inputform"><textarea id="objetivo" /><?php echo $objetivo; ?></textarea></div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Exames</div>
                    <div class="inputform"><textarea id="exames" /><?php echo $exames; ?></textarea></div>
                </div>
				
                <div class="linha top10">
                    <div class="label">Impressão</div>
                    <div class="inputform"><textarea id="impressao" /><?php echo $impressao; ?></textarea></div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Conduta</div>
                    <div class="inputform"><textarea id="conduta" /><?php echo $conduta; ?></textarea></div>
                </div>
                
            </div>
            
        </div>

   
        <div class="linha top10">
        	<input type="hidden" id="idconsulta" value="<?php echo $idconsulta; ?>" />
        	<input type="button" id="salvarprontuario" class="btnsalva" value="Salvar" acaoconsulta="cadastrar" idc="<?php echo $idpaciente; ?>" />
        </div>
        
        <div class="linha top20">
        	<div class="tipohist">Fotos:</div>
            <div class="linha top10">
            	<div class="btcarregafotospt" style="font-size:1.2em; float:left;">Carregar fotos</div>
            </div>
        	<div class="exibefotospt">
            	
            </div>
        </div>
        
        <div class="linha top20">
        	<div class="historico">
            	
            </div>
        </div>

    
</div>

<?php include('modal_receituario.php'); ?>
<?php include('modal_ecografia.php'); ?>
<div class="avisoconexao">Atenção! Verifique a conexão com a internet!</div>
</body>
</html>
