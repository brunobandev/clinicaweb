<?php 
	date_default_timezone_set("America/Sao_Paulo");
	setlocale(LC_ALL, 'pt_BR');
	include('_include_token.php');
	include('conexao.php'); 
	if(isset($_GET['data'])){
		$data = $_GET['data'];	
	}else{
		$data = date('Y-m-d');
		//echo $data;	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Unificação de Pacientes</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/relatorios.js"></script>

<script>
$(document).ready(function(){
	setTimeout(function(){
		$('.mascara').fadeOut(400);
	},2000);
	
	$('#buscauni').click(function(){
		$.ajax({
			url : "ajax_unifica.php",
			data : {
				acao : 'busca',
				nome : $('#pacientesuni').val()
			},
			type : "POST",
			success : function(data) {
				$('#exibepacientes').html(data);
				unificar();
			}
		});
	});
	
	function unificar(){
		$('input[name=ref]').click(function(){
			$(this).parent().parent().children().css({'background-color':'#73ff73'});
		});
		$('[type=checkbox]').click(function(){
			$(this).parent().parent().children().css({'background-color':'#ff7973'});
		});
		$('#unifica').click(function(){
			ids = '';
			$('[type=checkbox]').each(function(){
				if($(this).is(':checked')){
					ids += $(this).val()+',';
				}
			});
			ref = $('input[name=ref]:checked').val();
			ids = ids.slice(0, -1);
			
			
			$.ajax({
				url : "ajax_unifica.php",
				data : {
					acao : 'unificar',
					ref : ref,
					dup : ids
				},
				type : "POST",
				success : function(data) {
					$('#buscauni').click();
				}
			});
			
		});
	};
	
	
});
</script>

<style>
.tudo{
position: relative;
    float: left;
    margin-bottom: 100px;	
}
td{
 padding: 5px;
 margin:1px;
 background-color:#F2F2F2;
}
#unifica, #buscauni{
padding: 8px 12px;
    font-size: 1.3em;
    margin-left: 20px;	
}
</style>

</head>

<body>


<div class="linha tudo">
	<?php include('_include_cabecalho.php'); ?>
    <?php
		//session_start();
		
		$sql = "SELECT * FROM login WHERE id = '".$_SESSION["idusu"]."'";
		$resultado = mysqli_query($conexao, $sql);
		//echo $sql;
		if($res = mysqli_fetch_array($resultado)) {
			$nome = utf8_encode($res['nome']);
		}
	
	if($_COOKIE['tipo']==1 || $_COOKIE['tipo']==3){
	?>
    <div class="linha" style="margin-top: 100px;">
    	<div class="div50">
        	<input type="text" id="pacientesuni" style="width:360px;" />
            <input type="button" id="buscauni" value="Buscar" />
        </div>
        
        <div class="linha top30 carregapacientes">
        	<table id="exibepacientes">
            	
            </table>
        </div>
    
    </div>
    <?php } ?>
    <div class="linha exibe"></div>
    <div class="linha top20">
    	<input type="button" id="unifica" value="Unificar" />
    </div>
    <div class="linha top50">
    	<div class="titulomodal">Nomes duplicados</div>
        <table>
            <?php
				$sql2 = "SELECT nome, COUNT(nome) as qtd FROM clientes GROUP by nome HAVING COUNT(nome) > 1";
								
				$resultado2 = mysqli_query($conexao, $sql2);
				while($res = mysqli_fetch_assoc($resultado2)){
					$nome = utf8_encode($res['nome']);
					$qtd = $res['qtd'];
					?>
					<tr>
						<td><?php echo $nome; ?></td>
						<td><?php echo $qtd; ?></td>
					</tr>
					<?php
				}
				?>
        </table>
    </div>
</div>
<?php include('modais.php'); ?>
</body>
</html>
