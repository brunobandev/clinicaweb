<div class="modal modalusuario">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar Usuários</div>
        <div class="corpomodal">
        	<div class="linha top10">
            	<div class="div50">
                    <div class="label">Nome</div>
                    <div class="inputform"><input type="text" id="nomeusu" /></div>
                </div>
            </div>
            <div class="linha top10">
                <div class="div25">
                    <div class="label">Usuário</div>
                    <div class="inputform"><input type="text" id="usuario" /></div>
                </div>
                <div class="div25">
                    <div class="label">Senha</div>
                    <div class="inputform"><input type="text" id="senhausuario" /></div>
                </div>
                <div class="div25">
                    <div class="label">tipo</div>
                    <div class="inputform">
                    	<select id="tipousuario">
                        	<option value="">Selecione</option>
                        	<?php
                                $sql = "SELECT * FROM tipo_usuarios WHERE id > 1 order by id";
                                $resultado = mysqli_query($conexao, $sql);
                                while ($res = mysqli_fetch_assoc($resultado)) {
                                    $id = $res['id'];
                                    $tipo = $res['tipo'];
                                    echo "<option value='".$id."'>".utf8_encode($tipo)."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="linha linhausumed">
                    <div class="div25">
                        <div class="label">CRM</div>
                        <div class="inputform"><input type="text" id="crm" /></div>
                    </div>
                    <div class="div25">
                        <div class="label">Especialidade</div>
                        <div class="inputform"><input type="text" id="medespecialidade" /></div>
                    </div>
                    <div class="div25">
                            <div class="btenviar btenviaassinatura">Enviar assinatura</div>
                            <form id="formularioassinatura" method="post" enctype="multipart/form-data" action="upload/upload_assinatura.php">
                                <input type="file" id="inputassinatura" name="imagem" />
                                <input type="hidden" name="idassinatura" id="idassinatura" value="" />
                            </form>
                    </div>
                    <div class="div25">
                            <div class="label">Assinatura</div>
                            <div class="linha carregaassinatura"></div>
                            
                    </div>
                    <div class="linha">
                    	<div class="div25">
                            <div class="label">Permite orçamento cirúrgico</div>
                            <div class="inputform">
                            	<select id="permiteorcamento">
	                            	<option value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                    	</div>
                    </div>
                </div>
                
            </div>
   		</div>
        <div class="linha top10">
        <input type="hidden" id="idusualtera" value="" />
        	<input type="button" id="salvarusuario" class="btnsalva" value="Salvar" />
        </div>
        <div class="linha top40">
        	<div class="label">Usuários cadastrados</div>
            <div class="linha top20"><div class="btnovousu">Novo usuário</div></div>
            <div class="div50 top10">
            	<table>
                	<tr>
                    	<td class="cabectabela">Nome</td>
                        <td class="cabectabela">Usuário</td>
                    	<td class="cabectabela">Senha</td>
                        <td class="cabectabela">Tipo</td>
                        <td class="cabectabela">CRM</td>
                        <td class="cabectabela">Especialidade</td>
                        <td class="cabectabela">Prem. Orçamento</td>
                    	<td></td>
                        <td></td>
                    </tr>
                    <?php
                        $sql = "SELECT l.*, t.tipo as ntipo FROM login as l inner join tipo_usuarios as t on l.tipo = t.id order by id";
                        $resultado = mysqli_query($conexao, $sql);
                        while ($res = mysqli_fetch_assoc($resultado)) {
                            $id = $res['id'];
                            $usuario = $res['usuario'];
                            $nome = $res['nome'];
                            $tipo = $res['tipo'];
                            $permiteorcamento = $res['permite_orcamento'];
                            $ntipo = $res['ntipo'];
                            $senha = $res['senha'];
                            $crm = $res['crm'];
                            $especialidade = $res['especialidade'];
                            if ($permiteorcamento==1) {
                                $escorc = "Sim";
                            } else {
                                $escorc = "Não";
                            } ?>
                            <tr>
                            	<td class="tblusu"><?php echo utf8_encode($nome); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($usuario); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($senha); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($ntipo); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($crm); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($especialidade); ?></td>
                                <td class="tblusu"><?php echo $escorc; ?></td>
                                <?php if ($tipo!=1) { ?>
                                	<td class="alterausu" idusu="<?php echo $id; ?>" tipo="<?php echo $tipo; ?>" permiteorcamento="<?php echo $permiteorcamento; ?>" senha="<?php echo utf8_encode($senha); ?>" usuario="<?php echo utf8_encode($usuario); ?>" nome="<?php echo utf8_encode($nome); ?>" crm="<?php echo utf8_encode($crm); ?>" especialidade="<?php echo utf8_encode($especialidade); ?>" idusu="<?php echo $id; ?>" style="width:20px;">A</td>
                                	<td class="deletausu" idusu="<?php echo $id; ?>" style="width:20px;">X</td>
                                <?php } ?>
                            </tr>
                            <?php
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>

