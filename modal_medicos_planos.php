<div class="modal modalmedicosplanos">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Médico x plano de saúde</div>
        <div class="corpomodal">
        	<div class="linha top10">
            	<div class="div50">
                    <div class="label">Médico</div>
                    <div class="inputform">
                    	<select id="medicoplano">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$nome = $res['nome'];
									echo "<option value='".$id."'>".utf8_encode($nome)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="linha top10 carregaplanosmed">
            	<?php
					$sql = "SELECT * FROM planos_saude ORDER BY plano";
					$resultado = mysqli_query($conexao, $sql);
					while($res = mysqli_fetch_assoc($resultado)){
						$id = $res['id'];
						$plano = utf8_encode($res['plano']);
						?>
                       <div class="itemplano"><input type="checkbox" class="checkplano" idcheck="<?php echo $id; ?>" value="<?php echo $id; ?>" /> <?php echo $plano; ?></div> 
              <?php	}	?>
            	
            </div>
            
   		</div>
    </div>
    
    <div class="planos_atendidos">
    	
    </div>
    
</div>