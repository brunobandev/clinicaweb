<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

date_default_timezone_set("America/Sao_Paulo");
    setlocale(LC_ALL, 'pt_BR');
    include('_include_token.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Organização mensal de salas - Clínica da Cirurgia</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="css/estilo.css?54" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js?327"></script>
<style>
td{
padding: 5px;	
}
select{
width: 150px;	
}
.cabec{
	text-align:center;
	color:#03C;
	font-size: 1.3em;
}
.tabturno{
text-align:right;
	color:#03C;
	font-size: 1.3em;
}
</style>

<script>
$(document).ready(function(){
	$('.salamed').append($('#option_medicos').html());
	
	
	conta = 0;
	
	function carrega(){
		$('.mascara').show();
		setTimeout(function(){
		$('.salamed').each(function(){
			dia = $(this).attr('dia');
			turno = $(this).attr('turno');
			mes = $('#selmes').val();
			ano = $('#selano').val();
			sala = $('#selsala').val();
			sede = $('#selsede').val();
			alvo = $(this);
			conta++;
			$.ajax({
				url : "ajax_salas_medicos.php",
				data : {
					acao : 'consultames',
					dia : dia,
					turno : turno,
					mes : mes,
					ano : ano,
					sala : sala,
					sede : sede
				},
				type : "POST",
				//dataType:"json",
				async:false,
				success : function(data) {
					alvo.val(data);
					$('.mascara').hide();
					console.log(conta);
				}
			});
			
		});
		},500);
	}
	
	$('.salamed').change(function(){
		dia = $(this).attr('dia');
		turno = $(this).attr('turno');
		mes = $('#selmes').val();
			ano = $('#selano').val();
			sala = $('#selsala').val();
			sede = $('#selsede').val();
		
		medico = $(this).val();
		$.ajax({
			url : "ajax_salas_medicos.php",
			data : {
				acao : 'gravadiames',
				dia : dia,
				turno : turno,
				mes : mes,
				ano : ano,
				sala : sala,
				sede : sede,
				medico : medico
			},
			type : "POST",
			//dataType:"json",
			async:false,
			success : function(data) {
				console.log(data);
				//alert('salvo');
			}
		});
	});
	
	
	function carregasalas(){
		console.log('a');
		$.ajax({
			url : 'ajax_core.php',
			data : {
				acao : 'carregasalas',
				idsede : $('#selsede').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('#selsala').html(data);
			},
			error: function(){
					
			}
		});

	}
	carregasalas();
	
	$('#selsede').change(function(){
		idsede = $(this).val();
		$('#idsede').val(idsede);
		carregasalas();
	});
	
	
	$('#carrega').click(function(){
		carrega();
	});
	
});
</script>

</head>

<body>
<?php include('_include_cabecalho.php');?>
<div style="display:none;" id="option_medicos">
	<?php
    include('conexao.php');

    $sql2 = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
    $resultado2 = mysqli_query($conexao, $sql2);
    while ($res = mysqli_fetch_assoc($resultado2)) {
        ?>
		<option value="<?php echo $res['id']; ?>"><?php echo utf8_encode($res['nome']); ?></option>        
    <?php
    }
    
    ?>
</div>
<div class="div96 top100">
<div class="linha titulopagina" style="margin-top:30px;">Organizar salas x médicos x turnos por mês</div>
<div class="linha top20">

<select id="selsede">
	<option value=""> Sede </option>
	<?php
        $sql2 = "SELECT * FROM sedes ORDER BY id";
        $resultado2 = mysqli_query($conexao, $sql2);
        while ($res = mysqli_fetch_assoc($resultado2)) {
            ?>
			<option value="<?php echo $res['id']; ?>"><?php echo utf8_encode($res['nome']); ?></option>        
		<?php
        }
    ?>
</select>

<select id="selsala">
	<option value=""> Sala </option>
	<?php
        $sql2 = "SELECT * FROM salas ORDER BY id";
        $resultado2 = mysqli_query($conexao, $sql2);
        while ($res = mysqli_fetch_assoc($resultado2)) {
            ?>
			<option value="<?php echo $res['id']; ?>"><?php echo utf8_encode($res['nome']); ?></option>        
		<?php
        }
    ?>
</select>

<select id="selmes">
	<option value=""> Mês </option>
    <option value="1"> Jan </option>
    <option value="2"> Fev </option>
    <option value="3"> Mar </option>
    <option value="4"> Abr </option>
    <option value="5"> Mai </option>
    <option value="6"> Jun </option>
    <option value="7"> Jul </option>
    <option value="8"> Ago </option>
    <option value="9"> Set </option>
    <option value="10"> Out </option>
    <option value="11"> Nov </option>
    <option value="12"> Dez </option>
</select>

<select id="selano">
	<option value=""> Ano </option>
    <?php
        $anoat = 2018;
        while ($anoat < 2035) {
            $anoat++; ?>
    <option value="<?php echo $anoat; ?>"> <?php echo $anoat; ?> </option>
    <?php
        } ?>
</select>

<input type="button" id="carrega" value="Carregar" style="padding: 10px;" />
</div>   

<div class="linha top20">
<table>
<tr>
	<td></td>
	<td class="cabec">Segunda</td>
    <td class="cabec">Terça</td>
    <td class="cabec">Quarta</td>
    <td class="cabec">Quinta</td>
    <td class="cabec">Sexta</td>
</tr>

<tr>
	<td class="tabturno">Manhã</td>
	<td><select class="salamed" dia="1" turno="1" nomedia="Mon"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="2" turno="1" nomedia="Tue"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="3" turno="1" nomedia="Wed"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="4" turno="1" nomedia="Thu"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="5" turno="1" nomedia="Fri"><option value="">Selecionar médico</option></select></td>
</tr>

<tr>
	<td class="tabturno">Tarde</td>
	<td><select class="salamed" dia="1" turno="2" nomedia="Mon"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="2" turno="2" nomedia="Tue"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="3" turno="2" nomedia="Wed"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="4" turno="2" nomedia="Thu"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="5" turno="2" nomedia="Fri"><option value="">Selecionar médico</option></select></td>
</tr>

<tr>
	<td class="tabturno">Noite</td>
	<td><select class="salamed" dia="1" turno="3" nomedia="Mon"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="2" turno="3" nomedia="Tue"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="3" turno="3" nomedia="Wed"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="4" turno="3" nomedia="Thu"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="5" turno="3" nomedia="Fri"><option value="">Selecionar médico</option></select></td>
</tr>


</table>
</div>
<?php
    $mes = 1;
    $ano = 2020;
    $sala = 4;

    $dia = 0;
    $conta = 0;
?>
</div>

</body>
</html>