<?php

require_once __DIR__.'/bootstrap.php';

$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
    case 'POST':
        $healthPlanLoader = $container->getHealthPlanLoader();
        $createdHealthPlan = $healthPlanLoader->store($_POST);
        header("Location: planos-saude.php");
        break;

    default:
        echo $twig->render('health-plans/create.html.twig');
        break;
}
