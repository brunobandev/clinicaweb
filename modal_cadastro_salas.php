<div class="modal modalsalas">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar Salas</div>
        <div class="corpomodal">
        	<div class="linha top10">
            	<div class="div50">
                    <div class="label">Nome da sala</div>
                    <div class="inputform"><input type="text" id="nomesala" /></div>
                </div>
                
                <div class="div25">
                    <div class="label">Sede</div>
                    <div class="inputform">
                        <select id="sedesala">
                            <option value="">Selecione</option>
                            <?php
                                $sql = "SELECT * FROM sedes order by nome";
                                $resultado = mysqli_query($conexao, $sql);
                                while($res = mysqli_fetch_assoc($resultado)){
                                    $id = $res['id'];
                                    $nome = $res['nome'];
                                    echo "<option value='".$id."'>".utf8_encode($nome)."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
            </div>
           
   		</div>
        <div class="linha top10">
        <input type="hidden" id="idsalaaltera" value="" />
        	<input type="button" id="salvarsala" class="btnsalva" value="Salvar" />
        </div>
        <div class="linha top40">
        	<div class="label">Salas cadastradas</div>
            <div class="linha top20"><div class="btnovasala">Nova sala</div></div>
            <div class="div50 top10">
            	<table>
                	<tr>
                    	<td class="cabectabela">Nome</td>
                        <td class="cabectabela">Sede</td>
                    	<td></td>
                    </tr>
                    <?php
						$sql = "SELECT sa.id as idsala, sa.nome as nomesala, sa.id_sede as idsede, se.nome as nomesede FROM salas as sa inner join sedes as se on sa.id_sede = se.id order by se.id, sa.id";
						$resultado = mysqli_query($conexao, $sql);
						while($res = mysqli_fetch_assoc($resultado)){
							$id = $res['idsala'];
							$nome = $res['nomesala'];
							$idsede = $res['idsede'];
							$nomesede = $res['nomesede'];
							?>
                            <tr>
                            	<td class="tblusu"><?php echo utf8_encode($nome); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($nomesede); ?></td>
                                	<td class="alterasala" idsala="<?php echo $id; ?>" idsede="<?php echo $idsede; ?>" nome="<?php echo utf8_encode($nome); ?>" style="width:20px;">A</td>
                                	<!--<td class="deletasala" idsala="<?php echo $id; ?>" style="width:20px;">X</td>-->
                            </tr>
                            <?php
						}
					?>
                </table>
            </div>
            
        </div>
    </div>
</div>

