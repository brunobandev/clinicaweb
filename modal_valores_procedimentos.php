<div class="modal modalprocedimentos">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar procedimentos</div>
        <div class="corpomodal">
        	
            <div class="linha top20">
            	<div class="div50">
                    <div class="label">Procedimento</div>
                    <div class="inputform"><input type="text" id="procedimento" /></div>
                </div>
            </div>
            
            <div class="linha top20">
            	<div class="div25">
                    <div class="label">Valor Total</div>
                    <div class="inputform"><input type="text" id="valorproctotal" /></div>
                </div>
                <div class="div25">
                    <div class="label">Valor Clínica</div>
                    <div class="inputform"><input type="text" id="valorprocclinica" /></div>
                </div>
                <div class="div25">
                    <div class="label">Valor Médico</div>
                    <div class="inputform"><input type="text" id="valorprocmedico" /></div>
                </div>
            </div>
            
   		</div>
        <div class="linha top20">
        	<input type="button" id="salvarprocedimento" class="btnsalva" value="Salvar" acao="cadastra" idproc="" />
        </div>
    </div>
    <div class="linha top30">
    	<div class="linha top20">
            <div class="tipohist">Procedimentos:</div>
        </div>
        <div class="linha top20">
        <table>
        	<tr>
            	<td class="cabectbl">Procedimento</td>
                <td class="cabectbl">Valor total</td>
                <td class="cabectbl">Valor clínica</td>
                <td class="cabectbl">Valor médico</td>
                <td class="cabectbl"></td>
            </tr>
        	<?php
            $sql = "SELECT * FROM procedimentos WHERE ativo = 1 ORDER BY procedimento";
			$resultado = mysqli_query($conexao, $sql);
			//echo $sql;
			while($res = mysqli_fetch_array($resultado)) {
				$id = $res['id'];
				$procedimento = utf8_encode($res['procedimento']);
				$total = $res['total'];
				$medico = $res['medico'];
				$clinica = $res['clinica'];
				?>
                <tr class="tdprocedimento" idproc="<?php echo $id; ?>" procedimento="<?php echo $procedimento; ?>" total="<?php echo $total; ?>" medico="<?php echo $medico; ?>" clinica="<?php echo $clinica; ?>">
                    <td class="celtbl"><?php echo $procedimento; ?></td>
                    <td class="celtbl"><?php echo $total; ?></td>
                    <td class="celtbl"><?php echo $clinica; ?></td>
                    <td class="celtbl"><?php echo $medico; ?></td>
                    <td class="celtbl deletaproc">X</td>
                </tr>
                <?php
			}
			?>
          </table>
        </div>
        
    </div>
</div>