
<div class="modal modaleco">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Nova ecografia</div>
        <div class="corpomodal">
        	<div class="linha top10">
                <div class="label">Modelo de Ecografia</div>
                <div class="inputform">
                    <select id="modeloeco" style="width: 450px;">
                        <option value="">Escolha um modelo</option>
                        <?php
                            $sql0 = "SELECT * FROM mascaras_ecografia_padroes order by nome";
                                $resultado0 = mysqli_query($conexao, $sql0);
                                while($res0 = mysqli_fetch_assoc($resultado0)){
                                    $id = $res0['id'];
                                    $nome = utf8_encode($res0['nome']);
                        ?>
                        <option value="<?php echo $id; ?>"><?php echo $nome; ?></option>
                        <?php
                                }
                        ?>
                    </select>
                </div>
            </div>
            <div class="linha top10">
                    <div class="label">Nome Ecografia</div>
                    <div class="inputform"><input type="text" id="nomeecopaciente" style="width: 450px;" disabled="disabled" /></div>
            </div>
            <div class="linha top10">
                <div class="label">Texto da máscara de ecografia</div>
                <div class="inputform"><textarea id="laudoeco" name="laudoeco" class="ckeditor" /></textarea></div>
            </div>
   		</div>
        <div class="linha top10">
        	<input type="button" id="salvarecopaciente" class="btnsalva" value="Salvar e imprimir" idaltera="" />
        </div>
    </div>    
</div>
<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
