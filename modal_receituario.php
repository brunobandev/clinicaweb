<div class="modal modalreceituario">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Receituário</div>
        <div class="corpomodal">
        	
            <div class="linha top10 fotocliente" style="height:100px;">
                <img src="" class="imgpaciente" />
            </div>
            
        	<div class="linha top10">
            	<div class="linha">
                    <div class="label">Paciente</div>
                    <div class="inputform"><div class="exibenomepaciente"></div></div>
                </div>
                
                <div class="linha top20">
                	Tipo:<br />
                    <select id="tiporeceituario" style="width:200px;">
                    	<option value="1" selected="selected">Simples</option>
                        <option value="2">Controle Especial</option>
                    </select>
                </div>
                <div class="linha top20">
                	Usar modelo:<br />
                    <select id="modelosrec" style="width:200px;">
                    	<option value="0" selected="selected">Utilizar modelo</option>
                    	<?php
							$sql3 = "SELECT * FROM receituarios_padroes WHERE id_medico = " . $_COOKIE["idusu"] . " order by nome";
							//echo $sql3;
							$resultado3 = mysqli_query($conexao, $sql3);
							while($res3 = mysqli_fetch_assoc($resultado3)){
						?>
                        <option value="<?php echo $res3['id_receituario']; ?>"><?php echo $res3['nome']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                
                
               
                <div class="linha top10 dadosreceituario">
                	<table>
                        <tr class="cabectabela">
                            <td class="topo" style="width:30%">Medicamentos</td>
                            <td class="topo" style="width:20%">Quantidade</td>
                            <td class="topo"  style="width:50%">Freqüência</td>
                        </tr>
                        
                        <tr class="linharec rec1">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec2">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec3">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec4">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec5">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec6">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec7">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        <tr class="linharec rec8">
                            <td class="celula recmed"><input type="text" class="inputtable" /></td>
                            <td class="celula recqtd"><input type="text" class="inputtable" /></td>
                            <td class="celula recfreq"><input type="text" class="inputtable" /></td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            
        </div>

   
        <div class="linha top10">
        	<input type="button" id="salvarreceituario" class="btnsalva" value="Salvar" acaoconsulta="cadastrar" idc="" />
        </div>
    </div>
</div>