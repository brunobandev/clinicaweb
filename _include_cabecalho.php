<div class="mascara"><img src="img/loader1.gif" /></div>

<div class="linha cabecalho">
    <div class="logo"><img src="img/logo.png" /></div>
    
    <?php if ($_COOKIE["tipo"] == 1 || $_COOKIE["tipo"] == 3): ?>
    <div class="itemmenu mn1" nitem="1"><img src="img/icones/paciente.png" /><br />Pacientes</div>
    <?php endif; ?>
    <div class="itemmenu mn2" nitem="2"><img src="img/icones/agenda.png" /><br />Agenda</div>
    
    <?php if ($_COOKIE["tipo"] == 1 || $_COOKIE["tipo"] == 3): ?>
    <div class="itemmenu mn5" nitem="5"><img src="img/icones/engrenagem.png" /><br />Administração</div>
    <?php endif; ?>
    
    <div class="itemmenu mn6" nitem="6"><img src="img/icones/sair.png" /><br />Sair</div>
    
    <?php if ($_COOKIE["tipo"] == 1 || $_COOKIE["tipo"] == 3): ?>
    <div class="itemmenu iconotif" style="width:60px;"><img src="img/icones/notificacoes.png" style="width: 32px;" /><span class="numnotif">3</span></div>
    <div class="qnotificacoes">
    	<div class="linha centro titcancel">Cancelamentos por Whats</div>
        <div class="linha contnotif">
        
        </div>
    </div>
    <?php endif; ?>
    
    <input type="hidden" id="tipousu" value="<?php echo $_COOKIE["tipo"]; ?>" />
</div>

<div class="subadm">
	<div class="subitemadm" subadm="1">Cadastrar de planos de saúde</div>
    <div class="subitemadm" subadm="2">Cadastrar planos de saúde por médico</div>
    <div class="subitemadm" subadm="3">Cadastrar origem pacientes</div>
    <div class="subitemadm" subadm="8">Clientes duplicados</div>
    <?php if ($_COOKIE["tipo"] == 1): ?>
    <div class="subitemadm" subadm="5">Cadastrar procedimentos</div>
    <div class="subitemadm" subadm="6">Valores consulta</div>
    <div class="subitemadm" subadm="7">Gerar excel</div>
    <div class="subitemadm" subadm="9">Gráficos</div>
    
    <div class="subitemadm" subadm="4">Cadastrar usuários</div>
    <div class="subitemadm" subadm="10">Cadastrar salas por sede</div>
    <div class="subitemadm" subadm="11">Cadastrar salas para médicos por mês</div>
    <div class="subitemadm" subadm="12">Cadastrar salas para médicos por dia</div>
    <div class="subitemadm" subadm="13">Cadastrar itens de estoque</div>
    <div class="subitemadm" subadm="14">Verificar estoque</div>
    <div class="subitemadm" subadm="15">Máscaras de ecografia</div>
    <div class="subitemadm" subadm="16">Buscar prontuários</div>
    <?php endif; ?>
</div>

<input id="editacliente" type="hidden" value="" />

<?php if (isset($_GET['m'])): ?>
    <script>
        setTimeout(function() {
            $('[subadm=<?php echo $_GET['m']; ?>]').click();
        }, 500);
    </script>
<?php endif; ?>

<div class="avisoconexao">Atenção! Verifique a conexão com a internet!</div>
