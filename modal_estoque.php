<div class="modal modalestoquetotal">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Estoque</div>
        <div class="corpomodal">
        	<div class="linha top10">
                <div class="div25">
                    <div class="label">Sede</div>
                    <div class="inputform">
                    	<select id="sedeitens">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM sedes order by id";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$sede = $res['nome'];
									echo "<option value='".$id."'>".utf8_encode($sede)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
                <div class="div25">
                    <div class="label">Categoria</div>
                    <div class="inputform">
                    	<select id="categoriaest" disabled="disabled">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM estoque_categorias order by categoria";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$categoria = $res['categoria'];
									echo "<option value='".$id."'>".utf8_encode($categoria)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
            </div>  
   		</div>
        <div class="linha top20" id="carregaitensest">
        	
        </div>
    </div>
</div>

