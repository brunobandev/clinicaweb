<?php

require_once __DIR__.'/bootstrap.php';

$userLoader = $container->getUserLoader();
$users = $userLoader->getUsers();

echo $twig->render('users/index.html.twig', ['users' => $users]);
