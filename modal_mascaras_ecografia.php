
<div class="modal modalmascaraeco">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar máscara de ecografia</div>
        <div class="corpomodal">
        	<div class="linha top10">
            	<div class="div25">
                    <div class="label">Nome da máscara de ecografia</div>
                    <div class="inputform"><input type="text" id="nomeecografia" /></div>
                </div>
                <div class="div25">
                    <div class="label">Nome para impressão</div>
                    <div class="inputform"><input type="text" id="nomeecografiaimp" /></div>
                </div>
            </div>
            <div class="linha top10">
            	<div class="div98">
                <div class="label">Texto da máscara de ecografia</div>
                <div class="inputform"><textarea id="textoecografia" style="height: 300px; text-align:justify;" /></textarea></div>
                </div>
            </div>
   		</div>
        <div class="linha top10">
        	<input type="button" id="salvarmascaraeco" class="btnsalva" value="Salvar" idaltera="" />
        </div>
    </div>
    
    <div class="linha top40">
        <div class="label">Máscaras de Ecografia</div>
        <div class="linha top20"><div class="btnovamascaraeco">Nova máscara</div></div>
        <div class="div50 top10">
            <table>
                <tr>
                    <td class="cabectabela">Nome</td>
                    <td></td>
                </tr>
                <?php
                    $sql = "SELECT * FROM mascaras_ecografia_padroes order by nome";
                    $resultado = mysqli_query($conexao, $sql);
                    while($res = mysqli_fetch_assoc($resultado)){
                        $id = $res['id'];
                        $nome = $res['nome'];
                ?>
                        <tr>
                            <td class="tblusu" style="width: 280px;"><?php echo utf8_encode($nome); ?></td>
                            <td class="alteramascaraeco" id="<?php echo $id; ?>" style="width:20px;">A</td>
                            <td class="deletamascaraeco" id="<?php echo $id; ?>" style="width:20px;">X</td>
                        </tr>
                        <?php
                    }
                ?>
            </table>
        </div>
    </div>
    
</div>