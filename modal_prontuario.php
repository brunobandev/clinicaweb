<div class="modal modalprontuario">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">
        	Prontuário
            <div class="btlimpapront">Limpar prontuário</div>
        </div>
        <div class="corpomodal">
        	
            <div class="linha top10 fotocliente" style="height:100px;">
                <img src="" class="imgpaciente" />
            </div>
            
        	<div class="linha top10">
            	<div class="linha">
                    <div class="label">Paciente</div>
                    <div class="inputform"><div class="exibenomepaciente"></div></div>
                </div>
                <div class="linha top10">
                    <div class="label">Doença / CID</div>
                    <div class="inputform">
                    	<input type="text" id="doencapront" />
                        <div class="sugestoesdoenca"></div>
                        
                        <!--<select id="doencapront">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM doencas ORDER BY doenca";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									//$id = $res['id'];
									$cid = $res['cid'];
									$doenca = utf8_encode($res['doenca']);
									echo "<option value='".$cid."'>".$cid . " - " . $doenca."</option>";
								}
							?>
                        </select>-->
                    </div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Subjetivo</div>
                    <div class="inputform"><textarea id="subjetivo" /></textarea></div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Objetivo</div>
                    <div class="inputform"><textarea id="objetivo" /></textarea></div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Exames</div>
                    <div class="inputform"><textarea id="exames" /></textarea></div>
                </div>
				
                <div class="linha top10">
                    <div class="label">Impressão</div>
                    <div class="inputform"><textarea id="impressao" /></textarea></div>
                </div>
                
                <div class="linha top10">
                    <div class="label">Conduta</div>
                    <div class="inputform"><textarea id="conduta" /></textarea></div>
                </div>
                
            </div>
            
        </div>

   
        <div class="linha top10">
        	<input type="button" id="salvarprontuario" class="btnsalva" value="Salvar" acaoconsulta="cadastrar" idc="" />
        </div>
        
        <div class="linha top20">
        	<div class="historico">
            	
            </div>
        </div>
        
    </div>
</div>