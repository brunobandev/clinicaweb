<?php
	
			$addsql = '';
			$string = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND cons.data BETWEEN '$de' AND '$ate'";
			}
			
			$sql = "SELECT sum(proc.total) as totalproc, p.procedimento FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id inner join procedimentos as p on p.id = proc.id_procedimento WHERE 1=1 $addsql GROUP BY p.procedimento";
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){

				$string .= "{name: '".utf8_encode($res["procedimento"])."',data: [".$res['totalproc']."]},";
			 }
			 $string = substr($string, 0, -1);
			// echo $string;
?>

<div id="container_renda_tipos_procedimentos_abs" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script src="js/export-data.js"></script>
<script>


	Highcharts.chart('container_renda_tipos_procedimentos_abs', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Renda por tipo de procedimento'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [''],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Renda',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valuePrefix: 'R$ '
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [<?php echo $string; ?>]
});
	
	
</script>
