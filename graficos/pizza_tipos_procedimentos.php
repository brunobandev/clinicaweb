<?php

			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND cons.data BETWEEN '$de' AND '$ate'";
			}

			$sql = "SELECT sum(proc.total) as totalproc, p.procedimento FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id inner join procedimentos as p on p.id = proc.id_procedimento WHERE 1=1 $addsql GROUP BY p.procedimento";
			$string = "";
			$totalfinal = 0;
			$resultadotot = mysqli_query($conexao, $sql);
			while($restot = mysqli_fetch_assoc($resultadotot)){
				$totalfinal +=  $restot['totalproc'];
			 }
			
			//echo $sql;
			$total = '';
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				
				$pct = ($res['totalproc'] * 100) / $totalfinal;
				$pct = number_format($pct, 2, '.', '');
				$string .= "{name: '".utf8_encode($res["procedimento"])."',y: ".$pct."},";
			 }
			 $string = substr($string, 0, -1);
?>


<div id="container_tipos_procedimentos" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script src="js/export-data.js"></script>
<script>


	Highcharts.chart('container_tipos_procedimentos', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de renda por procedimento'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>
