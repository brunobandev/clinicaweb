<?php
	
			$addsql = '';
			$string = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND cons.data BETWEEN '$de' AND '$ate'";
			}
			
			$sql = "SELECT 	sum(proc.total) as totalproc, sum(proc.medico) as medicoproc, sum(proc.clinica) as clinicaproc FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id WHERE 1=1 AND cons.status IN (3,4,6) $addsql";
			
			$string = "";
			
			
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$string = "{name: 'Renda médico',data: [".$res['medicoproc']."]},{name: 'Renda clínica',data: [".$res['clinicaproc']."]}";
			 }
			 //echo $string;
?>

<div id="container_renda_procedimentos_clinica_abs" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script src="js/export-data.js"></script>
<script>


	Highcharts.chart('container_renda_procedimentos_clinica_abs', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Renda de procedimentos'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [''],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Renda',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valuePrefix: 'R$ '
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [<?php echo $string; ?>]
});
	
	
</script>
