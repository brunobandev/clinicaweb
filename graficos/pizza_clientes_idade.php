<?php
			
			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND data_cadastro BETWEEN '$de' AND '$ate'";
			}
			
			$sql = "SELECT TIMESTAMPDIFF(YEAR, c.data_nascimento, NOW()) AS idade, count(TIMESTAMPDIFF(YEAR, c.data_nascimento, NOW())) as total FROM clientes as c WHERE 1=1 $addsql group by TIMESTAMPDIFF(YEAR, c.data_nascimento, NOW())";
			//echo $sql;
			$string = "";
			$totalfinal = 0;
			$resultadotot = mysqli_query($conexao, $sql);
			while($restot = mysqli_fetch_assoc($resultadotot)){
				$totalfinal +=  $restot['total'];
			 }
			
			//echo $sql;
			$total10 = 0;
			$total20 = 0;
			$total30 = 0;
			$total40 = 0;
			$total50 = 0;
			$total60 = 0;
			$total70 = 0;
			$total80 = 0;
			$total90 = 0;
			$total100 = 0;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				
				$total = $res['total'];
				$idadepac = $res['idade'];
				
				if($idadepac < 10){ 
					$total10 += $total;
				}
				if($idadepac >= 10 && $idadepac < 20){ 
					$total20 += $total;
				}
				if($idadepac >= 20 && $idadepac < 30){ 
					$total30 += $total;
				}
				if($idadepac >= 30 && $idadepac < 40){ 
					$total40 += $total;
				}
				if($idadepac >= 40 && $idadepac < 50){ 
					$total50 += $total;
				}
				if($idadepac >= 50 && $idadepac < 60){ 
					$total60 += $total;
				}
				if($idadepac >= 60 && $idadepac < 70){ 
					$total70 += $total;
				}
				if($idadepac >= 70 && $idadepac < 80){ 
					$total80 += $total;
				}
				if($idadepac >= 80 && $idadepac < 90){ 
					$total90 += $total;
				}
				if($idadepac >= 90 && $idadepac < 100){ 
					$total100 += $total;
				}
			 }
			 
			if($totalfinal != 0){
			 
				$pct10 = ($total10 * 100) / $totalfinal;
				$pct10 = number_format($pct10, 2, '.', '');
				
				$pct20 = ($total20 * 100) / $totalfinal;
				$pct20 = number_format($pct20, 2, '.', '');
				
				$pct30 = ($total30 * 100) / $totalfinal;
				$pct30 = number_format($pct30, 2, '.', '');
				
				$pct40 = ($total40 * 100) / $totalfinal;
				$pct40 = number_format($pct40, 2, '.', '');
				
				$pct50 = ($total50 * 100) / $totalfinal;
				$pct50 = number_format($pct50, 2, '.', '');
				
				$pct60 = ($total60 * 100) / $totalfinal;
				$pct60 = number_format($pct60, 2, '.', '');
				
				$pct70 = ($total70 * 100) / $totalfinal;
				$pct70 = number_format($pct70, 2, '.', '');
				
				$pct80 = ($total80 * 100) / $totalfinal;
				$pct80 = number_format($pct80, 2, '.', '');
				
				$pct90 = ($total90 * 100) / $totalfinal;
				$pct90 = number_format($pct90, 2, '.', '');
				
				$pct100 = ($total100 * 100) / $totalfinal;
				$pct100 = number_format($pct100, 2, '.', '');
				
				$string = "{name: '0 a 10 anos',y: ".$pct10."},{name: '10 a 20 anos',y: ".$pct20."},{name: '20 a 30 anos',y: ".$pct30."},{name: '30 a 40 anos',y: ".$pct40."},{name: '40 a 50 anos',y: ".$pct50."},{name: '50 a 60 anos',y: ".$pct60."},{name: '60 a 70 anos',y: ".$pct70."},{name: '70 a 80 anos',y: ".$pct80."},{name: '80 a 90 anos',y: ".$pct90."},{name: '90 a 100 anos',y: ".$pct100."}";
			}
			 
			 //$string = substr($string, 0, -1);
?>



<div id="container_clientes_idades" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_clientes_idades', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de idades de pacientes'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>

