<?php
	
			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND cons.data BETWEEN '$de' AND '$ate'";
			}
			
			$sql = "SELECT count(cons.id) as total, s.status FROM consulta as cons inner join status_consulta as s on cons.status = s.id WHERE 1=1 $addsql GROUP BY s.status";
			$string = "";
			$string2 = "";
			$totalfinal = 0;
			$resultadotot = mysqli_query($conexao, $sql);
			while($restot = mysqli_fetch_assoc($resultadotot)){
				$totalfinal +=  $restot['total'];
			 }
			
			//echo $sql;
			$total = '';
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				
				$pct = ($res['total'] * 100) / $totalfinal;
				$pct = number_format($pct, 2, '.', '');
				$string .= "{name: '".$res["status"]."',y: ".$pct."},";
				$total = $res["total"];
			 }
			 $string = substr($string, 0, -1);
?>

<div id="container_consultas_status" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_consultas_status', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de consultas por status'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>
