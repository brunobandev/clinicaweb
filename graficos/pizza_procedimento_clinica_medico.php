<?php
	
			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND cons.data BETWEEN '$de' AND '$ate'";
			}
	
			$sql = "SELECT 	sum(proc.total) as totalproc,    sum(proc.medico) as medicoproc,    sum(proc.clinica) as clinicaproc FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id WHERE 1=1 $addsql";
			$string = "";
			
			
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$totalfinal = $res['totalproc'];
				if($totalfinal > 0){
				$pctmed = ($res['medicoproc'] * 100) / $totalfinal;
				$pctmed = number_format($pctmed, 2, '.', '');
				$pctcli = ($res['clinicaproc'] * 100) / $totalfinal;
				$pctcli = number_format($pctcli, 2, '.', '');
				}else{
					$pctcli = 0;
					$pctmed = 0;
				}
				$string = "{name: 'Renda médico',y: ".$pctmed."},{name: 'Renda clínica',y: ".$pctcli."}";
			 }
?>

<div id="container_procedimento_clinica_medicos" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_procedimento_clinica_medicos', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de renda de procedimentos'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>
