<?php
	
			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND data_cadastro BETWEEN '$de' AND '$ate'";
			}
	
			$sql = "SELECT count(id) as total, cidade FROM clientes WHERE 1=1 $addsql GROUP BY cidade order by total DESC";
			$string = "";
			$totalfinal = 0;
			$resultadotot = mysqli_query($conexao, $sql);
			while($restot = mysqli_fetch_assoc($resultadotot)){
				$totalfinal +=  $restot['total'];
			 }
			
			//echo $sql;
			$total = '';
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				if($res["cidade"] == ""){ $res["cidade"] = "Não informada"; }
				$pct = ($res['total'] * 100) / $totalfinal;
				$pct = number_format($pct, 2, '.', '');
				$string .= "{name: '".$res["cidade"]."',y: ".$pct."},";
			 }
			 $string = substr($string, 0, -1);
?>


<div id="container_clientes_cidades" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_clientes_cidades', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de clientes por cidade'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>

