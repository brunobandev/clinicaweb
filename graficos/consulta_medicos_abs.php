<?php
	
			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND cons.data BETWEEN '$de' AND '$ate'";
			}
			
			$sql = "select count(cons.id) as total, log.nome from consulta as cons inner join login as log on log.id = cons.id_medico WHERE cons.status IN (3,4,6) $addsql group by log.nome order by total desc";
			$string = "";
			//echo $sql;
			
			
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				
				$pct = ($res['total'] * 100) / $totalfinal;
				$pct = number_format($pct, 2, '.', '');
				$string .= "{name: '".utf8_encode($res["nome"])."',data: [".$res['total']."]},";

			 }
			 $string = substr($string, 0, -1);
			 //echo $string;
?>

<div id="container_consultas_clinica_medico_abs" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script src="js/export-data.js"></script>
<script>


	Highcharts.chart('container_consultas_clinica_medico_abs', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Nº de consultas por médico'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [''],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Nº de consultas',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' consultas'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [<?php echo $string; ?>]
});
	
	
</script>
