<?php
			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND data BETWEEN '$de' AND '$ate'";
			}
			$sql = "SELECT count(id) as total, left(DATA,10) as data FROM consulta WHERE status = 4 " . $addsql . "group by left(data,10) order by left(data,10)";
			//echo $sql;
			$total = '';
			$dias = '';
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$total .=  $res['total'].',';
				$dias .= "'".$res['data']."',";
			 }
			 $total = substr($total, 0, -1);
			 
			 $dias = substr($dias, 0, -1);
?>



<div id="container_consultas_data" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_consultas_data', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Consultas por dia'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: [<?php echo $dias; ?>]
		},
		yAxis: {
			title: {
				text: 'Dias'
			}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Consultas',
			data: [<?php echo $total; ?>]
		}]/*, {
			name: 'London',
			data: [60, 80, 55]
		}]*/
	});
</script>

