<?php

			$addsql = '';
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND data BETWEEN '$de' AND '$ate'";
			}

			$sql = "SELECT sum(valor) as total, sum(valor_medico) as medico, sum(valor_clinica) as clinica FROM consulta where status IN (3,4,6) $addsql";
			$string = "";
			//echo $sql;
			
			
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$totalfinal = $res['total'];
				$pctmed = ($res['medico'] * 100) / $totalfinal;
				$pctmed = number_format($pctmed, 2, '.', '');
				$pctcli = ($res['clinica'] * 100) / $totalfinal;
				$pctcli = number_format($pctcli, 2, '.', '');
				$string = "{name: 'Renda médico',y: ".$pctmed."},{name: 'Renda clínica',y: ".$pctcli."}";
			 }
?>



<div id="container_consulta_clinica_medicos" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_consulta_clinica_medicos', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de renda de consultas'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>

