<?php
	
			$addsql = '';
			$string = "";
			if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND data BETWEEN '$de' AND '$ate'";
			}
			
			$sql = "SELECT nota, count(nota) as totalnota FROM `pesquisa_satisfacao` WHERE nota > 0 $addsql group by nota";
			//echo $sql;
			$resultadotot = mysqli_query($conexao, $sql);
			$total = 0;
			while($restot = mysqli_fetch_assoc($resultadotot)){
				$total++;
			 }
			//echo $total;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				
				$pct = ($res['totalnota'] * 100) / $total;
				$pct = number_format($pct, 2, '.', '');
				$string .= "{name: 'Nota ".$res["nota"]."',y: ".$pct."},";
				$total = $res["totalnota"];
			 }
			 $string = substr($string, 0, -1);
			 //echo $string;
?>

<div id="container_satisfacao_notas" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/export-data.js"></script>
<script>


	Highcharts.chart('container_satisfacao_notas', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '% de satisfação por nota'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [<?php echo $string; ?>]
    }]
});
	
	
</script>
