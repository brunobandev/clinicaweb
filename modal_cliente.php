<div class="modal modalcliente">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">
        	Paciente
            <?php if($_SESSION['tipo']=='1'||$_SESSION['tipo']=='2'){ ?>
        	<div class="btprontuario">Novo prontuário</div>
            <div class="btreceituario">Novo receituário</div> 
            <?php } ?>   
        </div>
        <div class="corpomodal">
        	
            <div class="linha top10 fotocliente" style="height:100px;">
                <img src="" class="imgpaciente" />
            </div>
        
        	<div class="linha top10">
            	<div class="div25">
                    <div class="label">CPF</div>
                    <div class="inputform"><input type="text" id="cpf" /></div>
                </div>
                
                <div class="div50">
                    <div class="label">Nome</div>
                    <div class="inputform"><input type="text" id="nomecliente" /></div>
                </div>
                
                <?php if($_SESSION['tipo']=='1'||$_SESSION['tipo']=='3'){ ?>
                <div class="div25">
                	<div class="label">Origem</div>
                    <div class="inputform">
                    	<select id="origemcliente">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM clientes_origem ORDER BY origem";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$origem = $res['origem'];
									echo "<option value='".$id."'>".utf8_encode($origem)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
                <?php } ?>
            </div>
            
            <?php if($_SESSION['tipo']=='1'||$_SESSION['tipo']=='3'){ ?>
            <div class="linha top10">
            	<div class="div25">
                    <div class="label">Data de Nascimento</div>
                    <div class="inputform"><input type="text" id="datanascimento" /></div>
                </div>
                <div class="div25">
                    <div class="label">Telefone</div>
                    <div class="inputform"><input type="tel" id="telefone" required></div>
                </div>
                <div class="div25">
                    <div class="label">Telefone 2</div>
                    <div class="inputform"><input type="tel" id="telefone2" required></div>
                </div>
                <div class="div25">
                    <div class="label">E-mail</div>
                    <div class="inputform"><input type="email" id="email" required/></div>
                </div>
            </div>
            <div class="linha top10">
                <div class="div25 top10">
                    <div class="label">Cidade</div>
                    <div class="inputform"><input type="email" id="cidade" required/></div>
                </div>
                <div class="div50 top10">
                    <div class="label">Endereço</div>
                    <div class="inputform"><input type="email" id="endereco" required/></div>
                </div>
            </div>
            <?php } ?>
        </div>
        
        <?php if($_SESSION['tipo']=='1'||$_SESSION['tipo']=='3'){ ?>
        <div class="linha top10">
            <div class="linha divfoto">
                <div class="linha top10 enviafoto">
                    <div class="btenviar btenviafoto">Enviar foto</div>
                    <form id="formulariofoto" method="post" enctype="multipart/form-data" action="upload/upload_foto.php">
                        <input type="file" id="inputfoto" name="imagem" />
                    </form>
                    <div class="btenviar bttirafoto" id="bttirafoto" style="margin-left:20px;">Capturar foto</div>
                </div>
                <div class="video1"><video id="video" width="320" height="240" autoplay></video></div>
                <div class="video2"><canvas id="canvas" width="320" height="240"></canvas></div>
                <div class="botoesfoto">
                    <div id="snap"><img src="img/icones/camera.png" /></div>
                   
                </div>
            </div>         
        </div>
        
       
        <div class="linha top10">
        	<input type="hidden" id="fotocliente" value="" />
            
        	<input type="button" id="salvarcliente" class="btnsalva" value="Salvar" idc="" acao="cadastrar" />
        </div>
        <?php } ?>
        <div class="linha top10">
        	<div class="historico">
            	
            </div>
        </div>
        
    </div>
</div>
<script type="text/javascript" src="js/webcam.js"></script>
<?php if($_SESSION['tipo']=='2'||$_SESSION['tipo']=='4'){ ?>
<script>$('.modalcliente input').attr('disabled','disabled');</script>
<?php } ?>