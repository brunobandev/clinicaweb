<?php 
	date_default_timezone_set("America/Sao_Paulo");
	setlocale(LC_ALL, 'pt_BR');
	include('_include_token.php');
	include('conexao.php'); 
	if(isset($_GET['data'])){
		$data = $_GET['data'];	
	}else{
		$data = date('Y-m-d');
		//echo $data;	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Pacientes</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js"></script>


<script>
$(document).ready(function(){
	
	setTimeout(function(){
		$('.mascara').fadeOut(400);
	},2000);

	$( "#de" ).datepicker();
    $( "#de" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );
	$( "#ate" ).datepicker();
    $( "#ate" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );

});
</script>

<style>
.tudo{
position: relative;
    float: left;
    margin-bottom: 60px;	
}
input[type=text]{
width:120px;	
}
#de{
margin-left: 40px;	
}
.tdpesq{
	border:none;
	margin: 2px;
	background-color:#F3F3F3;
	padding: 3px 6px;
}
</style>

</head>

<body>


<div class="tudo">
	<?php include('_include_cabecalho.php'); ?>

    <div class="linha top30" style="margin-top:70px;">
    	<form method="post">
    		De: <input type="text" name="de" id="de" /> Até: <input type="text" name="ate" id="ate" /> <input type="submit" value="Buscar" id="busca" /> 
        </form>

    </div>
    <div class="linha titulomodal">
    	<center>
        	<?php
	if($_COOKIE['tipo']==1){
		if(isset($_POST['de'])&&isset($_POST['ate'])){
			//echo "Gráficos de " . $_POST['de'] . " até " . $_POST['ate'] . ".";
			$escde = '';
			$escate = '';
			$click = 0;
		}else{
			$_POST['de'] = date('d/m/Y', strtotime(date('Y-m-d'). ' - 30 days'));
			$_POST['ate'] = date('d/m/Y');
		}
		echo "Gráficos de " . $_POST['de'] . " até " . $_POST['ate'] . ".";
	?>
        </center>
    </div>
    
    <?php include('ajax_relatorios.php'); ?>
    
    <div class="linha top50"><?php include('graficos/consultas_data.php'); ?></div>
    
    <div class="linha">
    	<div class="div50 top30"><?php include('graficos/pizza_consultas_status.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/consultas_status_abs.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/pizza_clientes_cidade.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/clientes_cidade_abs.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/pizza_clientes_idade.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/pizza_consulta_clinica_medico.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/pizza_consultas_medicos.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/consulta_medicos_abs.php'); ?></div>
        <!--<div class="div50 top30"><?php //include('graficos/pizza_tipos_procedimentos.php'); ?></div>-->
        <!--<div class="div50 top30"><?php //include('graficos/renda_tipos_procedimentos_abs.php'); ?></div>-->
        <!--<div class="div50 top30"><?php //include('graficos/pizza_procedimento_clinica_medico.php'); ?></div>-->
        <!--<div class="div50 top30"><?php //include('graficos/renda_procedimentos_clinica_medicos_abs.php'); ?></div>-->
        <div class="div50 top30"><?php include('graficos/pizza_notas_pesquisa.php'); ?></div>
        <div class="div50 top30"><?php include('graficos/notas_pesquisa_abs.php'); ?></div>
    </div>
    <?php } ?>
    
    <div class="linha top30">
		<div class="linha"><div class="titulopagina">Pesquisa de satisfação (Whats)</div></div>
        
        <div class="linha top20">
        	<table>
        	<?php
				$addsql = '';
				if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND pes.data BETWEEN '$de' AND '$ate'";
			}
			
				$sql = "SELECT * FROM pesquisa_satisfacao as pes inner join clientes as cli on cli.telefone = pes.numero where pes.nota > 0 $addsql ORDER BY pes.`data` DESC";
				//echo $sql;
				$resultado = mysqli_query($conexao, $sql);
				while($res = mysqli_fetch_assoc($resultado)){
					?>
                    <tr>
                    	<td class="tdpesq"><?php echo $res['nome']; ?></td>
                        <td class="tdpesq"><?php echo $res['nota']; ?></td>
                        <td class="tdpesq"><?php echo $res['data']; ?></td>
                    </tr>
                    <?php
				}
			?>
            </table>
        </div>
        
        <div class="linha top30" style="margin-bottom: 50px;">
        	<table>
        	<?php
				$addsql = '';
				if(isset($_POST['de'])&&isset($_POST['ate'])){
				$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
				$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";

				$addsql = " AND pes.data BETWEEN '$de' AND '$ate'";
			}
			
				$sql = "SELECT * FROM pesquisa_satisfacao as pes inner join clientes as cli on cli.telefone = pes.numero where pes.mensagem <> '' and pes.nota = 0 $addsql ORDER BY pes.`data` DESC";
				//echo $sql;
				$resultado = mysqli_query($conexao, $sql);
				while($res = mysqli_fetch_assoc($resultado)){
					?>
                    <tr>
                    	<td class="tdpesq"><?php echo $res['nome']; ?></td>
                        <td class="tdpesq"><?php echo $res['mensagem']; ?></td>
                        <td class="tdpesq"><?php echo $res['data']; ?></td>
                    </tr>
                    <?php
				}
			?>
            </table>
        </div>
        
    </div>
    
</div>
<?php include('modais.php'); ?>
</body>
</html>
