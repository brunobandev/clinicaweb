<?php

include('conexao.php');

		$addsql = '';
		if(isset($_POST['de'])&&$_POST['de']!=''&&isset($_POST['ate'])&&$_POST['ate']!=''){
			$de = substr($_POST['de'],6,4)."-".substr($_POST['de'],3,2)."-".substr($_POST['de'],0,2);
			$ate = substr($_POST['ate'],6,4)."-".substr($_POST['ate'],3,2)."-".substr($_POST['ate'],0,2) . " 23:59";
		}else{
			$de = '';
			$ate = '';	
		}
		
			if($de!=''&&$ate!=''){ $addsql = " AND data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			$sql = "SELECT count(id) as total FROM consulta WHERE status = 4 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$totalconsultas = $res['total'];
			 }
		
			if($de!=''&&$ate!=''){ $addsql = " AND data_cadastro BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			$sql = "SELECT count(id) as total FROM clientes WHERE 1=1 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$totalclientes = $res['total'];
			 }
		
			
			
			/////////// valor total somando consultas e procedimentos
			if($de!=''&&$ate!=''){ $addsql = " AND data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			$sql = "SELECT sum(valor) as total FROM consulta WHERE status = 4 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$valortotalconsultas = $res['total'];
			 }
			 
			 if($de!=''&&$ate!=''){ $addsql = " AND cons.data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			 $sql = "SELECT sum(proc.total) as totalproc FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id WHERE cons.status = 4 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$totalproc = $res['totalproc'];
			 }
			 $total = $valortotalconsultas + $totalproc;
			 $valortotal = 'R$ ' . number_format($total, 2, ',', '.');
			 //////////////////////////////////////////
			 
			 
			 /////////// valor total somando consultas e procedimentos NÃO DEDUTÍVEL
			if($de!=''&&$ate!=''){ $addsql = " AND data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			$sql = "SELECT sum(valor_dinheiro) as total FROM consulta WHERE status = 4 AND nota_fiscal = 0 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$valortotalconsultasnd = $res['total'];
			 }
			 
			 if($de!=''&&$ate!=''){ $addsql = " AND cons.data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			 $sql = "SELECT sum(proc.dinheiro) as totalproc FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id WHERE cons.status = 4 AND proc.nota = 0 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$totalprocnd = $res['totalproc'];
			 }
			 $totalnd = $valortotalconsultasnd + $totalprocnd;
			 $valortotalnd = 'R$ ' . number_format($totalnd, 2, ',', '.');
			 //////////////////////////////////////////
			 
			  /////////// valor total somando consultas e procedimentos DEDUTÍVEL
			if($de!=''&&$ate!=''){ $addsql = " AND data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			$sql = "SELECT sum(valor_cartao) as totalcartao FROM consulta WHERE status = 4 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$valortotalcart = $res['totalcartao'];
			 }

			$sql = "SELECT sum(valor_dinheiro) as totaldinheiro FROM consulta WHERE status = 4 AND nota_fiscal = 1 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			if($res = mysqli_fetch_assoc($resultado)){
				$valortotaldinnota = $res['totaldinheiro'];
			 }
			 
			 if($de!=''&&$ate!=''){ $addsql = " AND cons.data BETWEEN '$de' AND '$ate'";}
			else{ $addsql = ''; }
			 $sql = "SELECT sum(proc.cartao) as totalproc FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id WHERE cons.status = 4 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$totalproccart = $res['totalproc'];
			 }

			 $sql = "SELECT sum(proc.dinheiro) as totalproc FROM procedimentos_realizados as proc inner join consulta as cons on proc.id_consulta = cons.id WHERE cons.status = 4 AND proc.nota = 1 $addsql";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$totalprocdinnota = $res['totalproc'];
			 }
			 
			 $totald = $valortotalcart + $valortotaldinnota + $totalproccart + $totalprocdinnota;
			 $valortotald = 'R$ ' . number_format($totald, 2, ',', '.');
			 //////////////////////////////////////////

?>

<div class="linha top50">
    
    	<div class="qrelatorio">
        	<div class="linha titrelatorio">Total de consultas realizadas</div>
            <div class="linha top20 numrelatorio relconsultas"><?php echo $totalconsultas; ?></div>
        </div>
        
        <div class="qrelatorio">
        	<div class="linha titrelatorio">Total de clientes cadastrados</div>
            <div class="linha top20 numrelatorio relclientes"><?php echo $totalclientes; ?></div>
        </div>
        
        <div class="qrelatorio">
        	<div class="linha titrelatorio">Valor total</div>
            <div class="linha top20 numrelatorio relvalor"><?php echo $valortotal; ?></div>
        </div>
        
        <div class="qrelatorio">
        	<div class="linha titrelatorio">Valor total não dedutível</div>
            <div class="linha top20 numrelatorio relvalor"><?php echo $valortotalnd; ?></div>
        </div>
        
        <div class="qrelatorio">
        	<div class="linha titrelatorio">Valor total dedutível</div>
            <div class="linha top20 numrelatorio relvalor"><?php echo $valortotald; ?></div>
        </div>
        
    </div>
