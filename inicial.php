<?php
    date_default_timezone_set("America/Sao_Paulo");
    setlocale(LC_ALL, 'pt_BR');
    include('_include_token.php');
    //include('conexao.php');
    if (isset($_GET['data'])) {
        $data = $_GET['data'];
    } else {
        $data = date('Y-m-d');
    }
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Clínica da cirurgia geral LTDA - Pacientes</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
	<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
	<link href="css/estilo.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.mask.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<script>
		setTimeout(function() {
			$('.mascara').fadeOut(400);
		}, 2000);
	</script>
</head>

<body>
	<div class="tudo">
		<?php include('_include_cabecalho.php'); ?>
		<div class="linha bemvindo top40 centro">Bem vindo(a) <?php echo $_COOKIE["nome"]; ?>. </div>
		<div class="linha info top20 centro">Utilize o menu acima para navegar.</div>
	</div>
	<?php //include('modais.php');?>
</body>

</html>