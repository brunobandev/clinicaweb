<?php
    date_default_timezone_set("America/Sao_Paulo");
    setlocale(LC_ALL, 'pt_BR');
    include('_include_token.php');
    include('conexao.php');
    if (isset($_GET['data'])) {
        $data = $_GET['data'];
    } else {
        $data = date('Y-m-d');
        //echo $data;
    }
    if (isset($_GET['t'])) {
        $tipocons = $_GET['t'];
    } else {
        if ($_COOKIE["tipo"]==1 || $_COOKIE["tipo"]==3) {
            $tipocons = 'salas';
        } else {
            $tipocons = 'medicos';
        }
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Agenda</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css?54" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js?328"></script>
<script type="text/javascript" src="js/agenda.js?328"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>

<?php if ($_COOKIE["tipo"]==2) { ?>
<style> .consulta{ margin-left: 0px; } </style>
<?php } ?>

</head>

<body>
<div class="mascara"><img src="img/loader1.gif" /></div>
<div class="mascara2"><img src="img/loader1.gif" /></div>
<div class="horasfixas">
	<div class="horafixa">6h</div>
    <div class="horafixa">7h</div>
    <div class="horafixa">8h</div>
    <div class="horafixa">9h</div>
    <div class="horafixa">10h</div>
    <div class="horafixa">11h</div>
    <div class="horafixa">12h</div>
    <div class="horafixa">13h</div>
    <div class="horafixa">14h</div>
    <div class="horafixa">15h</div>
    <div class="horafixa">16h</div>
    <div class="horafixa">17h</div>
    <div class="horafixa">18h</div>
    <div class="horafixa">19h</div>
    <div class="horafixa">20h</div>
    <div class="horafixa">21h</div>
    <div class="horafixa">22h</div>
    <div class="horafixa">23h</div>
</div>

<div class="estruturaag">
    <div class="linhahora" numlinhah="6">
    
    	<!--<div class="minutosconsulta" hora="" minini="" minfim=""></div>
        <div class="minutosconsulta" hora="" minini="" minfim=""></div>
        <div class="minutosconsulta" hora="" minini="" minfim=""></div>
        <div class="minutosconsulta" hora="" minini="" minfim=""></div>-->
        
        <div class="hora" nhora="6"></div>
        <div class="tituloagenda"></div>
        <div class="status"></div>
    </div>
</div>

<div class="tudo">
	<?php include('_include_cabecalho.php'); ?>
	<div class="topoagenda">
    	
        <div class="consultasde">Consultas de:</div>
        <div class="setadia menosdia"><</div>
        <input type="button" id="diaagenda" />
        <div class="setadia maisdia">></div>
        
            <div class="filtrosagenda">
            <?php if ($_COOKIE["tipo"]==1||$_COOKIE["tipo"]==3) { ?>
            <div class="escolhetipo">
                <select id="selectipoag">
                    <option value="">Selecionar Visualização</option>
                    <option value="medicos">Médicos</option>
                    <option value="salas">Salas</option>
                </select>
            </div>
            <?php } ?>
            
            <div class="escolhemedico">
                
                <?php
                if (($_COOKIE["tipo"]==1||$_COOKIE["tipo"]==3) && $tipocons != 'salas') { ?>
                
                <select id="espmed">
                    <option value="">Selecionar Médico</option>
                    <?php
                        $sql = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
                        echo $sql;
                        $resultado = mysqli_query($conexao, $sql);
                        while ($res = mysqli_fetch_assoc($resultado)) {
                            $idmed = $res['id'];
                            $nome = $res['nome']; ?>
                    <option value="<?php echo $idmed; ?>"><?php echo utf8_encode($nome); ?></option>
                    <?php
                        } ?>
                </select>
                <?php }
                    if ($_COOKIE["tipo"]==2) {
                        ?>
                <input type="hidden" id="espmed" value="<?php echo $_COOKIE['idusu']; ?>" />
                <?php
                    } ?>
    
                <?php if (($_COOKIE["tipo"]==1||$_COOKIE["tipo"]==3) && $tipocons == 'salas') { ?>
                
                <select id="espsala">
                    
                </select>
                <?php }
                    if ($_COOKIE["tipo"]==2) {
                        ?>
                <input type="hidden" id="espmed" value="<?php echo $_COOKIE['idusu']; ?>" />
                <?php
                    } ?>         
                
            </div>
            <?php //if($tipocons == 'salas'){?>
            <div class="escolhesede">
                 <select id="espsede">
                    <option value="">Selecionar Sede</option>
                    <?php
                        $sql = "SELECT * FROM sedes ORDER BY id";
                        $resultado = mysqli_query($conexao, $sql);
                        while ($res = mysqli_fetch_assoc($resultado)) {
                            $idsede = $res['id'];
                            $nome = $res['nome'];
                            if ($res['id']==1) {
                                $selec = "selected='selected'";
                            } else {
                                $selec = "";
                            } ?>
                    <option value="<?php echo $idsede; ?>" <?php echo $selec; ?>><?php echo $nome; ?></option>
                    <?php
                        } ?>
                </select>
            </div>
            <?php //}?>
            <div class="ocultacancel">
                <input type="checkbox" id="ocultacanceladas" /> Ocultar canceladas
            </div>
        </div>
    </div>
    <div class="corpoagenda">
        <div class="linhahatual"></div>
        <div class="consultas">
		
            
        </div>
    </div>
    <?php if ($_COOKIE['tipo']==1 || $_COOKIE['tipo']==3) { ?>;
    <div class="agdir"><img src="img/setadir.png" /></div>
    <div class="agesq"><img src="img/setaesq.png" style="margin-left:14px;" /></div>
    <?php } ?>
    <div class="agcima"><img src="img/setacima2.png" style="width: 24px; margin-left: 13px;" /></div>
    <div class="agbaixo"><img src="img/setabaixo2.png" style="width: 24px; margin-left:13px;  margin-top: 17px;" /></div>
</div>
<div class="balaoconsulta">
    <div class="fotobalao"><img src="" /></div>
    <div class="nomebalao"></div>
    <div class="horabalao"></div>
    <div class="motivobalao"></div>
    <div class="setabalao"><img src="img/setabalao.png" /></div>
</div>
<?php include('modais.php'); ?>

<input type="hidden" id="dataag" value="<?php echo $data; ?>" /> 
<input type="hidden" id="tipoag" value="<?php echo $tipocons; ?>" />
<input type="hidden" id="idsede" value="1" />

</body>
</html>
