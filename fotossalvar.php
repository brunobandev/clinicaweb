<?php
    date_default_timezone_set("America/Sao_Paulo");
    setlocale(LC_ALL, 'pt_BR');
    
    include('conexao.php');
    function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return($output_file);
    }
    $encript = date('Ymdis') . rand(0, 1000);
    $imagem = str_replace('data:image/png;base64,', '', $_POST['imagem']);
    base64_to_jpeg($imagem, "upload/fotos/".$encript.".png");
    
    echo $encript . ".png";
    
    $nome = utf8_decode($_POST['nome']);
    $sql = "INSERT INTO fotos_pacientes_capturadas (nome, data) VALUES ('".$nome."', Now())";
    $resultado = mysqli_query($conexao, $sql);
