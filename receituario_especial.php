<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Agenda</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>

<script>
$(document).ready(function(){
	$('td').each(function(){
		console.log($(this).html());
		if($(this).html()==''){ $(this).parent().remove(); }
	});
	copia = $('.lado1').html();
	$('.lado2').html(copia);
});
</script>

<style>
@media print{
	@page {size: landscape}
	@page { margin: 0; }
  body { margin: 1.6cm; }
  .linhasalvapadrao, #imprimerec{ display: none; }
	}
	body{
	font-size: 0.8em;	
	}
table{
    width: 100%;
    border: 0px;
    border-spacing: 0px;	
	border: none;
}
td{

padding: 8px 5px;
margin:0px;	
}
.datareceituario{
position:relative;
float:right;	
}
.lado{
position:relative;
float:left;
width: 48%;
margin: 0px 1%;	
}
</style>

</head>

<body>
    <div class="lado lado1">
        <div class="linha top20 centro"><img src="img/logo.png" /></div>
        <div class="linha top20 direita">
        	<strong>Praça XV de Novembro, 21, conjunto 902, Centro Histórico, Porto Alegre<br />
			Telefone 51 3094.2424<br />
			Clínica Mayo Surgery, CNPJ: 35.399.441/0001-92<br />
			<div class="linha top20"><center><span style="font-size:1.2em;">RECEITUÁRIO  DE CONTROLE ESPECIAL</span></center></strong></div>
        </div>
        <!--<div class="linha top20 centro titulopagina">Receituário</div>-->
        <div class="linha top10">
        <?php
                include('conexao.php');
                $idr = $_GET['idr'];
                $sql = "SELECT r.*, l.nome as nomemed, l.crm as crm, l.especialidade, c.nome as nomepaciente, c.endereco, c.bairro, c.cidade FROM receituarios as r inner join login as l on l.id = r.id_medico inner join clientes as c on c.id = r.id_paciente WHERE r.id = " . $idr;
                //echo $sql;
                $resultado = mysqli_query($conexao, $sql);
                while($res = mysqli_fetch_assoc($resultado)){
                    $data = substr($res['data'],8,2)."/".substr($res['data'],5,2)."/".substr($res['data'],0,4);
                    if(strlen($res['endereco'])>10){
						$endereco = $res['endereco'] . ', ' . $res['bairro'] . ' - ' . $res['cidade'];
					}else{
						$endereco = '';	
					}
                    ?>
                    <div class="intprontuario">
                        
                        <div class="nomeprontuario">Paciente: <?php echo $res['nomepaciente']; ?></div>
                        <div class="nomeprontuario top10">Endereço: <?php echo $endereco; ?></div>
                        <div class="nomeprontuario top20">Prescrição:</div>
                    </div>
                    <div class="intprontuario top20">
                        <table>
                           
                        <?php
                        $sql2 = "SELECT * FROM receituarios_itens where id_receituario = " . $idr;
                       // echo $sql;
                        $resultado2 = mysqli_query($conexao, $sql2);
                        while($res2 = mysqli_fetch_assoc($resultado2)){
                        ?>
                            <tr style="padding-top: 10px;">
                                <td class="celula" style="border-bottom: 1px solid #666;"><?php echo $res2['medicamento']; ?></td>
                                <td class="celula" style="border-bottom: 1px solid #666;"><?php echo $res2['quantidade']; ?></td>
                                <td class="celula" style="border-bottom: 1px solid #666;"><?php echo $res2['frequencia']; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </table>
                    </div>
                    <div class="div96 right top40">
                        <div style="position:relative; float:left;"><?php echo $data; ?></div>
                        <div style="position:relative; float:right; border-top: 1px solid #000; padding-top: 5px;"><?php echo $res['nomemed']; ?>, <?php echo $res['especialidade']; ?> (CRM <?php echo $res['crm']; ?>) </div>
                    </div>
                    
                    
                    <?php
                 }
        ?>
        
        </div>
        <div class="linha top10" style="text-align:center;"><img src="img/rodape_receituario.png" style="width:100%;" /></div>
    </div>
    <div class="lado lado2"></div>
</body>
</html>