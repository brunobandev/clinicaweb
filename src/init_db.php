<?php

/**
 * Settings.
 */
$databaseHost = 'clinica-web-mysql';
$databaseName = 'clinicaweb';
$databaseUser = 'root';
$databasePass = 'root';

/**
 * CREATE THE DATABASE
 */
$pdoDatabase = new PDO('mysql:host=' . $databaseHost, $databaseUser, $databasePass);
$pdoDatabase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdoDatabase->exec('CREATE DATABASE IF NOT EXISTS ' . $databaseName);

/*
 * CREATE THE TABLE
 */
$pdo = new PDO('mysql:host='.$databaseHost.'.;dbname='.$databaseName, $databaseUser, $databasePass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$pdo->exec('DROP TABLE IF EXISTS tipo_usuarios;');
$pdo->exec('CREATE TABLE `tipo_usuarios` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `tipo` varchar(40) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('INSERT INTO tipo_usuarios (id, tipo, data) VALUES (1, "Admin", now())');
$pdo->exec('INSERT INTO tipo_usuarios (id, tipo, data) VALUES (2, "Médico", now())');
$pdo->exec('INSERT INTO tipo_usuarios (id, tipo, data) VALUES (3, "Secretária", now())');
$pdo->exec('INSERT INTO tipo_usuarios (id, tipo, data) VALUES (4, "Técnico", now())');

$pdo->exec('DROP TABLE IF EXISTS login;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `login` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(200) NOT NULL,
    `usuario` varchar(20) NOT NULL,
    `senha` varchar(20) NOT NULL,
    `tipo` int(11) NOT NULL,
    `especialidade` varchar(150) NOT NULL,
    `crm` varchar(10) NOT NULL,
    `permite_orcamento` int(11) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec(
    'INSERT INTO login
    (nome, usuario, senha, tipo, especialidade, crm, permite_orcamento, data) VALUES
    ("Bruno Bandeira", "brunobandev", "123456", 1, "nenhuma", "", true, now())'
);

$pdo->exec(
    'INSERT INTO login
    (nome, usuario, senha, tipo, especialidade, crm, permite_orcamento, data) VALUES
    ("Matheus Andreatta", "matinho", "123456", 1, "nenhuma", "", true, now())'
);

$pdo->exec('DROP TABLE IF EXISTS clientes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `clientes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(255) NOT NULL,
    `cpf` varchar(14) NOT NULL,
    `rg` varchar(15) NOT NULL,
    `telefone` varchar(20) NOT NULL,
    `telefone2` varchar(20) NOT NULL,
    `email` varchar(100) NOT NULL,
    `data_nascimento` date NOT NULL,
    `cidade` varchar(150) NOT NULL,
    `cep` varchar(20) NOT NULL,
    `endereco` varchar(500) NOT NULL,
    `numero` varchar(10) NOT NULL,
    `complemento` varchar(200) NOT NULL,
    `bairro` varchar(200) NOT NULL,
    `obs` varchar(800) NOT NULL,
    `origem` int(11) NOT NULL,
    `foto` varchar(30) NOT NULL,
    `data_cadastro` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS clientes_origem;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `clientes_origem` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `origem` varchar(100) NOT NULL,
    `datacadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS consulta;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `consulta` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_cliente` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `id_sede` int(11) NOT NULL,
    `id_sala` int(11) NOT NULL,
    `nome` varchar(200) NOT NULL,
    `status` int(11) NOT NULL,
    `data` datetime NOT NULL,
    `hini` varchar(5) NOT NULL,
    `hfim` varchar(5) NOT NULL,
    `doenca` int(11) NOT NULL,
    `obs` varchar(1600) NOT NULL,
    `orcamento_cirurgico` varchar(800) NOT NULL,
    `forma_pagamento` int(11) NOT NULL,
    `plano_saude` int(11) NOT NULL,
    `parcelamento` int(11) NOT NULL,
    `nota_fiscal` int(11) NOT NULL,
    `valor` float NOT NULL,
    `valor_dinheiro` float NOT NULL,
    `valor_cartao` float NOT NULL,
    `valor_parcelado` float NOT NULL,
    `valor_clinica` float NOT NULL,
    `valor_medico` float NOT NULL,
    `justificativa` varchar(600) NOT NULL,
    `tipoconsulta` varchar(10) NOT NULL,
    `datacadastro` datetime NOT NULL,
    `id_usuario` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS consultas_valores;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `consultas_valores` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `tipo` varchar(12) NOT NULL,
    `valor` float NOT NULL,
    `medico` float NOT NULL,
    `clinica` float NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS contato;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `contato` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `contato` varchar(800) NOT NULL,
    `id_cliente` int(11) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS doencas;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `doencas` (
    `cid` varchar(6) DEFAULT NULL,
    `doenca` varchar(281) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8');

$pdo->exec('DROP TABLE IF EXISTS ecografia_pacientes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `ecografia_pacientes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_paciente` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `nome` varchar(255) NOT NULL,
    `texto` text NOT NULL,
    `datacadastro` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS estoque_categorias;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `estoque_categorias` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `categoria` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS estoque_itens;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `estoque_itens` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `categoria` int(11) NOT NULL,
    `nome` varchar(200) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS estoque_itens_quantidade;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `estoque_itens_quantidade` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_item` int(11) NOT NULL,
    `sede` int(11) NOT NULL,
    `minimo` int(11) NOT NULL,
    `total` int(11) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS etapa_cliente_whats;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `etapa_cliente_whats` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `numero` varchar(10) NOT NULL,
    `id_cliente` int(11) NOT NULL,
    `status` int(11) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS formas_pagamento;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `formas_pagamento` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `forma_pagamento` varchar(100) NOT NULL,
    `parcelamento` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS fotos_pacientes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `fotos_pacientes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_paciente` int(11) NOT NULL,
    `foto` varchar(100) NOT NULL,
    `datacadastro` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS fotos_pacientes_capturadas;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `fotos_pacientes_capturadas` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(200) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS fotos_prontuarios;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `fotos_prontuarios` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_paciente` int(11) NOT NULL,
    `id_consulta` int(11) NOT NULL,
    `datacadastro` datetime NOT NULL,
    `img` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS mascaras_ecografia_padroes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `mascaras_ecografia_padroes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_medico` int(11) NOT NULL,
    `nome` varchar(255) NOT NULL,
    `nomeimpressao` varchar(255) NOT NULL,
    `texto` text NOT NULL,
    `datacadastro` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS medicacoes_cronicas;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `medicacoes_cronicas` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_paciente` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `medicamento` varchar(255) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS medicos_planos;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `medicos_planos` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_medico` int(11) NOT NULL,
    `id_plano` int(11) NOT NULL,
    `data_cadastro` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS permissoes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `permissoes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_tipo` int(11) NOT NULL,
    `login` int(11) NOT NULL,
    `clientes` int(11) NOT NULL,
    `medicos` int(11) NOT NULL,
    `planossaude` int(11) NOT NULL,
    `doencas` int(11) NOT NULL,
    `procedimentos` int(11) NOT NULL,
    `financeiro` int(11) NOT NULL,
    `estoque` int(11) NOT NULL,
    `agenda` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS pesquisa_satisfacao;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `pesquisa_satisfacao` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `numero` varchar(10) NOT NULL,
    `id_cliente` int(11) NOT NULL,
    `nota` int(11) NOT NULL,
    `mensagem` varchar(500) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS planos_saude;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `planos_saude` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `plano` varchar(140) NOT NULL,
    `total` float NOT NULL,
    `medico` float NOT NULL,
    `clinica` float NOT NULL,
    `honorario` float NOT NULL,
    `coparticipacao` float NOT NULL,
    `coparticipacaoclinica` float NOT NULL,
    `coparticipacaomedico` float NOT NULL,
    `datacadastro` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS procedimentos;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `procedimentos` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `procedimento` varchar(200) NOT NULL,
    `total` float NOT NULL,
    `medico` float NOT NULL,
    `clinica` float NOT NULL,
    `ativo` int(11) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS procedimentos_realizados;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `procedimentos_realizados` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_consulta` int(11) NOT NULL,
    `id_procedimento` int(11) NOT NULL,
    `total` float NOT NULL,
    `dinheiro` float NOT NULL,
    `cartao` float NOT NULL,
    `nota` int(11) NOT NULL,
    `medico` float NOT NULL,
    `clinica` float NOT NULL,
    `justificativa` varchar(600) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS prontuario;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `prontuario` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_consulta` int(11) NOT NULL,
    `id_paciente` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `motivo` text NOT NULL,
    `cid` varchar(11) NOT NULL,
    `laudo_ecografia` text NOT NULL,
    `subjetivo` text NOT NULL,
    `objetivo` text NOT NULL,
    `exames` text NOT NULL,
    `impressao` text NOT NULL,
    `conduta` text NOT NULL,
    `inicio` datetime NOT NULL,
    `fim` datetime NOT NULL,
    `tempo` varchar(10) NOT NULL,
    `datacadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS receituario;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `receituario` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_paciente` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `receita` text NOT NULL,
    `tipo` int(11) NOT NULL,
    `datacadastro` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS receituarios;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `receituarios` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_paciente` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `tipo` int(11) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS receituarios_itens;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `receituarios_itens` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_receituario` int(11) NOT NULL,
    `medicamento` varchar(150) NOT NULL,
    `quantidade` varchar(50) NOT NULL,
    `frequencia` varchar(150) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS receituarios_padroes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `receituarios_padroes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_receituario` int(11) NOT NULL,
    `id_medico` int(11) NOT NULL,
    `nome` varchar(200) NOT NULL,
    `data` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS salas;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `salas` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(255) NOT NULL,
    `id_sede` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS sala_mes_medico;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `sala_mes_medico` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `ano` int(11) NOT NULL,
    `mes` int(11) NOT NULL,
    `turno` int(11) NOT NULL,
    `diasemana` int(11) NOT NULL,
    `sala` int(11) NOT NULL,
    `medico` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS sala_turno_medico;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `sala_turno_medico` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `sala` int(11) NOT NULL,
    `nomedia` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
    `diasemana` int(11) NOT NULL,
    `medico` int(11) NOT NULL,
    `turno` int(11) NOT NULL,
    `data` datetime NOT NULL,
    `sede` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8');

$pdo->exec('DROP TABLE IF EXISTS sedes;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `sedes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(255) NOT NULL,
    `endereco` varchar(255) NOT NULL,
    `telefone` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

$pdo->exec('DROP TABLE IF EXISTS status_consulta;');
$pdo->exec('CREATE TABLE IF NOT EXISTS `status_consulta` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `status` varchar(100) NOT NULL,
    `ordem` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1');

echo 'Ding!';
