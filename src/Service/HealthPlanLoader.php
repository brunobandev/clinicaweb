<?php

namespace Service;

use Repository\HealthPlanRepositoryInterface;
use Model\HealthPlan;

class HealthPlanLoader
{
    private $healthPlanRepository;

    public function __construct(HealthPlanRepositoryInterface $healthPlanRepository)
    {
        $this->healthPlanRepository = $healthPlanRepository;
    }

    public function getAll()
    {
        $healthPlansData = $this->healthPlanRepository->getAll();
        
        $healthPlans = [];
        foreach ($healthPlansData as $healthPlanData) {
            $healthPlans[] = $this->toObject($healthPlanData);
        }

        return $healthPlans;
    }

    public function getById(int $id)
    {
        $healthPlanData = $this->healthPlanRepository->getById($id);
        return $this->toObject($healthPlanData);
    }

    public function store(array $data)
    {
        $healthPlan = $this->toObject($data);
        return $this->healthPlanRepository->store($healthPlan);
    }

    public function update(array $data, int $id)
    {
        $healthPlan = $this->toObject($data);
        return $this->healthPlanRepository->update($healthPlan, $id);
    }

    public function delete(int $id)
    {
        return $this->healthPlanRepository->delete($id);
    }

    public function toObject(array $healthPlanData)
    {
        $healthPlan = new HealthPlan();

        if ($healthPlanData['id']) {
            $healthPlan->setId($healthPlanData['id']);
        }

        $healthPlan->setName($healthPlanData['plano']);
        $healthPlan->setTotalPrice($healthPlanData['total']);
        $healthPlan->setDoctorPrice($healthPlanData['medico']);
        $healthPlan->setClinicPrice($healthPlanData['clinica']);
        $healthPlan->setHonorary($healthPlanData['honorario']);
        $healthPlan->setCoparticipation($healthPlanData['coparticipacao']);
        $healthPlan->setDoctorCoparticipation($healthPlanData['coparticipacaomedico']);
        $healthPlan->setClinicCoparticipation($healthPlanData['coparticipacaoclinica']);

        return $healthPlan;
    }
}
