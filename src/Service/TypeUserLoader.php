<?php

namespace Service;

class TypeUserLoader
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getTypeUsers()
    {
        $typeUsersData = $this->queryForTypeUsers();
        
        $typeUsers = [];
        foreach ($typeUsersData as $typeUserData) {
            $typeUsers[] = $this->createTypeUserFromData($typeUserData);
        }

        return $typeUsers;
    }

    private function createTypeUserFromData(array $typeUserData)
    {
        $typeUser = new TypeUser();
        $typeUser->setId($typeUserData['id']);
        $typeUser->setDescription($typeUserData['tipo']);
        $typeUser->setCreatedAt($typeUserData['data']);

        return $typeUser;
    }

    private function queryForTypeUsers()
    {
        $pdo = $this->getPDO();
        $statement = $pdo->prepare('SELECT * FROM tipo_usuarios WHERE id != :id');
        $statement->execute(['id' => TypeUser::USER_TYPE_ADMIN]);
        
        $usersArray = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $usersArray;
    }

    private function getPDO()
    {
        return $this->pdo;
    }
}
