<?php

namespace Service;

use PDO;
use Repository\UserRepository;
use Repository\HealthPlanRepository;

class Container
{
    private $configuration;
    private $pdo;
    
    private $typeUserLoader;

    private $userLoader;
    private $userRepository;

    private $healthPlanLoader;
    private $healthPlanRepository;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    public function getPDO()
    {
        if ($this->pdo === null) {
            $this->pdo = new PDO(
                $this->configuration['db_host'],
                $this->configuration['db_user'],
                $this->configuration['db_pass']
            );
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $this->pdo;
    }

    public function getTypeUserLoader()
    {
        if ($this->typeUserLoader === null) {
            $this->typeUserLoader = new TypeUserLoader($this->getPDO());
        }

        return $this->typeUserLoader;
    }

    /**
     * USERS.
     */
    public function getUserLoader()
    {
        if ($this->userLoader === null) {
            $this->userLoader = new UserLoader($this->getUserRepository());
        }

        return $this->userLoader;
    }

    public function getUserRepository()
    {
        if ($this->userRepository === null) {
            $this->userRepository = new UserRepository($this->getPDO());
        }

        return $this->userRepository;
    }

    /**
     * HEALTH PLANS.
     */
    public function getHealthPlanLoader()
    {
        if ($this->healthPlanLoader === null) {
            $this->healthPlanLoader = new HealthPlanLoader($this->getHealthPlanRepository());
        }

        return $this->healthPlanLoader;
    }

    public function getHealthPlanRepository()
    {
        if ($this->healthPlanRepository === null) {
            $this->healthPlanRepository = new HealthPlanRepository($this->getPDO());
        }

        return $this->healthPlanRepository;
    }
}
