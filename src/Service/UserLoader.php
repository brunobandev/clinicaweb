<?php

namespace Service;

use Model\User;
use Repository\UserRepositoryInterface;

class UserLoader
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUsers()
    {
        $usersData = $this->userRepository->fetchAllUsersData();
        
        $users = [];
        foreach ($usersData as $userData) {
            $users[] = $this->createUserFromData($userData);
        }

        return $users;
    }

    public function findOneById($id)
    {
        $userArray = $this->userRepository->fetchSingleUserData($id);

        return $this->createUserFromData($userArray);
    }

    public function login($username, $password)
    {
        $userArray = $this->userRepository->getLogin($username, $password);
        
        if ($userArray) {
            $user = $this->createUserFromData($userArray);
            
            setcookie('idusu', $user->getId(), time() + 12 * 3600);
            setcookie('tipo', $user->getType(), time() + 12 * 3600);
            setcookie('nome', $user->getName(), time() + 12 * 3600);

            return true;
        }

        return false;
    }

    public function createUserFromData(array $userData)
    {
        $user = new User();
        $user->setId($userData['id']);
        $user->setName($userData['nome']);
        $user->setUsername($userData['usuario']);
        $user->setType($userData['tipo']);

        return $user;
    }
}
