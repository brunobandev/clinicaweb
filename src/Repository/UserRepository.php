<?php

namespace Repository;

use PDO;

class UserRepository implements UserRepositoryInterface
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function fetchAllUsersData()
    {
        $pdo = $this->pdo;
        $statement = $pdo->prepare('SELECT * FROM login');
        $statement->execute();
        
        $usersArray = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $usersArray;
    }

    public function fetchSingleUserData($id)
    {
        $pdo = $this->pdo;
        $statement = $pdo->prepare('SELECT * FROM login WHERE id = :id');
        $statement->execute(['id' => $id]);
        
        $userArray = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$userArray) {
            return null;
        }

        return $userArray;
    }

    public function getLogin($username, $password)
    {
        $pdo = $this->pdo;
        $statement = $pdo->prepare('SELECT * FROM login WHERE usuario = :username AND senha = :password');
        $statement->execute(['username' => $username, 'password' => $password]);
        
        $userData = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$userData) {
            return null;
        }
        
        return $userData;
    }
}
