<?php

namespace Repository;

use Model\HealthPlan;

interface HealthPlanRepositoryInterface
{
    public function getAll();
    public function getById(int $id);
    public function store(HealthPlan $healthPlan);
    public function update(HealthPlan $healthPlan, int $id);
    public function delete(int $id);
}
