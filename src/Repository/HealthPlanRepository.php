<?php

namespace Repository;

use PDO;
use Model\HealthPlan;

class HealthPlanRepository implements HealthPlanRepositoryInterface
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getAll()
    {
        $pdo = $this->pdo;
        $statement = $pdo->prepare('SELECT * FROM planos_saude');
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById(int $id)
    {
        $pdo = $this->pdo;
        $statement = $pdo->prepare('SELECT * FROM planos_saude WHERE id = :id ORDER BY plano');
        $statement->execute(['id' => $id]);
        
        $healthPlanArray = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$healthPlanArray) {
            return null;
        }

        return $healthPlanArray;
    }

    public function store(HealthPlan $healthPlan)
    {
        $pdo = $this->pdo;
        $sql = 'INSERT INTO planos_saude 
            (plano, total, honorario, medico, clinica, coparticipacao, coparticipacaoclinica, coparticipacaomedico, datacadastro) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, now())';
        $statement = $pdo->prepare($sql);
        $statement->execute([
            $healthPlan->getName(),
            $healthPlan->getTotalPrice(),
            $healthPlan->getHonorary(),
            $healthPlan->getDoctorPrice(),
            $healthPlan->getClinicPrice(),
            $healthPlan->getCoparticipation(),
            $healthPlan->getClinicCoparticipation(),
            $healthPlan->getDoctorCoparticipation()
        ]);

        return $statement->lastInsertId;
    }

    public function update(HealthPlan $healthPlan, int $id)
    {
        $pdo = $this->pdo;
        $sql = 'UPDATE planos_saude SET plano = ?, total = ?, honorario = ?, 
            medico = ?, clinica = ?, coparticipacao = ?, coparticipacaoclinica = ?, coparticipacaomedico = ? 
            WHERE id = ?';
        $statement = $pdo->prepare($sql);
        $statement->execute([
            $healthPlan->getName(),
            $healthPlan->getTotalPrice(),
            $healthPlan->getHonorary(),
            $healthPlan->getDoctorPrice(),
            $healthPlan->getClinicPrice(),
            $healthPlan->getCoparticipation(),
            $healthPlan->getClinicCoparticipation(),
            $healthPlan->getDoctorCoparticipation(),
            $id
        ]);

        return $statement->rowCount();
    }

    public function delete(int $id)
    {
        $pdo = $this->pdo;
        $statement = $pdo->prepare('DELETE FROM planos_saude WHERE id = :id');
        $statement->execute(['id' => $id]);

        return $statement->rowCount();
    }
}
