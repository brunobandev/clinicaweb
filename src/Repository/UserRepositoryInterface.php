<?php

namespace Repository;

interface UserRepositoryInterface
{
    public function fetchAllUsersData();
    public function fetchSingleUserData($id);
    public function getLogin($username, $password);
}
