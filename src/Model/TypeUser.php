<?php

namespace Model;

class TypeUser
{
    private $id;
    private $description;
    private $createdAt;

    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_DOCTOR = 2;
    const USER_TYPE_SECRETARY = 3;
    const USER_TYPE_TECHNICIAR = 4;

    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDescription()
    {
        return utf8_encode($this->description);
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
