<?php

namespace Model;

class HealthPlan
{
    private $id;
    private $name;
    private $totalPrice;
    private $doctorPrice;
    private $clinicPrice;
    private $honorary;
    private $coparticipation;
    private $doctorCoparticipation;
    private $clinicCoparticipation;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    public function getDoctorPrice()
    {
        return $this->doctorPrice;
    }

    public function setDoctorPrice($doctorPrice)
    {
        $this->doctorPrice = $doctorPrice;
    }

    public function getClinicPrice()
    {
        return $this->clinicPrice;
    }

    public function setClinicPrice($clinicPrice)
    {
        $this->clinicPrice = $clinicPrice;
    }

    public function getHonorary()
    {
        return $this->honorary;
    }

    public function setHonorary($honorary)
    {
        $this->honorary = $honorary;
    }

    public function getCoparticipation()
    {
        return $this->coparticipation;
    }

    public function setCoparticipation($coparticipation)
    {
        $this->coparticipation = $coparticipation;
    }
 
    public function getDoctorCoparticipation()
    {
        return $this->doctorCoparticipation;
    }

    public function setDoctorCoparticipation($doctorCoparticipation)
    {
        $this->doctorCoparticipation = $doctorCoparticipation;
    }
 
    public function getClinicCoparticipation()
    {
        return $this->clinicCoparticipation;
    }

    public function setClinicCoparticipation($clinicCoparticipation)
    {
        $this->clinicCoparticipation = $clinicCoparticipation;
    }
}
