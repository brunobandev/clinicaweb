<div class="modal modalvalorconsulta">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar valor de consulta</div>
        <div class="corpomodal">
            <?php
			
			$novavalor = '';
			$novamedico = '';
			$novaclinica = '';
			$retornovalor = '';
			$retornomedico = '';
			$retornoclinica = '';
			
			$sql = "SELECT * FROM consultas_valores WHERE tipo = 'NOVA'";
			$resultado = mysqli_query($conexao, $sql);
			//echo $sql;
			if($res = mysqli_fetch_array($resultado)) {
				$novavalor = $res['valor'];
				$novamedico = $res['medico'];
				$novaclinica = $res['clinica'];
			}
			
			$sql = "SELECT * FROM consultas_valores WHERE tipo = 'RETORNO'";
			$resultado = mysqli_query($conexao, $sql);
			//echo $sql;
			if($res = mysqli_fetch_array($resultado)) {
				$retornovalor = $res['valor'];
				$retornomedico = $res['medico'];
				$retornoclinica = $res['clinica'];
			}
			?>
            <div class="linha top20">
            	<div class="div25">
                    <div class="label">Nova total</div>
                    <div class="inputform"><input type="text" id="novatotal" value="<?php echo $novavalor; ?>" /></div>
                </div>
                <div class="div25">
                    <div class="label">Nova clínica</div>
                    <div class="inputform"><input type="text" id="novaclinica" value="<?php echo $novaclinica; ?>" /></div>
                </div>
                <div class="div25">
                    <div class="label">Nova médico</div>
                    <div class="inputform"><input type="text" id="novamedico" value="<?php echo $novamedico; ?>" /></div>
                </div>
            </div>
            
            <div class="linha top20">
            	<div class="div25">
                    <div class="label">Retorno total</div>
                    <div class="inputform"><input type="text" id="retornototal" value="<?php echo $retornovalor; ?>" /></div>
                </div>
                <div class="div25">
                    <div class="label">Retorno clínica</div>
                    <div class="inputform"><input type="text" id="retornoclinica" value="<?php echo $retornoclinica; ?>" /></div>
                </div>
                <div class="div25">
                    <div class="label">Retorno médico</div>
                    <div class="inputform"><input type="text" id="retornomedico" value="<?php echo $retornomedico; ?>" /></div>
                </div>
            </div>
            
   		</div>
        <div class="linha top20">
        	<input type="button" id="salvarvalorconsulta" class="btnsalva" value="Salvar" />
        </div>
    </div>
</div>