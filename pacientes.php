<?php 
	date_default_timezone_set("America/Sao_Paulo");
	setlocale(LC_ALL, 'pt_BR');
	include('_include_token.php');
	include('conexao.php'); 
	if(isset($_GET['data'])){
		$data = $_GET['data'];	
	}else{
		$data = date('Y-m-d');
		//echo $data;	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Pacientes</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<!--<script type="text/javascript" src="js/agenda.js"></script>-->

<script>
setTimeout(function(){
	$('.mascara').fadeOut(400);
},2000);
</script>

</head>

<body>

<?php if($_COOKIE["tipo"]==1 || $_COOKIE["tipo"]==3){ ?>
<div class="tudo">
	<?php include('_include_cabecalho.php'); ?>
	<div class="linha centro top20 titulopagina">
   		Gerenciamento de pacientes
    </div>
    
    <?php if($_COOKIE["tipo"]==1||$_COOKIE["tipo"]==3){ ?>
   <!-- <div class="linha top20">
    	<div class="submenu">
            <div class="subitem" usun="1">Cadastrar paciente</div>
        </div>
    </div> -->
    <?php } ?>
    
    <div class="linha centro top20">
    	<input type="text" id="buscapacientes" placeholder="Buscar paciente" />
    </div>
    
    <div class="linha top20 listapacientes">
    	
    </div>
    
</div>

<?php include('modais.php'); ?>

<input type="hidden" id="dataag" value="<?php echo $data; ?>" /> 
<?php } ?>
</body>
</html>
