<div class="modal modalconsulta">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">
        	Consulta
            <div class="btvercliente">Dados do Cliente</div>
        </div>
        <div class="corpomodal">
        	
            <div class="linha top10 fotocliente" style="height:100px;">
                <img src="" class="imgpaciente" />
            </div>
            
        	<div class="linha top10">
            	<div class="div50">
                    <div class="label">Paciente</div>
                    <div class="inputform"><input type="text" id="paciente" idcliente="" /><div class="btaddpaciente">+</div></div>
                    <div class="sugestoes"></div>
                </div>
                <div class="div25">
                    <div class="label">Médico</div>
                    <div class="inputform">
                    	<select id="medico">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$nome = $res['nome'];
									echo "<option value='".$id."'>".utf8_encode($nome)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
                <div class="div25">
                	<div class="label">Status</div>
                    <div class="inputform">
                    	<select id="status">
                        	<?php
								$sql = "SELECT * FROM status_consulta ORDER BY ordem";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$status = $res['status'];
									echo "<option value='".$id."'>".$status."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="linha top10">
            	<div class="div25">
                    <div class="label">Data</div>
                    <div class="inputform"><input type="text" id="data" /></div>
                </div>
                <div class="div25">
                    <div class="label">Início</div>
                    <div class="inputform"><input type="time" id="hini"  min="06:00" max="22:00" required></div>
                </div>
                <div class="div25">
                    <div class="label">Fim</div>
                    <div class="inputform"><input type="time" id="hfim" min="06:00" max="22:00" required/></div>
                </div>
            </div>
            
            <div class="linha top10">
            	<div class="div25">
                    <div class="label">Forma de pagamento</div>
                    <div class="inputform">
                    	<select id="forma_pagamento">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM formas_pagamento ORDER BY id";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$forma = $res['forma_pagamento'];
									echo "<option value='".$id."'>".utf8_encode($forma)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
                <div class="div25">
                    <div class="label">Parcelamento</div>
                    <div class="inputform"><input type="tel" id="parcelamento" value="0" style="width:65px;"> x</div>
                </div>
                <div class="div25">
                    <div class="label">Valor</div>
                    <div class="inputform"><input type="text" id="valor" value="0"></div>
                </div>
                <div class="div25">
                    <div class="label">Nota Fiscal</div>
                    <div class="inputform">
                    	<select id="nota">
                        	<option value="1">Sim</option>
                            <option value="0">Não</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="linha top10 linhaplanos">
            	<div class="label">Plano de Saúde</div>
                    <div class="inputform">
                    	<select id="plano_saude">
                        	<option value="">Selecione</option>
                        	
                        </select>
                    </div>
            </div>
            
        </div>
        <div class="linha top10">
        	<div class="label">Observações</div>
        	<textarea id="obsconsulta" style="height:140px;"></textarea>
        </div>
        <div class="linha top10 enviadoc">
            <div class="btenviar btenviadoc">Enviar documento</div>
            <form id="formulario" method="post" enctype="multipart/form-data" action="upload/upload.php">
                <input type="file" id="inputdocumento" name="imagem" />
                <input type="hidden" value="" name="iddoc" id="iddoc" />
            </form>
        </div>
        <div class="linha top10 documentos">
        	<div class="label">Documentos</div>
            <div class="exibedocs"></div>
        </div>
        
        <div class="linha top10">
        	<input type="button" id="salvarconsulta" class="btnsalva" value="Salvar" acaoconsulta="cadastrar" idaltera="" />
        </div>
    </div>
</div>