<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

include('conexao.php');
$idc = $_POST['idconsulta'];
$idp = $_POST['idpaciente'];
    
$diretorio = "upload/prontuarios/".$idc."/";

if (!is_dir($diretorio)) {
    mkdir($diretorio, 0777);
}

if (!is_dir($diretorio)) {
    echo "Pasta $diretorio nao existe";
} else {
    $permitidos = array(".jpg",".jpeg",".gif",".png", ".bmp");
    $arquivo = isset($_FILES['arquivo']) ? $_FILES['arquivo'] : false;

    for ($k = 0; $k < count($arquivo['name']); $k++) {
        $encript = date('Ymdis') . rand(0, 10000);
        $destino = $diretorio.$arquivo['name'][$k];

        $ext = strtolower(strrchr($arquivo['name'][$k], "."));
        if (move_uploaded_file($arquivo['tmp_name'][$k], $destino)) {
            $filename = $destino;
            $exif = exif_read_data($filename);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                echo $orientation;
                if ($orientation != 1) {
                    $img = @ImageCreateFromJpeg($filename);
                    if (!$img) {
                        $img= @imagecreatefromstring(file_get_contents($filename));
                    }
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                        $deg = 180;
                        break;
                        case 6:
                        $deg = 270;
                        break;
                        case 8:
                        $deg = 90;
                        break;
                    }
                    if ($deg) {
                        $img = imagerotate($img, $deg, 0);
                    }
                    imagejpeg($img, $filename, 100);
                }
            }
            
            rename($diretorio."/".$arquivo['name'][$k], $diretorio . "/" . $encript . $ext);
                            
            $idp = utf8_decode($_POST['idpaciente']);
            $idc = utf8_decode($_POST['idconsulta']);
            $sql = "INSERT INTO fotos_prontuarios (img,id_paciente,id_consulta,datacadastro) VALUES ('".$encript.$ext . "','".$idp."','".$idc."',NOW())";
            $resultado = mysqli_query($conexao, $sql);
        }
    } ?>

	<script>location.href="fotos_prontuario.php?idc=<?php echo $idc; ?>&idp=<?php echo $idp; ?>";</script>
<?php
} ?>