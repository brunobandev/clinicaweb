<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

date_default_timezone_set("America/Sao_Paulo");
	setlocale(LC_ALL, 'pt_BR');
	include('_include_token.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Organização mensal de salas - Clínica da Cirurgia</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="css/estilo.css?54" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js?327"></script>

<style>
td{
padding: 5px;	
}
select{
width: 150px;	
}
.cabec{
	text-align:center;
	color:#03C;
	font-size: 1.3em;
}
.tabturno{
text-align:right;
	color:#03C;
	font-size: 1.3em;
}
</style>



<script>
$(document).ready(function(){
	
	$( "#diasala" ).datepicker({
  dateFormat: "dd/mm/yy"
});
	
	$('.salamed').append($('#option_medicos').html());
	
	
	conta = 0;
	
	function carrega(){
		$('.mascara').show();
		setTimeout(function(){
		$('.salamed').each(function(){
			data = $('#diasala').val();
			turno = $(this).attr('turno');
			sala = $('#selsala').val();
			sede = $('#selsede').val();
			alvo = $(this);
			conta++;
			
			$.ajax({
				url : "ajax_salas_medicos.php",
				data : {
					acao : 'diasemana',
					data : data
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					$('.cabec').html(data);
				}
			});
			
			$.ajax({
				url : "ajax_salas_medicos.php",
				data : {
					acao : 'consultadia',
					data : data,
					turno : turno,
					sala : sala,
					sede : sede
				},
				type : "POST",
				//dataType:"json",
				async:false,
				success : function(data) {
					alvo.val(data);
					$('.mascara').hide();
					console.log(data);
				}
			});
			
		});
		},500);
	}
	
	$('.salamed').change(function(){
		dia = $(this).attr('dia');
		turno = $(this).attr('turno');
		mes = $('#selmes').val();
			ano = $('#selano').val();
			sala = $('#selsala').val();
			sede = $('#selsede').val();
		
		medico = $(this).val();
		$.ajax({
			url : "ajax_salas_medicos.php",
			data : {
				acao : 'gravadata',
				data : data,
				turno : turno,
				sala : sala,
				sede : sede,
				medico : medico
			},
			type : "POST",
			//dataType:"json",
			async:false,
			success : function(data) {
				console.log(data);
				//alert('salvo');
			}
		});
	});
	
	function carregasalas(){
		console.log('a');
		$.ajax({
			url : 'ajax_core.php',
			data : {
				acao : 'carregasalas',
				idsede : $('#selsede').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('#selsala').html(data);
			},
			error: function(){
					
			}
		});

	}
	carregasalas();
	
	$('#selsede').change(function(){
		idsede = $(this).val();
		$('#idsede').val(idsede);
		carregasalas();
	});
	
	$('#carrega').click(function(){
		carrega();
	});
	
});
</script>

</head>

<body>
<?php include('_include_cabecalho.php');?>
<div style="display:none;" id="option_medicos">
	<?php
    include('conexao.php');

	$sql2 = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
	$resultado2 = mysqli_query($conexao, $sql2);
	while($res = mysqli_fetch_assoc($resultado2)){
	?>
		<option value="<?php echo $res['id'];?>"><?php echo utf8_encode($res['nome']);?></option>        
    <?php	
	}
	
	?>
</div>
<div class="div96 top100">
<div class="linha titulopagina" style="margin-top:30px;">Organizar salas x médicos x turnos por dia</div>
<div class="linha top20">

<select id="selsede">
	<option value=""> Sede </option>
	<?php
		$sql2 = "SELECT * FROM sedes ORDER BY id";
		$resultado2 = mysqli_query($conexao, $sql2);
		while($res = mysqli_fetch_assoc($resultado2)){
		?>
			<option value="<?php echo $res['id'];?>"><?php echo utf8_encode($res['nome']);?></option>        
		<?php	
		}
	?>
</select>

<select id="selsala">
	<option value=""> Sala </option>
	<?php
		$data = date('d/m/Y');
		$sql2 = "SELECT * FROM salas ORDER BY id";
		$resultado2 = mysqli_query($conexao, $sql2);
		while($res = mysqli_fetch_assoc($resultado2)){
		?>
			<option value="<?php echo $res['id'];?>"><?php echo utf8_encode($res['nome']);?></option>        
		<?php	
		}
	?>
</select>

<input type="text" id="diasala" style="width: 150px;" placeholder="data" value="<?php echo $data; ?>" />

<input type="button" id="carrega" value="Carregar" style="padding: 10px;" />
</div>
<div class="linha top20">
<table>
<tr>
	<td></td>
	<td class="cabec"></td>
</tr>

<tr>
	<td class="tabturno">Manhã</td>
	<td><select class="salamed" data="1" turno="1"><option value="">Selecionar médico</option></select></td>
</tr>

<tr>
	<td class="tabturno">Tarde</td>
	<td><select class="salamed" data="1" turno="2"><option value="">Selecionar médico</option></select></td>
</tr>

<tr>
	<td class="tabturno">Noite</td>
	<td><select class="salamed" data="1" turno="3"><option value="">Selecionar médico</option></select></td>
</tr>


</table>
</div>
<?php



$mes = 1;
$ano = 2020;
$sala = 4;

$dia = 0;
$conta = 0;


		/*$numero = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		
		
			while($dia < $numero){
				if($ano < 2031){
					$turno = 0;
					$dia++;
					$data = $dia."-".$mes."-".$ano;
					$nomedia = date('D', strtotime($data));
					if($nomedia!='Sun'&&$nomedia!='Sat'){
						while($turno<3){
							$conta++;
							$turno++;
							if($dia < 10){ $escdia = '0'.$dia; }
							else{ $escdia = $dia; }
							//echo $conta . ") ". $escdia."-".$mes."-".$ano." (".$nomedia.") - Turno:".$turno."<br>";
							$diacont = $ano."-".$mes."-".$escdia;
							
							$sql2 = "INSERT INTO sala_turno_medico (data,sala,turno,medico,nomedia) VALUES ('".$diacont."',".$sala.",'".$turno."','','".$nomedia."')";	
							echo $sql2;		
							$resultado2 = mysqli_query($conexao, $sql2);
							
						}
					}
					if($turno==3){ $turno = 0; }
					if($dia==$numero){
						$dia = 0;
						if($mes==12){ $mes = 1; $ano++; }
						else{ $mes++; }
					}
				}
			}
		
		//}*/
?>
</div>

</body>
</html>