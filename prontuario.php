<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Agenda</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<link href="css/imgbox.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/agenda.js"></script>
<script type="text/javascript" src="js/imgbox.js"></script>


</head>

<body>
<div class="linha top20 centro"><img src="img/logo.png" /></div>
<?php
		include('conexao.php');
		$idp = $_GET['idp'];
		$sql = "SELECT p.*, d.doenca, l.nome as nomemedico, l.crm, c.nome as nomepaciente, DATE_FORMAT(inicio,'%H:%i:%S') as inicioconsulta, DATE_FORMAT(fim,'%H:%i:%S') as fimconsulta FROM prontuario as p left join doencas as d on p.cid = d.cid inner join login as l on p.id_medico = l.id inner join clientes as c on p.id_paciente = c.id WHERE p.id_paciente = ".$idp." order by datacadastro DESC";
		//echo $sql;
		$resultado = mysqli_query($conexao, $sql);
		while($res = mysqli_fetch_assoc($resultado)){
			$data = substr($res['datacadastro'],8,2)."/".substr($res['datacadastro'],5,2)."/".substr($res['datacadastro'],0,4);
			$inicio = $res['inicioconsulta'];
			$fim = $res['fimconsulta'];
			$tempo = $res['tempo'];
			$idconsulta = $res['id_consulta'];
			?>
            
            <div class="linha top20 centro titulopagina">Prontuário médico</div>
            <div class="foraprontuario top20">
                <div class="intprontuario">
                     <div class="linha top10">
                        <div class="dataprontuário"><?php echo $data; ?></div>
                    </div>
                    <div class="linha top10">
                        <div class="dataprontuário"><?php echo $inicio; ?> - <?php echo $fim; ?></div>
                    </div>
                    <div class="linha top10">
                        <div class="dataprontuário">Tempo da consulta: <?php echo $tempo; ?></div>
                    </div>
                   
                    
                    <div class="linha nomeprontuario top30">Paciente: <?php echo $res['nomepaciente']; ?></div>
                    <div class="linha doencaprontuario top20">CID/Doença: <?php echo $res['cid'] . " - " . $res['doenca']; ?></div>
                    <div class="linha doencaprontuario top20">Médico: <?php echo $res['nomemedico'] . " (" . $res['crm'] . ")"; ?></div>
                    
                </div>
                <div class="intprontuario top40">
                    <div class="qprontuario">
                        <div class="tipoqprontuario">Motivo da Consulta</div>
                        <div class="txtprontuario"><?php echo $res['motivo']; ?></div>
                    </div>
                </div>
                <div class="intprontuario top40">
                    <div class="qprontuario">
                        <div class="tipoqprontuario">Subjetivo</div>
                        <div class="txtprontuario"><?php echo $res['subjetivo']; ?></div>
                    </div>
                </div>
                <div class="intprontuario top40">
                    <div class="qprontuario">
                        <div class="tipoqprontuario">Objetivo</div>
                        <div class="txtprontuario"><?php echo $res['objetivo']; ?></div>
                    </div>
                </div>
                <div class="intprontuario top40">
                    <div class="qprontuario">
                        <div class="tipoqprontuario">Exames</div>
                        <div class="txtprontuario"><?php echo $res['exames']; ?></div>
                    </div>
                </div>
                <div class="intprontuario top40">
                    <div class="qprontuario">
                        <div class="tipoqprontuario">Impressão</div>
                        <div class="txtprontuario"><?php echo $res['impressao']; ?></div>
                    </div>
                </div>
                <div class="intprontuario top20">
                    <div class="qprontuario">
                        <div class="tipoqprontuario">Conduta</div>
                        <div class="txtprontuario"><?php echo $res['conduta']; ?></div>
                    </div>
                </div>
                
                <div class="linha top20">
                    <div class="tipohist" style="margin-left:30px;">Fotos:</div>
                    <div class="exibefotospt" style="margin-left:30px;">
                        <?php
                            $sql2 = "SELECT * FROM fotos_prontuarios WHERE id_consulta = " . $idconsulta . " ORDER BY datacadastro";
                            //echo $sql;
                            $resultado2 = mysqli_query($conexao, $sql2);
                            while($res2 = mysqli_fetch_assoc($resultado2)){
                            ?>
                            <div class="fotopt"><img src="upload/prontuarios/<?php echo $idconsulta; ?>/<?php echo $res2['img']; ?>" class="imgbox" /></div>
                            <?php
                            }
                        ?>
                    </div>
                </div>
                
            </div>
            
            <?php
		 }
?>

</body>
</html>