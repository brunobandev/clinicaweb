// JavaScript Document

$(document).ready(function() {
	$('body').append('<div class="imgboxbg"></div><div class="imgboxabreimg"></div>');
	$('.imgboxbg').slideUp();
	$('.imgboxabreimg').slideUp();
	function abre(img){
		$('html, body').scrollTop(0);
		$('.imgboxbg').slideDown(300);
		$('.imgboxabreimg').slideDown(300);
		$('.imgboxabreimg').html('<img src="../img/rotate-icon.png" class="rodafoto" angulo="0" style="margin-top:10px;" /><div class="fechafoto bglaranja">Fechar</div><div class="legendafoto">1/1</div><div class="fotoanterior bglaranja"><</div><div class="fotoposterior bglaranja">></div><br><img class="fotoaberta" src="'+img+'">');
		fechafoto();
		passafotos();
		ajustafotoGaleria();
		roda();
	}
	function fechafoto(){
		$('.fechafoto').click(function(){
			$('.imgboxabreimg').animate({'top':'-2000px'},600,function(){
				$('.imgboxabreimg').slideUp();
				$('.imgboxbg').slideUp(300);	
			});
		});	
	}
	
	function roda(){
		$('.rodafoto').click(function(){
			ang = parseInt($(this).attr('angulo'));
			if(ang < 270){ ang += 90; }
			else{ ang = 0; }
			$(this).attr('angulo', ang);
			$('.fotoaberta').css({
				"-webkit-transform": "rotate("+ang+"deg)",
				"-moz-transform": "rotate("+ang+"deg)",
				"transform": "rotate("+ang+"deg)" /* For modern browsers(CSS3)  */
			});
		});
	}
	
	var contaimg = 0;
	$('.imgbox').each(function(){
		contaimg++;
		$(this).attr('numimg',contaimg);
	});
	
	function passafotos(){
		$('.fotoposterior').click(function(){
			abrirnumero = $('.imgboxabreimg').attr('aberta');
			if(abrirnumero < contaimg){
				abrirnumero++;
			}else{
				abrirnumero = 1;
			}
			$('.imgboxabreimg').attr('aberta',abrirnumero);
			abre($('[numimg='+abrirnumero+']').attr('src'));
		});
		
		$('.fotoanterior').click(function(){
			abrirnumero = $('.imgboxabreimg').attr('aberta');
			if(abrirnumero > 1){
				abrirnumero--;
			}else{
				abrirnumero = contaimg;	
			}
			$('.imgboxabreimg').attr('aberta',abrirnumero);
			abre($('[numimg='+abrirnumero+']').attr('src'));
		});
	}
	
	$('.imgbox').click(function(){
		$('.imgboxabreimg').attr('aberta',$(this).attr('numimg'));
		abre($(this).attr('src'));
	});
	$('.imgboxbg').click(function(){
		$('.fechafoto').click();
	});
	
	function ajustafotoGaleria(){
		if(document.body.clientWidth < 800){
			$('.imgboxabreimg > img').css({'width':'100%','left':'10px'});
			$('.imgboxabreimg').css({'margin-left':'0px','left':'0px','padding':'1%','width':'98%'});
		}
		else{
			margem = ($('.fotoaberta').width() / 2)*-1;
			$('.imgboxabreimg').animate({'margin-left':margem+'px'},300);	
		}
		$('.imgboxabreimg').animate({'top':'10px'},300);
		alturasetas = ($('.imgboxabreimg > img').height() / 2)-15;
		$('.fotoanterior,.fotoposterior').css('top',alturasetas+'px');
		meio = $('.imgboxabreimg > img').width() / 2;
		$('.legendafoto').text($('.imgboxabreimg').attr('aberta')+"/"+contaimg);
		$('.legendafoto').animate({'margin-left':(meio-10)+'px'},300);

		
	}
});