// JavaScript Document

$(document).ready(function(){
	tipoag = $('#tipoag').val();
	if(tipoag=='salas'){ arquivoag = 'ajax_consultas_salas.php'; }
	else{arquivoag = 'ajax_consultas.php';}
	split = $('#dataag').val().split('-');
	datainvert = split[2] + '/' + split[1] + '/' + split[0];
	$("#diaagenda").datepicker('setDate', datainvert);
	
	$("#diaagenda").change(function(){
      	mudadia();
		console.log('dia');
	});
	
	
	function loader(n){
		if(n==1){
			$('.mascara, .loader').fadeIn(400);	
		}
		if(n==0){
			$('.mascara, .loader').fadeOut(400);
		}
	}
	
	$('.maisdia').click(function(){
		var date_new = $('#diaagenda').datepicker ('getDate'); 
		date_new.setDate (date_new.getDate () + 1); 
		$('#diaagenda').datepicker ('setDate', date_new);
		mudadia();
	});
	$('.menosdia').click(function(){
		var date_new = $('#diaagenda').datepicker ('getDate'); 
		date_new.setDate (date_new.getDate () - 1); 
		$('#diaagenda').datepicker ('setDate', date_new);
		mudadia();
	});
	
	
	function estrutura(){
		$('.mascara').show();
		est = $('.estruturaag').html();
		inicio = 6;
		fim = 23;
		conta = inicio;
		
		while(conta < (fim+1)){
			$('.corpoagenda').append(est);
			conta++;	
		}
		
		$('.estruturaag').remove();
		conta=inicio;
		$('.linhahora').each(function(){
			$(this).attr('numlinhah',conta);
			$(this).children('.hora').attr('nhora',conta);
			$(this).children('.hora').html(conta+'h');
			conta++;
		});

		
		agora();
		carregaconsultas(1);
		
		setTimeout(function(){
			$('.mascara').fadeOut(400);
		},2000);
		
	}

	
	estrutura();
	
	function agora(){
		hoje = new Date();
		horas = hoje.getHours();
		minutos = hoje.getMinutes();
		//console.log(horas+':'+minutos);
		
		//// altura da linha de horas (-180 altura do topo)
		if(horas >=6 && horas<=23){
			alturah = $('[numlinhah='+horas+']').height();
			topom = ((minutos/60)*alturah);
			topoh = $('[numlinhah='+horas+']').offset().top - 180;
			//alert(alturah + ' - ' + topom);
			$('.linhahatual').css({'top':(topoh+topom)+'px'});
		}
	}
	
	function mudadia(){
		dia = $('#diaagenda').val();
		splitd = dia.split('/');
		datainv = splitd[2] + '-' + splitd[1] + '-' + splitd[0]; 
		$('#dataag').val(datainv);
		carregaconsultas(1);
		//location.href = '?data='+datainv;
	}
	
	function carregasalas(){
		console.log('a');
		$.ajax({
			url : 'ajax_core.php',
			data : {
				acao : 'carregasalas',
				idsede : $('#espsede').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('#espsala, #sala').html(data);
				carregaconsultas(1);
				//loader(0);
				console.log('b');
			},
			error: function(){
				loader(0);	
			}
		});

	}
	carregasalas();
	
	$('#espmed').change(function(){
		carregaconsultas(1);
	});
	
	$('#espsala').change(function(){
		carregaconsultas(1);
	});
	
	$('#espsede').change(function(){
		idsede = $(this).val();
		$('#idsede').val(idsede);
		carregasalas();
	});
	
	function carregaconsultas(mostraloader){
		if(mostraloader==1){
			loader(1);
		}
		$.ajax({
			url : arquivoag,
			data : {
				dataag : $('#dataag').val(),
				tipousu : $('#tipousu').val(),
				idmedico : $('#espmed').val(),
				idsala : $('#espsala').val(),
				idsede : $('#idsede').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				//alert('a');
				$('.consultas').html(data);	
				if($('#tipousu').val()==2){
					$('.agmedico').width($(window).width() - 110);
					$('.agmedico').css({'cursor':'auto'});
					$('.nomemedico').width($(window).width() - 120);
					$('.consulta').width($('.agmedico').width() - 20);
				}
				loader(0);
			},
			error: function(){
				loader(0);	
			}
		});
	}
	
	$('.hora').click(function(){
		window.scrollTo(0, 0);
		$('.fotocliente img').attr('src','');
		$('.btvercliente, .dadospac').hide();
		$('.modalcliente input[type=text], .modalcliente input[type=email], .modalcliente input[type=tel], .modalcliente select, #obsconsulta, #orcamentocirurgico').val('');
		$('.nomepac').html('Novo paciente');
		$('#fotocliente').val('');
		$('.video1,.video2,.botoesfoto').hide();
		$('.btdelconsulta, .btreceituario, .btprontuario').hide();
		$('.historico').html('');
		$('#status').val('1');

		if($('#tipousu').val()=='1'||$('#tipousu').val()=='3'){
			
			//////////VALOR PADRÃO DA CONSULTA
			valorconsulta('NOVA');
			
			hc = $(this).attr('nhora');
			if(parseInt(hc) < 10){
				hc = '0' + hc;	
			}
			//$('#hini').val(hc+':00');
			//$('#hfim').val(hc+':15');
			$('.modalcliente').css({'background-color':'#FFF'});
			var date_new = $('#diaagenda').datepicker ('getDate'); 
			$('#data').datepicker ('setDate', date_new);
			$('.modalcliente,.mascara').fadeIn(400);
			
			$('#salvarcliente').attr('acaoconsulta','cadastrar');
			$('#salvarcliente').attr('acao','cadastrar');
			$('#salvarcliente').attr('idc','');
			$('#salvarcliente').attr('idconsulta','');
			$('.qdadoscliente input, .qdadoscliente select').removeAttr('disabled');
			
			
			///limpa dados do formulário
			$('.linhaplanos').hide();
			$('#plano_saude').val('');
			$('#paciente').val('');
			$('#paciente').attr('idcliente','');
			$('#parcelamento').val('');
			$('#valor').val('');
			$('#justificaconsulta').val('');
			$('.qjustconsulta').hide();
			$('#chkjustificaconsulta').prop( "checked", false );
			$('#valor,#valormedico,#valorclinica').attr('disabled','disabled');
			
			$('#valorproc, #valormedicoproc, #valorclinicaproc').attr('disabled','disabled');	
			$('.qjustproc').fadeOut(400);
			$('#justificaproc').val('');
			$('.procscadastrar').html('');
			$('.linhavaloresprocedimentos').hide();
			
			$('#cpf').val('');
			$('#medico').children('option:selected').removeAttr('selected');
			$('#sala').children('option:selected').removeAttr('selected');
			$('#status').children('option:selected').removeAttr('selected');
			$('#nota').children('option:selected').removeAttr('selected');
			$('#tipoconsulta').val('NOVA');
			$('#forma_pagamento').children('option:selected').removeAttr('selected');
			$('#obs').val('');
			$('#iddoc').val('');
			$('.enviadoc, .documentos, .fotocliente').hide();
		}
	});

	/*$('#salvarconsulta').click(function(){
		loader(1);
		alert('aa');
		acaoconsulta = $(this).attr('acaoconsulta');
		idaltera = $(this).attr('idaltera');
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'cadastroconsulta',
				acaoconsulta : acaoconsulta,
				idpaciente : $('#paciente').attr('idcliente'), 
				idmedico : $('#medico option:selected').val(),
				nomepaciente : $('#paciente').val(), 
				status : $('#status').val(), 
				dataconsulta : $('#data').val(),
				plano_saude :  $('#plano_saude').val(),
				hini : $('#hini').val(), 
				hfim : $('#hfim').val(), 
				valormedico : $('#valormedico').val(),
				valorclinica : $('#valorclinica').val(),
				parcelamento : $('#parcelamento').val(),
				forma_pagamento : $('#forma_pagamento').val(),
				justificavalor: $('#justificaconsulta').val(),
				nota : $('#nota').val(),
				valor : $('#valor').val(),
				idaltera : idaltera,
				obs	: $('#obsconsulta').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				//alert('Sucesso!');
				carregaconsultas();
				$('.modal,.mascara').fadeOut(400);
				console.log(data);
				//location.reload();						
			}
		});
	});*/
	
	
	
	function valorconsulta(tipo){
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'valoresconsulta',
					tipo : tipo
				},
				type : "POST",
				dataType:"json",
				success : function(data) {
					$('#valor,#cartao').val(data[0].valor);
					$('#valormedico').val(data[0].medico);
					$('#valorclinica').val(data[0].clinica);
				}
			});
	}
	
	function valorprocedimento(idproc){
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'valoresprocedimento',
					idproc : idproc
				},
				type : "POST",
				dataType:"json",
				success : function(data) {
					//alert('sucesso');
					$('#valorproc,#cartaoproc').val(data[0].total);
					$('#valormedicoproc').val(data[0].medico);
					$('#valorclinicaproc').val(data[0].clinica);
				}
			});
	}
	
	$('#tipoconsulta').change(function(){
		valorconsulta($(this).val());
	});
	
	$('#chkjustificaconsulta').change(function(){
		
		if($(this).is(':checked')){
			//alert('a');
			$('#valor, #valormedico, #valorclinica').removeAttr('disabled');
			$('.qjustconsulta').fadeIn(400);
		}else{
			valorconsulta($('#tipoconsulta').val());
			$('#valor, #valormedico, #valorclinica').attr('disabled','disabled');	
			$('.qjustconsulta').fadeOut(400);
		}
	});
	
	$('#chkjustificaproc').change(function(){
		
		if($(this).is(':checked')){
			//alert('a');
			$('#valorproc, #valormedicoproc, #valorclinicaproc').removeAttr('disabled');
			$('.qjustproc').fadeIn(400);
		}else{
			valorprocedimento($('#listaprocedimentos').val());
			$('#valorproc, #valormedicoproc, #valorclinicaproc').attr('disabled','disabled');	
			$('.qjustproc').fadeOut(400);
		}
	});
	
	
	$('#listaprocedimentos').change(function(){
		idproc = $(this).val();
		$('#valorproc, #valormedicoproc, #valorclinicaproc').attr('disabled','disabled');	
		$('.qjustproc').fadeOut(400);
		$('#justificaproc').val('');
		valorprocedimento(idproc);
		
		$('.linhavaloresprocedimentos').fadeIn(400);
	});
	
	$('#btaddprocedimento').click(function(){
		idproc = $('#listaprocedimentos').val();
		procedimento = $('#listaprocedimentos option:selected').text();
		total = $('#valorproc').val();
		dinheiro = $('#dinheiroproc').val();
		cartao = $('#cartaoproc').val();
		if ($('#notaproc').is(":checked")) { 
			nota = 1; 
		} else { 
			nota = 0; 
		}
		
		medico = $('#valormedicoproc').val();
		clinica = $('#valorclinicaproc').val();
		justificativa = $('#justificaproc').val();
		$('.procscadastrar').append('<div class="cadproc" value="'+idproc+'" total="'+total+'" dinheiro="'+dinheiro+'" cartao="'+cartao+'" nota="'+nota+'" medico="'+medico+'" clinica="'+clinica+'" justificativa="'+justificativa+'">+ '+ procedimento+'</div><div class="deletaprocreg">X</div>');
		$('.deletaprocreg').click(function(){
			$(this).prev().remove();
			$(this).remove();
		});
	});
	
	
	
	
	
	if($('#tipousu').val()==1 || $('#tipousu').val()==3){
		$(window).scroll(function(){
			rollesq = $(this).scrollLeft();
			if($(this).scrollLeft() > 0){
				if($('.horasfixas').css('display')=='none'){
					$('.horasfixas').show();
				}
			}else{
				if($('.horasfixas').css('display')=='block'){
					$('.horasfixas').hide();	
				}
			}
			$('.horasfixas').css({'left':rollesq + 'px'});
		});
		
		num=0;
		function scrlateral(num){
			lat = num * $(window).width();
			$('html, body').stop().animate({scrollLeft: lat}, 500);
			if(num<0){
				num = 0;	
			}
		}
		
		
	}
	
	$(window).scroll(function(){
		if($('html, body').scrollLeft()<60){
			$('.agesq').fadeOut(400);	
		}else{
			$('.agesq').fadeIn(400);
		}
		
		if($('html, body').scrollTop()<160){
			$('.agcima').fadeOut(400);	
		}else{
			$('.agcima').fadeIn(400);
		}
	});
	
	$('.agdir').click(function(){
		num++;
		scrlateral(num);
		
	});
	$('.agesq').click(function(){
		num--;
		scrlateral(num);
		
	});
	$('.agcima').click(function(){
		scrt = $(window).scrollTop() - 500;
		$('html, body').stop().animate({scrollTop: scrt}, 500);
	});
	$('.agbaixo').click(function(){
		scrt = $(window).scrollTop() + 500;
		$('html, body').stop().animate({scrollTop: scrt}, 500);
	});
	
	$('.exibepaciente').click(function(){
		valorconsulta('NOVA');
	});
	
	$('#ocultacanceladas').click(function(){
		if($(this).is(':checked')){
			$('[statusconsulta=5]').fadeOut(400);	
		}else{
			$('[statusconsulta=5]').fadeIn(400);	
		}
	});
	
	$('.btdelconsulta').click(function(){
		idcons = $(this).attr('idcons');
		//alert(idcons);
		var r = confirm("Deseja realmente deletar essa consulta?");
		if (r == true) {
		  	$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'deletaconsulta',
					idcons : idcons
				},
				type : "POST",
				success : function(data) {
					$.ajax({
						url : arquivoag,
						data : {
							dataag : $('#dataag').val(),
							tipousu : $('#tipousu').val(),
							idmedico : $('#espmed').val(),
							idsede : $('#idsede').val()
						},
						type : "POST",
						//dataType:"json",
						success : function(data) {
							//alert('a');
							$('.consultas').html(data);	
							$('.modal,.mascara').fadeOut(400);			
						}
					});
	
				}
			});
		} else {
		  
		}
	});
	
	$('#selectipoag').change(function(){
		location.href = 'agenda.php?t='+$(this).val();
	});
	
	function verificapreenchimento(){
		if($('#medico').val()==null || $('#medico').val()==''){ $('#medico').css({'box-shadow':'0px 0px 8px red'}); }
		else{ $('#medico').css({'box-shadow':'0px 0px 5px green'}); }
		if($('#sala').val()==null || $('#sala').val()==''){ $('#sala').css({'box-shadow':'0px 0px 8px red'}); }
		else{ $('#sala').css({'box-shadow':'0px 0px 5px green'}); }	
	}
	
	setInterval(function(){
		carregaconsultas(0);
		agora();
		$('.linhahatual').width($('html body').width());
	},60000);
	
	if ($('.linhahatual').length){
		setTimeout(function(){
			$('body, html').stop().animate({scrollTop: $('.linhahatual').offset().top - 200}, 500);
		},300);
	}
	
});