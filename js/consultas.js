// JavaScript Documentf

function hconsultas(){
		
		if($('.consulta').length){
			$('.consulta').each(function(){
				iniint = $(this).attr('horai').split(":");
				hi = parseInt(iniint[0]);
				mi = parseInt(iniint[1]);
				fimint = $(this).attr('horaf').split(":");
				hf = parseInt(fimint[0]);
				mf = parseInt(fimint[1]);
				
				
				
				//// altura da consulta subtrai 8 de padding (4 top + 4 bottom)
				alturah = $('[numlinhah='+hi+']').height() - 4;
				topom = (mi/60)*alturah;
				/// 180 refere-se a altura do topo -2 para ajuste
				topoh = $('[numlinhah='+hi+']').offset().top - 178;
				
				calch = hf - hi;
				hs = calch*60;
				//console.log(hs);
				mn = mi - mf;
				totalhm = hs + (mf - mi);	
				//console.log(totalhm);
				totalhm = (totalhm/60)*alturah;
				$(this).css({'top':(topoh+topom)+'px','height':totalhm+'px'});
				
				if(parseInt(totalhm)<15){
					$(this).children('.txtconsulta').html('');
				}
				
				
			});
		}
	}
	
	if($('#tipousu').val()==1 || $('#tipousu').val()==3){
		function posmedico(){
			larguramed = 350;
			contamed = 0;
			$('.agmedico').each(function(){
				//console.log(contamed);
				ctm = contamed * larguramed;
				$(this).css({'width':larguramed+'px','left':ctm+'px'});
				contamed++;
			});
			larglinha = contamed * larguramed + 80;
			if(larglinha > $(window).width() - 100){
				$('.linhahora,.linhahatual').css({'width':larglinha+'px'});
			}else{
				$('.linhahora').css({'width':'98%'});
			}
		}
		posmedico();
	}
	
	hconsultas();
	
	
	consultaaberta = 0;
	//alert('aaa');
	$('.consulta').click(function(){
		//if($('#tipousu').val()=='2'){ loader(1); }
		//$('.mascara2').fadeIn(400);
		idc = $(this).attr('idpaciente');
		idconsulta = $(this).attr('idconsulta');
		$('.btdelconsulta, .btreceituario, .btprontuario').show();
		$('.btdelconsulta').attr('idcons',idconsulta);
		if($('#tipousu').val()=='2'){ 
			//$('.btprontuario').click(); 
			//$('#salvarprontuario').attr('idc',idc); 
			setTimeout(function(){
				$('.mascara,.loader').hide();
			},1000);
			//window.open('prontuario_externo.php?idpaciente='+idc+'&idconsulta='+idconsulta);
			location.href = 'prontuario_externo.php?idpaciente='+idc+'&idconsulta='+idconsulta;
		}
		$('.video1,.video2,.botoesfoto').hide();
		$('.qdadoscliente input, .qdadoscliente select, #orcamentocirurgico').val('');
		$('.fotocliente img').attr('src','');
		window.scrollTo(0, 0);
		consultaaberta=1;
		if($('#tipousu').val()=='2'){
			$('.btaddpaciente').hide();	
		}
		
		
		carregarhistorico(idc);
		$('#editacliente').val(idc);
		
		if($('#tipousu').val()!='2'){
			$('.modalcliente').fadeIn(400);
		}
		$('.mascara').fadeIn(400);
		$('#salvarcliente').attr('acaoconsulta','alterar');
		$('#salvarcliente').attr('acao','alterar');
		$('#salvarcliente').attr('idc',idc);
		$('#salvarcliente').attr('idconsulta',idconsulta);
		//$('#salvarconsulta').attr('idaltera',$(this).attr('idconsulta'));
		//$('#iddoc').val($(this).attr('idconsulta'));
		//console.log(idc);
		
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'carregapaciente',
				idc : idc
			},
			type : "POST",
			dataType:"json",
			success : function(data) {
				//console.log(data);
				//$('.qdadoscliente input, .qdadoscliente select').attr('disabled','disabled');
				if(data!=''){
					$('.mascara2').fadeOut(400);
					$('.divfoto').show();
					$('.fotocliente').show();
					endereco = data[0].endereco;
					numero = data[0].numero;
					complemento = data[0].complemento;
					
					if(data[0].cpf!=''){ $('#cpf').val(data[0].cpf) };
					if(data[0].rg!=''){ 
						
						$('#rg').val(data[0].rg) 
					};
					if(data[0].nome!=''){ $('#paciente').val(data[0].nome); $('.nomepac').html(data[0].nome) };
					if(data[0].origem!=''){ $('#origemcliente').val(data[0].origem) };
					if(data[0].email!=''){ $('#email').val(data[0].email) };
					if(data[0].cidade!=''){ $('#cidade').val(data[0].cidade) };
					if(data[0].cep!=''){ $('#cep').val(data[0].cep) };
					if(data[0].endereco!=''){ $('#endereco').val(endereco); };
					if(data[0].numero!=''){ $('#numeroend').val(numero); }
					if(data[0].complemento!=''){ $('#complemento').val(complemento); }
					if(data[0].bairro!=''){ $('#bairro').val(data[0].bairro) };
					if(data[0].fotopaciente!=''){ $('#fotocliente').val(data[0].fotopaciente) }
					else{$('#fotocliente').val('')};
					if(data[0].totalconsultas!=''){ $('.numtotalconsultas').html(data[0].totalconsultas) }
					
					 
						carregaultimas(idc); 
						console.log('aqiui');
					
					$('.dadospac').fadeIn(400);
					
					
					if(data[0].telefone!=''&&data[0].telefone!='0'){ 
						$('#telefone').val(data[0].telefone);
						$('.btwhats').show();
						$('.btwhats').click(function(){
							fone = $('#telefone').val();
							if(fone.length == 8 && parseInt(fone.substring(0, 1)) > 7){
								fone = '519'+fone;
							}
							if(fone.length == 9 && parseInt(fone.substring(0, 1)) > 7){
								fone = '51'+fone;
							}
							//alert(fone.length);
							$.ajax({
								url : "ajax_contato_whats.php",
								data : {
									idconsulta : idconsulta
								},
								type : "POST",
								//dataType:"json",
								success : function(data) {
									url = "https://api.whatsapp.com/send?phone=55"+fone+"&text="+data;
									window.open(url);
								}
							});	
							
							//window.open("https://api.whatsapp.com/send?phone=55"+fone+"&text=Ol%C3%A1%2C%20somos%20da%20Cl%C3%ADnica%20da%20Cirurgia.");
						});
					};
					if(data[0].telefone2!=''&&data[0].telefone2!='0'){ $('#telefone2').val(data[0].telefone2) };
					if(data[0].data_nascimento!=''){ 
						split = data[0].data_nascimento.split('-');
						datainvert = split[2] + '/' + split[1] + '/' + split[0];
						$('#datanascimento').val(datainvert);
					};
					
					//alert(data[0].foto);
					/*$('.imgpaciente').attr('src' ,'upload/fotos/'+data[0].foto);
					setTimeout(function(){
						$('.imgpaciente').fadeIn(400);
					},1000);*/
					
				}					
			}
		});
		
		$('.fotocliente, .btvercliente').show();
		$('.btvercliente').attr('idc',$(this).attr('idpaciente'));
		var date_new = $('#diaagenda').datepicker ('getDate');	 
		idconsulta = $(this).attr('idconsulta');
		$('#data').datepicker ('setDate', date_new);
		$('#hini').val($(this).attr('horai'));
		$('#hfim').val($(this).attr('horaf'));
		$('#paciente').val($(this).attr('nomepaciente'));
		$('#paciente').attr('idcliente',$(this).attr('idpaciente'));
		$('#parcelamento').val($(this).attr('parcelamento'));
		$('#valor').val($(this).attr('valor'));
		$('#dinheiro').val($(this).attr('dinheiro'));
		$('#cartao').val($(this).attr('cartao'));
		$('#valormedico').val($(this).attr('valormedico'));
		$('#valorclinica').val($(this).attr('valorclinica'));
		$('#justificaconsulta').val($(this).attr('justificativa'));
		$('#chkjustificaconsulta').prop( "checked", false );
		$('#valor,#valormedico,#valorclinica').attr('disabled','disabled');
		if($('#justificaconsulta').val()!=''){
			$('.qjustconsulta').show();
		}else{
			$('.qjustconsulta').hide();
		}	
		//$('#medico').children('[value='+$(this).attr('idmed')+']').change();
		$('#medico').children('[value='+$(this).attr('idmed')+']').attr('selected','selected');
		$('#medico').val($(this).attr('idmed'));
		$('#sala').children('[value='+$(this).attr('idsala')+']').change();
		$('#sala').children('[value='+$(this).attr('idsala')+']').attr('selected','selected');
		$('#sala').val($(this).attr('idsala'));
		
		verificapreenchimento();
		
		$('#tipoconsulta').val($(this).attr('tipoconsulta'));
		$('#status').children('[value='+$(this).attr('statusconsulta')+']').change();
		$('#status').children('[value='+$(this).attr('statusconsulta')+']').attr('selected','selected');
		$('#status').val($(this).attr('statusconsulta'));
		$('#nota').children('[value='+$(this).attr('nota')+']').change();
		$('#nota').children('[value='+$(this).attr('nota')+']').attr('selected','selected');
		$('#forma_pagamento').children('[value='+$(this).attr('forma_pagamento')+']').change();
		$('#forma_pagamento').children('[value='+$(this).attr('forma_pagamento')+']').attr('selected','selected');
		plano = $(this).attr('planosaude');
		if(plano!=0){
			idm = $('#medico').val();
			if(idm!=''){
				$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'listaplanosmed',
						idm : idm
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						$('#plano_saude').html(data);
						$('.linhaplanos').fadeIn(400);
						$('.linhaplanos').show();
						$('#plano_saude').val(plano);
						$('#plano_saude').children('[value='+plano+']').change();
						$('#plano_saude').children('[value='+plano+']').attr('selected','selected');
					}
				});	
			}

		}else{
			$('#plano_saude').val('');
		}
		
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'obsconsulta',
					idconsulta : idconsulta 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					$('#obsconsulta').val(data);					
				}
			});
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'orcamentoconsulta',
					idconsulta : idconsulta 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					$('#orcamentocirurgico').val(data);					
				}
			});
			
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'fotopaciente',
					idpaciente : $('#paciente').attr('idcliente') 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					if(data!=''){
						$('.imgpaciente').attr('src' , data);
					}else{
						$('.imgpaciente').attr('src' ,'img/icones/persona_ico.png');	
					}
					setTimeout(function(){$('.imgpaciente').fadeIn(400); $('.fotocliente, .btvercliente').show();},1000);					
				}
			});	
				
		});
		
		function carregaultimas(idc){
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'ultimasconsultas',
					idc : idc 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					$('.ultimascons').html(data);			
				}
			});		
		}
		
		$('.agmedico').click(function(event){
			//alert(event.pageY);
			if(consultaaberta==0){
				$('.btvercliente').hide();
				alturac = 180;
				//alturalinhac = 47;
				alturalinhac = 102;
				horaclick = parseInt(((event.pageY - alturac) / alturalinhac)+6);
				med = $(this).attr('med');
				sala = $(this).attr('sala');
				setTimeout(function(){
					$('[nhora='+horaclick+']').click();
					if($('#tipoag').val()=='salas'){ $('#sala').val(sala); }
					else{ $('#medico').val(med); }
					verificapreenchimento();
				},1000);
				
			}
		});
		
		$('.minutosconsulta').click(function(event){
			$('#hini').attr('disabled','disabled');
			$('#hfim').attr('disabled','disabled');
			//idmedico = '';
			//idmedico = $(this).attr('idmed');
			imed = $(this).attr('med');
			$(this).parent().attr('med',imed);
			
			if(consultaaberta==0){
				$('.btvercliente').hide();
				horaclick = parseInt($(this).attr('horaini'));
				horaini = $(this).attr('horaini');
				horafim = $(this).attr('horafim');
				minini = $(this).attr('minini');
				minfim = $(this).attr('minfim');
				setTimeout(function(){
					$('#medico').val(imed);
					verificapreenchimento();
				},2000);
				setTimeout(function(){
					//console.log(horafim+':'+minfim);
					$('[nhora='+horaclick+']').click();
					console.log(horaini+':'+minini);
					$('#hini').val(horaini+':'+minini);
					$('#hfim').val(horafim+':'+minfim);
					//alert(imed);
					
					
				},1000);
				
			}
		});
		
		function verificapreenchimento(){
			if($('#medico').val()==null || $('#medico').val()==''){ $('#medico').css({'box-shadow':'0px 0px 8px red'}); }
			else{ $('#medico').css({'box-shadow':'0px 0px 5px green'}); }
			if($('#sala').val()==null || $('#sala').val()==''){ $('#sala').css({'box-shadow':'0px 0px 8px red'}); }
			else{ $('#sala').css({'box-shadow':'0px 0px 5px green'}); }
			if($('#obsconsulta').val()==null || $('#obsconsulta').val()==''){ $('#obsconsulta').css({'box-shadow':'0px 0px 8px red'}); }
			else{ $('#obsconsulta').css({'box-shadow':'0px 0px 5px green'}); }	
		}
		
		
		
		
		//////////ENVIAR DOCUMENTOS
		$('#inputdocumento').change(function(){
			 $('.mascara,.loader').fadeIn(400);
			 $('#visualizar').html('Enviando...');
			
			 /* Efetua o Upload */
			 $('#formulario').ajaxForm({

				success:function(data){
					/*setTimeout(function(){
					$('.cortafoto').load('upload/cortafoto.php?idusu=<?php echo $idusu;?>');
					$('#fotoperfil').css({'height':'260px'});
					setTimeout(function(){$('#formulario,.mascara,#loader').hide();},2000)
					},3000);*/
					$.ajax({
						url : "ajax_core.php",
						data : {
							acao : 'docsconsulta',
							idconsulta : idconsulta 
						},
						type : "POST",
						//dataType:"json",
						success : function(data) {
							$('.exibedocs').html(data);				
						}
					});
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			 }).submit();
		 });
		 
		 $('.consulta').mouseenter(function(){
			 nomebalao = $(this).attr('nomepaciente');
			 fotobalao = $(this).attr('foto');
			 motivobalao = $(this).attr('motivo');
			 horabalao = $(this).attr('horai') + ' - ' + $(this).attr('horaf');
			$('.balaoconsulta').stop().fadeIn(400);
			$(this).css({'z-index':'50'});
			$( this ).on( "mousemove", function( event ) {
				$('.nomebalao').html(nomebalao);
				$('.horabalao').html(horabalao);
				$('.motivobalao').html(motivobalao);
				$('.fotobalao img').attr('src',fotobalao);
			  balaox = (event.pageX - $('html, body').scrollLeft() - 5);
			  balaoy = (event.pageY - $('html, body').scrollTop() - 164);
			  $('.balaoconsulta').css({'top':balaoy+'px','left':balaox+'px'});
			});
		});
		$('.consulta').mouseleave(function(){
			$('.balaoconsulta').stop().fadeOut(400);
			$(this).css({'z-index':'30'});
		});
		
		$('#medico,#sala,#obsconsulta').change(function(){
			verificapreenchimento();
		});
		$('#obsconsulta').keyup(function(){
			verificapreenchimento();
		});
		
		
		
		
		function carregarhistorico(idc){
		//alert(idc);
			$.ajax({
				url : "ajax_historico.php",
				data : {
					acao : 'listarhistorico',
					idc : idc,
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					$('.historico').html(data);
					$('.abreprontuario').click(function(){
						idp = $(this).attr('idp');
						window.open('prontuario.php?idp='+idp);
					});
					$('.abrereceituario').click(function(){
						idr = $(this).attr('idr');
						tiporec = $(this).attr('tipo');
						if(tiporec=='1'){
							window.open('receituario.php?idr='+idr);
						}
						if(tiporec=='2'){
							window.open('receituario_especial.php?idr='+idr);
						}
					});
					$('.abreconsulta').click(function(){
						idc = $(this).attr('idc');
						window.open('consulta.php?idc='+idc);
					});
					$('.abrelaudo').click(function(){
						cod = $(this).attr('cod');
						window.open('laudo_ecografia.php?cod='+cod);
					});
				}
			});
		}
		
		quantlinhamin = 4; /// 15 minutos
		//alturahora = $('.linhahora').first().height();
		//$('.minutosconsulta').height((alturahora / quantlinhamin)-(1 / (quantlinhamin / 2)));
		
		alturahora = $('.linhahora').first().height() + 2;
		$('.minutosconsulta').height((alturahora / quantlinhamin)-1);
		
		$('.minutosconsulta').mouseenter(function(){
			//alert('a');
			$(this).children('.exibehm').fadeIn(200);
		});
		$('.minutosconsulta').mouseleave(function(){
			$(this).children('.exibehm').fadeOut(200);
		});
		