// JavaScript Document
$(document).ready(function(){
	
	$( "#data" ).datepicker();
    $( "#data" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );
	$('#datanascimento').mask('00/00/0000');
	$('#cep').mask('00000-000');
	$('#telefone').mask('(99)999999999');
	$('#telefone2').mask('(99)999999999');
	$('#cpf').mask('999.999.999-99');
	$('#rg').mask('9999999999');
	$( "#diaagenda" ).datepicker();
    $( "#diaagenda" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );
	
	
	function loader(n){
		if(n==1){
			$('.mascara, .loader').fadeIn(400);	
		}
		if(n==0){
			$('.mascara, .loader').fadeOut(400);
		}
	}
	
	
	
	
	consultaaberta = 0;
	$('.btaddpaciente,[usun=1]').click(function(){
			if($('.modalconsulta').css('display')=='block'){
				consultaaberta = 1;	
			}else{ consultaaberta = 0; }
			$('.fotocliente img').attr('src','');
			$('.qdadoscliente input, .qdadoscliente select').removeAttr('disabled');
			$('.modalcliente').css({'background-color':'#FFF'});
			$('#cpf').val('');
			$('#rg').val('');
			$('#paciente').val('');
			$('#paciente').attr('idcliente','');
			$('#origemcliente').val('');
			$('#email').val('');
			$('#cidade').val('');
			$('#cep,#endereco,#bairro').val('');
			$('#telefone').val('');
			$('#telefone2').val('');
			$('#datanascimento').val('');
			$('.modalcliente,.mascara').fadeIn(400);
			$('.historico').html('');
			$('.video1,.video2,.botoesfoto,.fotocliente,.btprontuario,.btreceituario').hide();
			$('#salvarcliente').attr('acao','cadastrar');
			$('#salvarcliente').attr('acaoconsulta','cadastrar');
			if (typeof stream != "undefined") {
				stream.getTracks().forEach(function(track) {
				  track.stop();
				});
			}
	});
	$('.mascara').click(function(){
		/*consultaaberta=0;
		$('.modal,.mascara').fadeOut(400);*/
	});
	
	
	
	$('.icomenu').click(function(){
		$('.qmenu').fadeToggle(400);
	});
	
	$('.btenviadoc').click(function(){
		$('#inputdocumento').click();
	});
	$('.btenviafoto').click(function(){
		$('#inputfoto').click();
	});
	$('.btenviaassinatura').click(function(){
		$('#inputassinatura').click();
	});
	$('#inputassinatura').change(function(){
		 $('.mascara,.loader').fadeIn(400);
		rand = Math.floor(Math.random() * 1000);
		 /* Efetua o Upload */
		 $('#formularioassinatura').ajaxForm({

			success:function(data){
				alert('Assinatura Enviada');
				$('.carregaassinatura').html("<img src='upload/assinaturas/"+data+"?"+rand+"' width='200'>");
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
		 }).submit();
	});
	
	$('.btprontuario').click(function(){
		//$('.exibenomepaciente').html($('#paciente').val());
		//$('#salvarprontuario').attr('idc',$('#editacliente').val());
		//$('.modalprontuario,.mascara').fadeIn(400);
		//$('#doencapront').val('');
		//$('#subjetivo').val('');
		//$('#objetivo').val('');
		//$('#exames').val('');
		//$('#impressao').val('');
		//$('#conduta').val('');
		//window.open('prontuario_externo.php?idpaciente='+$('#editacliente').val()+'&idconsulta='+$('#salvarcliente').attr('idconsulta'));
		location.href = 'prontuario_externo.php?idpaciente='+$('#editacliente').val()+'&idconsulta='+$('#salvarcliente').attr('idconsulta');
	});
	
	$('.btlimpapront').click(function(){
		$('#doencapront').val('');
		$('#subjetivo').val('');
		$('#objetivo').val('');
		$('#exames').val('');
		$('#impressao').val('');
		$('#conduta').val('');
	});
	
	$('.btreceituario').click(function(){
		$('.exibenomepaciente').html($('.nomepac').text());
		$('.modalreceituario,.mascara').fadeIn(400);
		$('.modalcliente').fadeOut(400);
		$('#salvarreceituario').attr('idc',$('#salvarcliente').attr('idc'));
		$('#medicamentos').val('');
		$('#quantidade').val('');
		$('#frequencia').val('');
	});
	
	dadosrec = $('.linhadadosrec').clone();
	$('.maisreceituario').click(function(){
		$('.dadosreceituario').append(dadosrec);
		
	});
	
	$('.fecharmodal').click(function(){
		consultaaberta=0;
		$(this).parent().fadeOut(400);
		modalaberto = 0;
		$('.modalprocedimentos input [type=text]').val('');
		setTimeout(function(){
			$('.modal').each(function(){
				
				if($(this).css('display')!='none'){
					modalaberto = 1;	
					
				}
			});
			if(modalaberto==0){
				$('.mascara').fadeOut(400);	
			}
		},600);

		if (typeof stream != "undefined") {
			stream.getTracks().forEach(function(track) {
			  track.stop();
			});
		}
	});
	
	/////////MENU
	 
	  $('[nitem=1]').click(function(){
		location.href='pacientes.php'; 
	 });
	 
	 $('[nitem=2]').click(function(){
		location.href='agenda.php'; 
	 });
	 
	 $('[nitem=5]').click(function(){
		$('.subadm').fadeIn(400); 
	 });
	 
	 $('[usun=2]').click(function(){
		$('.modalorigem,.mascara').fadeIn(400); 
	 });
	 
	 
	 $('.subitemadm').click(function(){
		window.scrollTo(0, 0);
	 });
	 
	 $('[subadm=1]').click(function(){
		$('.modalplanos,.mascara').fadeIn(400); 
	 });
	 $('[subadm=2]').click(function(){
		$('.modalmedicosplanos,.mascara').fadeIn(400);
		$('.carregaplanosmed').hide();
		$('#medicoplano').val(''); 
	 });
	 $('[subadm=3]').click(function(){
		$('.modalorigem,.mascara').fadeIn(400); 
	 });
	 $('[subadm=5]').click(function(){
		$('.modalprocedimentos,.mascara').fadeIn(400);
		$('#salvarprocedimento').attr('acao','cadastra');
	 });
	 $('[subadm=6]').click(function(){
		$('.modalvalorconsulta,.mascara').fadeIn(400);
	 });
	 $('[subadm=7]').click(function(){
		$('.modalexcel,.mascara').fadeIn(400);
	 });
	 $('[subadm=8]').click(function(){
		location.href='unifica.php';
	 });
	 $('[subadm=9]').click(function(){
		location.href='graficos.php';
	 });
	 $('[subadm=10]').click(function(){
		$('.modalsalas,.mascara').fadeIn(400); 
	 });
	 $('[subadm=11]').click(function(){
		window.open('salas_mes.php');
	 });
	 $('[subadm=12]').click(function(){
		window.open('salas_dia.php');
	 });
	 $('[subadm=13]').click(function(){
		$('.modalestoque,.mascara').fadeIn(400);
	 });
	 $('[subadm=14]').click(function(){
		$('.modalestoquetotal,.mascara').fadeIn(400);
	 });
	 $('[subadm=15]').click(function(){
		$('.modalmascaraeco,.mascara').fadeIn(400);
	 });
	 $('[subadm=16]').click(function(){
		$('.modalbuscaprontuarios,.mascara').fadeIn(400);
	 });
	 
	 $('.subitemadm').click(function(){
		$('.subadm').fadeOut(400); 
	 });
	 $('.subadm').mouseleave(function(){
		 $('.subadm').fadeOut(400);
	 });
	 
	 $('[nitem=3]').click(function(){
		$('#tipocliente').children('[value=3]').change();
		$('#tipocliente').children('[value=3]').attr('selected','selected'); 
		$('.modalcliente,.mascara').fadeIn(400); 
	 });
	 
	
	 
	 $('[subadm=4]').click(function(){
		 $('#tipousuario').val();
		 $('#usuario').val();
		 $('#senhausuario').val();
		$('.modalusuario,.mascara').fadeIn(400); 
	 });
	 
	 $('[nitem=6]').click(function(){
		 $.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'sair'
			},
			type : "POST",
			success : function(data) {
				location.reload();				
			}
		});
	 });
	 
	 function buscaendereco(cep){
		$('.mascara2').fadeIn(400);
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'buscaendereco',
				cep : cep
			},
			type : "POST",
			dataType:"json",
			success : function(data) {
				$('#endereco').val(data[0].rua);
				$('#bairro').val(data[0].bairro);
				$('#cidade').val(data[0].cidade);
				$('.mascara2').fadeOut(400);
				$('#numero').focus();
			},
			error: function (data) {
                console.log(data);
				$('.mascara2').fadeOut(400);
            }
		}); 
	 }
	 
	 function buscacep(cidade,rua){
		//$('.mascara2').fadeIn(400);
		
		$.ajax({
			url : "_ajax_busca_endereco_externo.php",
			data : {
				acao : 'buscaendereco',
				cidade : cidade,
				rua : rua
			},
			type : "POST",
			dataType:"json",
			success : function(data) {
				$('.opcoescep').html('');
			
				if(data.enderecos.endereco.logradouro==null){
				$.each(data.enderecos.endereco, function(i) {
					//console.log(data);
						$('.opcoescep').append('<div class="opcaocep" bairro="'+data.enderecos.endereco[i].bairro+'" rua="'+data.enderecos.endereco[i].logradouro+'" cep="'+data.enderecos.endereco[i].cep+'" cidade="'+data.enderecos.endereco[i].localidade+'">'+data.enderecos.endereco[i].logradouro+', '+data.enderecos.endereco[i].bairro+', '+data.enderecos.endereco[i].localidade+' ('+data.enderecos.endereco[i].cep+')</div>');
				});
				}else{
					$('.opcoescep').append('<div class="opcaocep" bairro="'+data.enderecos.endereco.bairro+'" rua="'+data.enderecos.endereco.logradouro+'" cep="'+data.enderecos.endereco.cep+'" cidade="'+data.enderecos.endereco.localidade+'">'+data.enderecos.endereco.logradouro+', '+data.enderecos.endereco.bairro+', '+data.enderecos.endereco.localidade+' ('+data.enderecos.endereco.cep+')</div>');
				}
				$('.opcaocep').click(function(){
					$('#endereco').val($(this).attr('rua'));
					$('#bairro').val($(this).attr('bairro'));
					$('#cep').val($(this).attr('cep'));
					$('#cidade').val($(this).attr('cidade'));
				});
			},
			error: function (data) {
                console.log(data);
            }
		}); 
	 }
	 
	 $('#cep').blur(function(){
		 buscaendereco($(this).val());
	 });
	 
	 $('#endereco').keyup(function(){
		 if($('#cidade').val()!='' && $(this).val().length > 5){
		 	buscacep($('#cidade').val(),$(this).val());
			$('.opcoescep').show();
		 }
	 });
	 
	 $('#endereco').blur(function(){
		 $('.opcoescep').html();
		 $('.opcoescep').fadeOut(400);
	 });
	 
	 $('#telefone,#telefone2').blur(function(){
		 fone = $(this).val().replace('(','').replace(')','');
		 if(fone!=''){
			if(fone.length < 10){ alert('Ops! Verifique se o DDD + número de telefone está correto.'); }
			if(fone.length == 10 && fone.substring(2, 3) > 6){
				$(this).val('('+fone.substring(0, 2) + ')9' + fone.substring(2, 10));
			}
		 }
	 });
	 
	 $('#sedeitens').change(function(){
		 $('#categoriaest').removeAttr('disabled');
	 });
	 
	 $('#categoriaest').change(function(){
		 listaitensest();
	 });
	 
	 function listaitensest(){
		$('.mascara2').fadeIn(400);
		 $.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'listaitensestoque',
				sede : $('#sedeitens').val(),
				categoria : $('#categoriaest').val()
			},
			type : "POST",
			success : function(data) {
				$('#carregaitensest').html(data);
				$('.mascara2').fadeOut(400);
				verificaminimo();
				altqtditem();
				btadd();
			},
			error: function (data) {
                console.log(data);
				verificaminimo();
				$('.mascara2').fadeOut(400);
            }
		});  
	 }
	 
	 function verificaminimo(){
		$('.linhaitemdtq').each(function(){
			console.log($(this).attr('minimo'));
			if(parseInt($(this).attr('minimo')) >= parseInt($(this).attr('total'))){
				$(this).css({'background-color':'#ffcbcb'});
			}else{
				$(this).css({'background-color':'#ffffff'});
			}
		});
	 }
	 
	 function btadd(){
		$('.maisstq').click(function(){
			id = $(this).attr('addid');
			valor = $('[totalid='+id+']').val();
			nvalor = parseInt(valor)+1;
			$('[totalid='+id+']').val(nvalor);
		});
		$('.menosstq').click(function(){
			id = $(this).attr('addid');
			valor = $('[totalid='+id+']').val();
			nvalor = parseInt(valor)-1;
			$('[totalid='+id+']').val(nvalor);
		});
	 }

	
	
	////////////DADOS
	
	$('#salvarusuario').click(function(){
		loader(1);
		$.ajax({
			url : "ajax_core.php",
			data : {
				idusu : $('#idusualtera').val(),
				acao : 'cadastrousuario',
				usuario : $('#usuario').val(), 
				tipo : $('#tipousuario').val(),
				permiteorcamento : $('#permiteorcamento').val(), 
				nome : $('#nomeusu').val(),
				crm : $('#crm').val(),
				especialidade : $('#medespecialidade').val(),
				senha : $('#senhausuario').val(),
				acaousu : $('#salvarusuario').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				location.href='?m=4';						
				//console.log(data);
			}
		});
	});
	
	$('.deletausu').click(function(){
		idusu = $(this).attr('idusu');
		var r = confirm("Deletar usuário?");
		if (r == true) {
		loader(1);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'deletausuario',
					idusu : idusu, 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					location.href='?m=4';						
					//console.log('aaa');
				}
			});
		}
	});
	
	$('#salvarsala').click(function(){
		$('.mascara').fadeIn(400);
		$.ajax({
			url : "ajax_core.php",
			data : {
				idsala : $('#idsalaaltera').val(),
				acao : 'cadastrosala',
				nome : $('#nomesala').val(),
				sede : $('#sedesala').val(),
				acaosala : $('#salvarsala').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				location.href='?m=10';
			}
		});
	});
	
	
	$('#salvaritemest').click(function(){
		$('.mascara').fadeIn(400);
		$.ajax({
			url : "ajax_core.php",
			data : {
				iditem : $('#iditemestaltera').val(),
				acao : 'cadastroitemest',
				acaoitem : $('#salvaritemest').val(),
				nome : $('#itemest').val(),
				categoria : $('#categoriaitemest').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				location.href='?m=13';
				//console.log(data);
			}
		});
	});
	
	$('.deletaitemest').click(function(){
		iditem = $(this).attr('iditem');
		var r = confirm("Deletar item?");
		if (r == true) {
		loader(1);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'deletaitemest',
					iditem : iditem 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					location.href='?m=13';						
					//console.log('aaa');
				}
			});
		}
	});
	
	function altqtditem(){
		$('.alteraqtditem').click(function(){
			iditem = $(this).attr('iditem');
			total = $(this).attr('total');
			minimo = $(this).attr('minimo');
			sede = $(this).attr('sede');
			$('.mascara2').fadeIn(400);
			verificaminimo();
			$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'alteraqtditem',
						iditem : iditem,
						total : $('[totalid='+iditem+']').val(),
						minimo : $('[minimoid='+iditem+']').val(),
						sede : sede
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						//alert('Alterado!');
						$('[iditem='+iditem+']').css({'border':'1px solid #ccc'});
						$('.mascara2').fadeOut(400);
						listaitensest();
						
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
				});
			
		});
		
		$('.totalitemest').keyup(function(){
			id = $(this).attr('totalid');
			$('[iditem='+id+']').css({'border':'2px solid red'});
		});
		$('.minimoitemest').keyup(function(){
			id = $(this).attr('minimoid');
			$('[iditem='+id+']').css({'border':'2px solid red'});
		});
		
	}
	
	
	$('.btnovousu').click(function(){
		$('#salvarusuario').val('Salvar');
		$('#usuario').val('');
		$('#nomeusu').val('');
		$('#tipousuario').val('');
		$('#senhausuario').val('');
		$('#idusualtera').val('');
		$('#crm,#especialidade').val('');
		$('.btenviaassinatura,.carregaassinatura').hide();
		$('.carregaassinatura').html('');
	});
	
	$('.alterausu').click(function(){
		rand = Math.floor(Math.random() * 1000);
		$('#salvarusuario').val('Alterar');
		$('#idusualtera').val($(this).attr('idusu'));
		$('#idassinatura').val($(this).attr('idusu'));
		$('#usuario').val($(this).attr('usuario'));
		$('#nomeusu').val($(this).attr('nome'));
		$('#tipousuario').val($(this).attr('tipo'));
		$('#permiteorcamento').val($(this).attr('permiteorcamento'));
		$('#senhausuario').val($(this).attr('senha'));
		$('#crm').val($(this).attr('crm'));
		$('#medespecialidade').val($(this).attr('especialidade'));
		$('.btenviaassinatura').show();
		$('.carregaassinatura').html("<img src='upload/assinaturas/"+$(this).attr('idusu')+".jpg?"+rand+"' width='200'>");
		carregaassinatura($(this).attr('idusu'));
	});
	
	
	
	$('.alteraitemest').click(function(){
		$('#salvaritemest').val('Alterar');
		$('#iditemestaltera').val($(this).attr('iditem'));
		$('#itemest').val($(this).attr('item'));
		$('#categoriaitemest').val($(this).attr('categoria'));
	});
	
	$('.btnovasala').click(function(){
		$('#salvarsala').val('Salvar');
		$('#nomesala').val('');
		$('#sedesala').val('');
	});
	
	$('.alterasala').click(function(){
		$('#salvarsala').val('Alterar');
		$('#idsalaaltera').val($(this).attr('idsala'));
		$('#nomesala').val($(this).attr('nome'));
		$('#sedesala').val($(this).attr('idsede'));
	});
	
	$('#salvarcliente').click(function(){
		$(this).hide();
		$('.mascara,.mascara2').fadeIn(400);
		
		setTimeout(function(){
			$('#salvarcliente').show();	
		},5000);
		
		continua = 1;
		acaoconsulta = $(this).attr('acaoconsulta');
		idc = $('#salvarcliente').attr('idc');
		idconsulta = $('#salvarcliente').attr('idconsulta');
		endereco = $('#endereco').val();
		numero = $('#numeroend').val();
		complemento = $('#complemento').val();
		idsede = $('#espsede').val();
		if($('#orcamentocirurgico')){
			orcamento = $('#orcamentocirurgico').val();	
		}
		else{
			orcamento = "";	
		}
		
		
			if(continua==1){
				$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'cadastrocliente',
						idc : idc,
						cpf : $('#cpf').val(),
						rg : $('#rg').val(),
						nome : $('#paciente').val(), 
						//tipo : $('#tipocliente').val(), 
						telefone : $('#telefone').val(), 
						telefone2 : $('#telefone2').val(), 
						email : $('#email').val(), 
						cidade : $('#cidade').val(),
						cep : $('#cep').val(),
						endereco : endereco,
						numero : numero,
						complemento : complemento,
						bairro : $('#bairro').val(),
						datanascimento : $('#datanascimento').val(), 
						origem : $('#origemcliente').val(),
						tipoacao : $('#salvarcliente').attr('acao'),
						foto : $('#fotocliente').val(),
						acaoconsulta : acaoconsulta
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						console.log(data);
						//$('#fotocliente').val('');
						if(data!=''){ 
							idp = data; 
							idaltera = '';
						}
						else{ 
							idp = idc;
							idaltera = idconsulta; 
						}
						//alert(idaltera);
						if($('#obsconsulta').val().trim()==''){
							alert('O motivo da consulta deve ser preenchido.');
							$('#obsconsulta').focus();
							$('.mascara2').fadeOut(400);	
						}
						
						if($('#medico').val()!=''&&$('#data').val()!=''&&$('#hini').val()!=''&&$('#hfim').val()!=''&&$('#obsconsulta').val().trim()!=''){
							
							//idp = data;
							//acaoconsulta = 'cadastrar';
							if ($('#nota').is(":checked")) { 
								nota = 1; 
							} else { 
								nota = 0; 
							} 
							$.ajax({
								url : "ajax_core.php",
								data : {
									acao : 'cadastroconsulta',
									acaoconsulta : acaoconsulta,
									idpaciente : idp, 
									idmedico : $('#medico option:selected').val(),
									idsala : $('#sala option:selected').val(),
									idsede : $('#espsede option:selected').val(),
									nomepaciente : $('#paciente').val(), 
									status : $('#status').val(), 
									dataconsulta : $('#data').val(),
									plano_saude :  $('#plano_saude').val(),
									hini : $('#hini').val(), 
									hfim : $('#hfim').val(), 
									parcelamento : $('#parcelamento').val(),
									forma_pagamento : $('#forma_pagamento').val(),
									tipoconsulta : $('#tipoconsulta').val(),
									nota : nota,
									valor : $('#valor').val(),
									dinheiro : $('#dinheiro').val(),
									cartao : $('#cartao').val(),
									valormedico : $('#valormedico').val(),
									valorclinica : $('#valorclinica').val(),
									justificavalor: $('#justificaconsulta').val(),
									idaltera : idaltera,
									obs	: $('#obsconsulta').val(),
									orcamento : orcamento
								},
								type : "POST",
								//dataType:"json",
								success : function(data) {
									if(data==1){
										alert('O horário está conflitando com outra consulta. Verifique.');	
										$('.mascara2').fadeOut(400);
									}else{

										/////////grava procedimentos
										
										$.ajax({
											url : "ajax_core.php",
											data : {
												acao : 'idultimaconsulta',
												idconsulta : idaltera
											},
											type : "POST",
											//dataType:"json",
											success : function(data) {
												idconsulta = data;
												gravaprocedimento(idconsulta);
												if(acaoconsulta == 'cadastrar'){
													whatsconsultamarcada(idconsulta);
												}
											}
										});	
										
										if(tipoag=='salas'){ arquivoag = 'ajax_consultas_salas.php'; }
										else{arquivoag = 'ajax_consultas.php';}
										
										$.ajax({
											url : arquivoag,
											data : {
												dataag : $('#dataag').val(),
												tipousu : $('#tipousu').val(),
												idmedico : $('#espmed').val(),
												idsede : $('#idsede').val()
											},
											type : "POST",
											//dataType:"json",
											success : function(data) {
												//alert('a');
												$('.consultas').html(data);				
											}
										});
										$('.modal,.mascara,.mascara2').fadeOut(400);
										//console.log(data);
										//location.reload();
									}
								}
							});
						}
						
						//console.log(data);
						//$('.modalcliente').fadeOut(400);
									
									
					}
				});
			}
		//},2000);
	});
	
	function gravaprocedimento(idconsulta){
		$('.cadproc').each(function(){
			idproc = $(this).attr('value');
			total = $(this).attr('total');
			dinheiro = $(this).attr('dinheiro');
			cartao = $(this).attr('cartao');
			nota = $(this).attr('nota');
			medico = $(this).attr('medico');
			clinica = $(this).attr('clinica');
			justificativa = $(this).attr('justificativa');
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'gravaprocconsulta',
					idconsulta : idconsulta,
					idproc : idproc,
					total : total,
					dinheiro : dinheiro,
					cartao : cartao,
					nota : nota,
					medico : medico,
					clinica : clinica,
					justificativa : justificativa
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
									
				}
			});
		});
	}
	
	function whatsconsultamarcada(idconsulta){
		///////////////manda whats
			$.ajax({
				url : "testewhats2.php",
				data : {
					idconsulta : idconsulta
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
							
				}
			});		
	}
	
	function verificadiferenca(idc,dataconsulta,idm){

		if($('#medico').val()!=''){
			$.ajax({
				url : "ajax_core.php",
				data : {
					idc : idc,
					idm : idm,
					acao : 'diferencadias',
					dataconsulta : dataconsulta
				},
				type : "POST",
				dataType:"json",
				success : function(data) {
					diasdiff = parseInt(data[0].diferenca);
					status = parseInt(data[0].status);
					ultima = data[0].dataultima;
					tipo = data[0].tipo;
					if(diasdiff < 15 && tipo != 'EXAME'){
						alert('A última consulta deste paciente com este médico foi a menos de 15 dias, portanto essa consulta deverá ser marcada como RETORNO ou EXAME.');
						$('#tipoconsulta').val('RETORNO');
						$('#tipoconsulta').change();
					}
					if(diasdiff > 15){
						alert('A última consulta deste paciente com este médico foi a mais de 15 dias, portanto essa consulta deverá ser marcada como NOVA.');
						$('#tipoconsulta').val('NOVA');
						$('#tipoconsulta').change();
					}
					if(status == 7 && tipo == 'NOVA'){
						alert('ATENÇÃO: A última consulta desse paciente foi marcada como ausência no dia '+ultima+'.');
						$('#tipoconsulta').val('NOVA');
						$('#tipoconsulta').change();
					}
					console.log(data);
				}
			});
		}
	}
	
	$('#medico').change(function(){
		if($('#salvarcliente').attr('idc')!='' && $('#salvarcliente').attr('acaoconsulta')!='alterar'){
			verificadiferenca($('#salvarcliente').attr('idc'),$('#dataag').val(),$('#medico').val());
			//alert('1');
		}
	});
	

	$('#paciente').keyup(function(){
		//console.log($('#paciente').val());
		$('.modalcliente').css({'background-color':'#fff'});
		$('#paciente').attr('idcliente','');
		if($('#paciente').val().length > 4){
		$('.sugestoespac').fadeIn(400);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'completacliente',
					nome : $('#paciente').val()
				},
				type : "POST",
				success : function(data) {
					if(data.trim()!=''){
						$('.sugestoespac').html(data);
					}else{
						$('.sugestoespac').html('Nenhum nome encontrado');
					}
					$('.nomebuscacliente').click(function(){
						$('#paciente').val($(this).html());
						$('#paciente').attr('idcliente',$(this).attr('idc'));
						$('#salvarcliente').attr('acao','alterar');
						$('#salvarcliente').attr('idc',$(this).attr('idc'));
						$('.sugestoespac').fadeOut(400);
						$('.modalcliente').css({'background-color':'#f1f1ff'});
						carregapaciente($(this).attr('idc'));
						verificadiferenca($(this).attr('idc'),$('#dataag').val(),$('#medico').val());
					});						
				}
			});
		}else{
			$('.sugestoespac').fadeOut(400);
		}
	});
	
	$('#paciente').blur(function(){
		$('.sugestoespac').fadeOut(400);
	});
	
	function buscacpf(cpf,tipo){
		
		$('.mascara2').fadeIn('400');
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'buscacpf',
					cpf : cpf,
					tipo : tipo
				},
				dataType:"json",
				type : "POST",
				success : function(data) {
						console.log(data);
						if(data!=''){
							$('#paciente').val(data[0].nome);
							$('#paciente').attr('idcliente',data[0].id);
							$('#salvarcliente').attr('acao','alterar');
							$('#salvarcliente').attr('idc',data[0].id);
							$('.modalcliente').css({'background-color':'#f1f1ff'});
							carregapaciente(data[0].id);
							verificadiferenca(data[0].id,$('#dataag').val(),$('#medico').val());
						}
						$('.mascara2').fadeOut('400');
				}
			});
	}
	
	$('#cpf, #rg').blur(function(){
		if($('#salvarcliente').attr('acao')!='alterar'){
			tipo = $(this).attr('id');
			totalc = 0;
			if(tipo.trim() == 'cpf'){ totalc = 14 }
			if(tipo.trim() == 'rg'){ totalc = 10 }
			//alert(totalc);
			if($(this).val().length == totalc){
				buscacpf($(this).val(),tipo);
			}
		}
	});
	
	$('#dinheiro').keyup(function(){
		valord = parseFloat($(this).val());
		cartao = parseFloat($('#valor').val()) - valord;
		$('#cartao').val(cartao);
	});
	$('#cartao').keyup(function(){
		valorc = parseFloat($(this).val());
		dinheiro = parseFloat($('#valor').val()) - valorc;
		$('#dinheiro').val(dinheiro);
	});
	
	$('#dinheiroproc').keyup(function(){
		valord = parseFloat($(this).val());
		cartao = parseFloat($('#valorproc').val()) - valord;
		$('#cartaoproc').val(cartao);
	});
	$('#cartaoproc').keyup(function(){
		valorc = parseFloat($(this).val());
		dinheiro = parseFloat($('#valorproc').val()) - valorc;
		$('#dinheiroproc').val(dinheiro);
	});
	
	
	function valorconsulta(tipo){
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'valoresconsulta',
					tipo : tipo
				},
				type : "POST",
				dataType:"json",
				success : function(data) {
					$('#valor,#cartao').val(data[0].valor);
					$('#valormedico').val(data[0].medico);
					$('#valorclinica').val(data[0].clinica);
				}
			});
	}
	
	function valorconsultaplano(plano){
		$('.mascara2').fadeIn(400);
		$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'valoresconsultaplano',
					plano : plano
				},
				type : "POST",
				dataType:"json",
				success : function(data) {
					$('#valor,#cartao').val(data[0].honorario);
					$('#valormedico').val(data[0].medico);
					$('#valorclinica').val(data[0].clinica);
					$('.mascara2').fadeOut(400);
				}
			});
	}
		
	
	$('#buscapacientes').keyup(function(){
		if($('#buscapacientes').val().length > 0){
		$('.sugestoes').fadeIn(400);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'listaclientes',
					nome : $('#buscapacientes').val()
				},
				type : "POST",
				success : function(data) {
					$('.listapacientes').html(data);
					
					$('.exibepaciente').click(function(){
						$('.modalcliente input[type=text], .modalcliente input[type=email], .modalcliente input[type=tel], .modalcliente select, #obsconsulta, #orcamentocirurgico').val('');
						$('#fotocliente').hide();
						$('.video1,.video2,.botoesfoto').hide();
						$('.btprontuario,.btreceituario').hide();
						idc = $(this).attr('idc');
						$('#salvarcliente').attr('idc',idc);
						$('#editacliente').val(idc);
						$('#salvarprontuario').attr('idc',idc);
						$('#salvarcliente').attr('acao','alterar');
						
						$('.modalcliente input[type=text], .modalcliente select').val('');
						valorconsulta('NOVA');
						carregapaciente(idc);
						carregarhistorico(idc);
					});						
				}
			});
		}else{
			$('.sugestoes').fadeOut(400);
		}
	});
	
	$('.btvercliente').click(function(){
		carregapaciente($(this).attr('idc'));
	});
	
	$('#plano_saude').change(function(){
		idplano = $(this).val();
		valorconsultaplano(idplano);
	});
	
	
	$('#doencapront').keyup(function(){
		if($('#doencapront').val().length > 0){
		$('.sugestoesdoenca').fadeIn(400);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'completadoenca',
					nome : $('#doencapront').val()
				},
				type : "POST",
				success : function(data) {
					if(data.trim()!=''){
						$('.sugestoesdoenca').html(data);
					}else{
						$('.sugestoesdoenca').fadeOut(400);
					}
					$('.nomedoenca').click(function(){
						$('#doencapront').val($(this).html());
						$('#doencapront').attr('cid',$(this).attr('cid'));
						$('.sugestoesdoenca').fadeOut(400);
					});						
				}
			});
		}else{
			$('.sugestoesdoenca').fadeOut(400);
		}
	});
	
	
	function carregapaciente(idc){
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'carregapaciente',
				idc : idc
			},
			type : "POST",
			dataType:"json",
			success : function(data) {
				//alert(idc);
				//carregarhistorico(idc);
				if(data!=''){
					//console.log('bbbb');
					$('.modalcliente,.mascara').fadeIn(400);
					$('.divfoto').show();
					$('.fotocliente').show();
					//alert('num'+data[0].numero);
					if(data[0].cpf!=''){ $('#cpf').val(data[0].cpf) };
					if(data[0].rg!=''){ $('#rg').val(data[0].rg) };
					if(data[0].nome!=''){ $('#paciente').val(data[0].nome); $('.nomepac').html(data[0].nome) };
					if(data[0].origem!=''){ $('#origemcliente').val(data[0].origem) };
					if(data[0].email!=''){ $('#email').val(data[0].email) };
					if(data[0].cidade!=''){ $('#cidade').val(data[0].cidade) };
					if(data[0].cep!=''){ $('#cep').val(data[0].cep) };
					if(data[0].endereco!=''){ $('#endereco').val(data[0].endereco) };
					if(data[0].bairro!=''){ $('#bairro').val(data[0].bairro) };
					if(data[0].numero!=''){ $('#numeroend').val(data[0].numero) };
					if(data[0].complemento!=''){ $('#complemento').val(data[0].complemento) };
					if(data[0].fotopaciente!=''){ $('#fotocliente').val(data[0].fotopaciente) };
					if(data[0].totalconsultas!=''){ $('.numtotalconsultas').html(data[0].totalconsultas) }
					if(data[0].dataultima!=''&&data[0].dataultima!=null){ 
					//alert(data[0].dataultima);
						$('.diamesult').html(data[0].dataultima.substring(0, 5));
						$('.anocons').html(data[0].dataultima.substring(6,10));
						$('.qultconsulta').show();
						//alert(data[0].dataultima); 
					}else{
						$('.diamesult,.anocons').html('');
						$('.qultconsulta').hide();
					}
					$('.dadospac').fadeIn(400);
					if(data[0].telefone!=''&&data[0].telefone!='0'){ $('#telefone').val(data[0].telefone) };
					if(data[0].telefone2!=''&&data[0].telefone2!='0'){ $('#telefone2').val(data[0].telefone2) };
					if(data[0].data_nascimento!=''){ 
						split = data[0].data_nascimento.split('-');
						datainvert = split[2] + '/' + split[1] + '/' + split[0];
						$('#datanascimento').val(datainvert);
					};
					
					//alert(data[0].foto);
					if(data[0].fotopaciente!=''&&data[0].fotopaciente != null){
						$('.imgpaciente').attr('src' ,'upload/fotos/'+data[0].fotopaciente);
						//alert(data[0].foto);
					}else{
						$('.imgpaciente').attr('src' ,'img/icones/persona_ico.png');
					}
					setTimeout(function(){$('.imgpaciente').fadeIn(400)},1000);
					
				}					
			}
		});
	}
	
	
	$('#salvarorigem').click(function(){
		loader(1);
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'cadastroorigem',
				norigem : $('#cadorigem').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.modal,.mascara').fadeOut(400);
				location.href='?m=3';
				//location.reload();						
			}
		});
	});
	
	$('#salvarplano').click(function(){
		loader(1);
		acaoplano = $(this).val();
		idaltera = '';
		if(acaoplano == 'Alterar'){ idaltera = $(this).attr('idaltera'); }
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'cadastroplanos',
				acaoplano : acaoplano,
				idaltera : idaltera,
				plano : $('#cadplano').val(),
				total : $('#valorplano').val(),
				medico : $('#valormedicoplano').val(),
				clinica : $('#valorclinicaplano').val(),
				honorario : $('#honorarioplano').val(),
				coparticipacao : $('#copartplano').val(),
				coparticipacaoclinica : $('#copartplanoclinica').val(),
				coparticipacaomedico : $('#copartplanomedico').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.modal,.mascara').fadeOut(400);
				//console.log(data);
				location.reload();						
			}
		});
	});
	
	$('#salvarvalorconsulta').click(function(){
		loader(1);
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'alteravalorconsulta',
				novatotal : $('#novatotal').val(),
				novamedico : $('#novamedico').val(),
				novaclinica : $('#novaclinica').val(),
				retornototal : $('#retornototal').val(),
				retornomedico : $('#retornomedico').val(),
				retornoclinica : $('#retornoclinica').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				//console.log(data);
				$('.modal,.mascara').fadeOut(400);
				//$('.modalprocedimentos input').val('');
				location.reload();
				//location.reload();						
			}
		});
	});
	
	$('#salvarprocedimento').click(function(){
		loader(1);
		acaoproc = $(this).attr('acao');
		idproc = $(this).attr('idproc');
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'cadastroprocedimento',
				acaoproc : acaoproc,
				procedimento : $('#procedimento').val(),
				idproc : idproc,
				total : $('#valorproctotal').val(),
				medico : $('#valorprocmedico').val(),
				clinica : $('#valorprocclinica').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				//console.log(data);
				$('.modal,.mascara').fadeOut(400);
				$('.modalprocedimentos input').val('');
				location.href='?m=5';
				//location.reload();						
			}
		});
	});
	
	$('.deletaproc').click(function(){
		idproc = $(this).parent().attr('idproc');
		var r = confirm("Deletar procedimento?");
		if (r == true) {
		loader(1);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'deletaprocedimento',
					idproc : idproc, 
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					location.href='?m=5';						
					//console.log('aaa');
				}
			});
		}
	});
	
	$('.tdprocedimento').click(function(){
		$('#salvarprocedimento').attr('acao','altera');
		$('#salvarprocedimento').attr('idproc',$(this).attr('idproc'));
		$('#procedimento').val($(this).attr('procedimento'));
		$('#valorproctotal,#cartaoproc').val($(this).attr('total'));
		$('#valorprocclinica').val($(this).attr('clinica'));
		$('#valorprocmedico').val($(this).attr('medico'));
	});
	
	$('#salvardoenca').click(function(){
		loader(1);
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'cadastrodoenca',
				doenca : $('#caddoenca').val(),
				cid : $('#cadcid').val()
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.modal,.mascara').fadeOut(400);
				//console.log(data);
				location.reload();						
			}
		});
	});
	
	$('#salvarreceituario').click(function(){
		//alert('a');
		loader(1);

		idc = $('#salvarreceituario').attr('idc');
		tiporec = $('#tiporeceituario').val();
		
		//////////////primeiro ajax salva o receituário passando o tipo e o id do cliente. O PHP deve salvar o médico junto e retornar o ID do prontuário cadastrado.
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'cadastroreceituario',
					idc : idc,
					tiporeceituario : tiporec
				},
				type : "POST",
				async: true,
				//dataType:"json",
				success : function(data) {
					////////////// agora deve percorrer os TR, com o valor de cada input (td)
					idrec = data;
					$('.linharec').each(function(){
						vmed = $(this).children('.recmed').children().val();
						vqtd = $(this).children('.recqtd').children().val();
						vfreq = $(this).children('.recfreq').children().val();
						if(vmed != ''){
							$.ajax({
								url : "ajax_core.php",
								data : {
									acao : 'cadastroitemreceituario',
									idrec : idrec,
									vmed : vmed,
									vqtd : vqtd,
									vfreq : vfreq
								},
								type : "POST",
								//dataType:"json",
								success : function(data) {
									console.log(vmed + ' - ' + vqtd + ' - ' + vfreq);
									if(tiporec==1){
										window.open('receituario.php?idr='+idrec);
									}
									if(tiporec==2){
										window.open('receituario_especial.php?idr='+idrec);
									}
									location.reload();
								}
							});	
						}
					});
					loader(0);
				}
			});
		///////////////////////////
		
	});
	
	$('#modelosrec').change(function(){
		idrec = $(this).val();
		if(idrec != ''){
			$('.mascara2').fadeIn(400);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'buscamodelorec',
					idrec : idrec
				},
				type : "POST",
				dataType:"json",
				success : function(data) {
					for(var i=0; i < data.length; i++) {
						nummed = i+1;
						$('.rec'+nummed).children('.recmed').children().val(data[i].medicamento);
						$('.rec'+nummed).children('.recqtd').children().val(data[i].quantidade);
						$('.rec'+nummed).children('.recfreq').children().val(data[i].frequencia);
					}
					$('.mascara2').fadeOut(400);
				},
				error: function (data) {
					console.log(data);
					$('.mascara2').fadeOut(400);
				}
			});
		}
		
	});
	
	
	$('#salvarmascaraeco').click(function(){
		//alert('a');
		loader(1);

		nomemascara = $('#nomeecografia').val();
		nomemascaraimp = $('#nomeecografiaimp').val();
		txtmascara = $('#textoecografia').val();
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'salvamascaraeco',
				acaomascaraeco : $('#salvarmascaraeco').val(),
				idmascaraeco : $('#salvarmascaraeco').attr('idaltera'),
				nomemascara : nomemascara,
				nomemascaraimp : nomemascaraimp,
				txtmascara : txtmascara
			},
			type : "POST",
			async: true,
			//dataType:"json",
			success : function(data) {
				alert('Máscara Cadastrada');
				$('#nomeecografia,#nomeecografiaimp,#textoecografia').val('');
				//location.href='?m=15';
			}
		});
		///////////////////////////
		
	});
	
	$('.alteramascaraeco').click(function(){
		idm = $(this).attr('id');
		$('#salvarmascaraeco').attr('idaltera',idm);
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'buscamascaraeco',
				idm : idm,
			},
			type : "POST",
			async: true,
			dataType:"json",
			success : function(data) {
				$('#nomeecografia').val(data[0].nome);
				$('#nomeecografiaimp').val(data[0].nomeimpressao);
				$('#textoecografia').val(data[0].texto);
				$('#salvarmascaraeco').val('Alterar');
			}
		});
	});
	
	$('.deletamascaraeco').click(function(){
		idmasc = $(this).attr('id');
		var r = confirm("Deletar máscara?");
		if (r == true) {
		loader(1);
			$.ajax({
				url : "ajax_core.php",
				data : {
					acao : 'deletamascaraeco',
					id : idmasc
				},
				type : "POST",
				//dataType:"json",
				success : function(data) {
					location.href='?m=15';						
					//console.log('aaa');
				}
			});
		}
	});
	
	$('.btnovamascaraeco').click(function(){
		$('#nomeecografia, #nomeecografiaimp').val('');
		$('#textoecografia').val('');
		$('#salvarmascaraeco').val('Salvar');
	});
	
	
	
	$('#medicoplano').change(function(){
		$('.carregaplanosmed').fadeIn(400);
		$('.planos_atendidos').html('');
		$('.checkplano').attr("checked", false);
		idm = $(this).val();
		//alert(idm);
		$.ajax({
			url : "ajax_planos_saude.php",
			data : {
				acao : 'verificarplanos',
				idm : idm
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.checkplano').attr("checked", false);
				$('.planos_atendidos').html(data);
				$('.atendeplano').each(function(){
					$('[idcheck='+$(this).val()+']').attr("checked", true);
				});
			}
		});
	});
	
	$(".checkplano").change(function() {
		idp = $(this).val();
		idm = $('#medicoplano').val();
	  if ($(this).prop("checked") == true) {
			$.ajax({
			url : "ajax_planos_saude.php",
			data : {
				acao : 'addplano',
				idm : idm,
				idp : idp
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				
			}
		});
		
	  }
	  
	  if ($(this).prop("checked") == false) {
		$.ajax({
			url : "ajax_planos_saude.php",
			data : {
				acao : 'removeplano',
				idm : idm,
				idp : idp
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				
			}
		});
	  }
	});
	
	$('#forma_pagamento').change(function(){
		
		if($(this).val()==1){
			$('.mascara2').fadeIn(400);
			idm = $('#medico').val();
			if(idm!=''){
				$.ajax({
					url : "ajax_core.php",
					data : {
						acao : 'listaplanosmed',
						idm : idm
					},
					type : "POST",
					//dataType:"json",
					success : function(data) {
						$('#plano_saude').html(data);
						$('.linhaplanos').fadeIn(400);
						$('.mascara2').fadeOut(400);
						//console.log(data);
					}
				});	
			}
		}else{
			$('.linhaplanos').fadeOut(400);
		}
		$('#plano_saude').val('');
	});
	
	function carregarhistorico(idc){
		//alert(idc);
		$.ajax({
			url : "ajax_historico.php",
			data : {
				acao : 'listarhistorico',
				idc : idc,
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.historico').html(data);
				$('.abreprontuario').click(function(){
					idp = $(this).attr('idp');
					window.open('prontuario.php?idp='+idp);
				});
				$('.abrereceituario').click(function(){
					idr = $(this).attr('idr');
					tiporec = $(this).attr('tipo');
					alert('aaa');
					if(tiporec=='1'){
						window.open('receituario.php?idr='+idr);
					}
					if(tiporec=='2'){
						window.open('receituario_especial.php?idr='+idr);
					}
				});
				$('.abreconsulta').click(function(){
					idc = $(this).attr('idc');
					window.open('consulta.php?idc='+idc);
				});
				$('.abrelaudo').click(function(){
					cod = $(this).attr('cod');
					window.open('laudo_ecografia.php?cod='+cod);
				});
			}
		});
	}
	
	function verificanotificacoes(){
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'verificaalerta',
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.contnotif').html(data);
				if(parseInt($('#numnotificacoes').val()) > 0){
					$('.numnotif').text($('#numnotificacoes').val());
					$('.numnotif').show();
				}else{
					$('.numnotif').hide();
				}
			}
		});
	}
	
	$('.iconotif').click(function(){
		$('.qnotificacoes').fadeToggle(400);
	});
	
	$('#buscatermopront').click(function(){
		$.ajax({
			url : "ajax_core.php",
			data : {
				acao : 'buscatermopront',
				txt : $('#txttermopront').val(),
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.carregabuscapront').html(data);
			}
		});
	});
	
	
	$('.logo').click(function(){
		location.reload();
	});
	
	
	
	//////////ENVIAR FOTO CLIENTE
		$('#inputfoto').change(function(){
			 $('.mascara,.loader').fadeIn(400);
			
			 /* Efetua o Upload */
			 $('#formulariofoto').ajaxForm({

				success:function(data){
					$('#fotocliente').val(data);
					alert('Foto enviada. Será vinculada ao paciente quando a consulta for salva.');
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			 }).submit();
		 });
	
	
	var google = "https://www.google.com/images/errors/logo_sm.gif"; 

    
		function checkconnection() {
			var status = navigator.onLine;
			if (status) {
				alert('Internet connectada!');
				window.open("https://www.minhaconexao.com.br/");
			} else {
				alert('Sem internet!');
			}
		}
		$('[nitem=4]').click(function(){
			checkconnection();
		});
	
	setTimeout(function(){
		$('.topoagenda').width($('.linhahatual').width());
		if($(window).width()<1000){
			$('#diaagenda').css({'font-size':'1em'});	
		}
	},3000);
	
	setInterval(function(){
		$.ajax({
			url : "ajax_verifica_conexao.php",
			data : {
				acao : 'verificaconexao'
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				if(data==1 && $('.avisoconexao').css('right')!='-800px'){
					$('.avisoconexao').animate({'right':'-800px'},600);
				}
			},
			error : function(data) {
				$('.avisoconexao').animate({'right':'-8px'},600);
			}
		});
	},10000);
	
	setInterval(function(){
		verificanotificacoes();
	},60000);
		verificanotificacoes();
	
	
	
});