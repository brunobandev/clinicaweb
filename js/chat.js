// JavaScript Document

$(document).ready(function(){
	$('#medicos_chat').change(function(){
		nomemed = $('#medicos_chat option:selected').text();
		idoutro = $(this).val();
		if(idoutro!=''){
			$('#idoutro').val(idoutro);
			$('.nomemedchat').html(nomemed);
			$('.caixachat').animate({'bottom':'50px'},400);
			carregachat();
		}
	});
	
	$('.fechachat').click(function(){
		$('#idoutro').val('');
		$('.nomemedchat').html('');
		$('.caixachat').animate({'bottom':'-500px'},400);
		$('#medicos_chat').val('');
	});
	
	$('#enviamsg').keypress(function(e) {
			
		idoutro = $('#idoutro').val();
		if(e.which == 13){
			msg = $('#enviamsg').val();
			$('#enviamsg').val('');	
			$.ajax({
			url : "ajax_chat.php",
			data : {
				acao : 'enviarmensagem',
				idoutro : idoutro,
				msg : msg
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				carregachat();
			}
		});	
		}
	});
	
	
	
	function carregachat(){
		idoutro = $('#idoutro').val();
		$.ajax({
			url : "ajax_chat.php",
			data : {
				acao : 'carregachat',
				idoutro : idoutro
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.contchat').html(data);
				$('.contchat').scrollTop(10000);
				
			}
		});
	}
	
	function ativos(){
		$.ajax({
			url : "ajax_chat.php",
			data : {
				acao : 'chatsativos'
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				$('.medicoschat').html(data);
			}
		});
	}
	
	function verificanovamsg(){
		$.ajax({
			url : "ajax_chat.php",
			data : {
				acao : 'verificanovamsg'
			},
			type : "POST",
			//dataType:"json",
			success : function(data) {
				console.log(data);
				$('.novasmensagens').html(data);
				$('#medicos_chat option').css({'background':'#FFF'});
				if(data.trim()!=''){
					$('.novamsg').each(function(){
						idoutro = $(this).val();
						nomeoutro = $(this).attr('nomeoutro');
						balao(nomeoutro);
						$('#medicos_chat [value='+idoutro+']').css({'background':'#15e421'});
						$('#medicos_chat').css({'box-shadow':'0px 0px 6px #15e421'});
					});
				}else{
					$('#medicos_chat').css({'box-shadow':'none'});
					$('#medicos_chat option').css({'background':'#FFF'});
				}
			}
		});
	}
	
	function balao(nome){
		$('.nomenovamsg').html(nome);
		$('.balaochat').fadeIn(400);
		setTimeout(function(){
			$('.balaochat').fadeOut(400);
		},3000);
			
	};
	
	setInterval(function(){
		//console.log('1');
		if($('#idoutro').val()!=''){
			carregachat();
			//ativos();	
		}
		verificanovamsg();
		
	},20000);
	
	
	
	
});
	