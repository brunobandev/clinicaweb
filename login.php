<?php

if (isset($_COOKIE['idusu']) && isset($_COOKIE['tipo'])) {
    header('Location: inicial.php');
    exit;
}

require_once __DIR__.'/bootstrap.php';
use Service\Container;

if (isset($_POST["username"]) &&  isset($_POST["password"])) {
    if (!empty($_POST["username"]) && !empty($_POST["password"])) {
        $container = new Container($configuration);
        $userLoader = $container->getUserLoader();
        $isLogged = $userLoader->login($_POST["username"], $_POST["password"]);

        if ($isLogged) {
            header("Location: inicial.php");
            return;
        }
            
        $message = "Usuário ou senha inválidos!";
        header("Location: login.php?message=" . $message);
        return;
    }
        
    $message = "Usuário e senha não podem estar vazios!";
    header("Location: login.php?message=" . $message);
    return;
}
?>

<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
	<link href="css/estilo.css" rel="stylesheet">

	<title>Clínica da cirurgia geral LTDA - Login</title>

	<style>
		.btnsalva {
			width: 300px;
			position: relative;
			margin-left: -150px;
		}
	</style>
</head>

<body>
	<div id="alerta"></div>
	<div class="linha logologin centro top20"><img src="img/logo.png" /></div>
	<div class="linha titulologin centro top20">Login</div>
	<?php if (isset($_GET["message"]) && !empty($_GET["message"])) : ?>
	<h5 class="centro"><?= $_GET["message"]; ?></h5>
	<?php endif; ?>
	<form action="login.php" method="post">
		<div class="qlogin">
			<div class="linha">
				<div class="label">Usuário</div>
				<div class="input"><input type="text" name="username" id="usuario" class="inputtxt" /></div>
			</div>
			<div class="linha top20">
				<div class="label">Senha</div>
				<div class="input"><input type="password" name="password" id="senha" class="inputtxt" /></div>
			</div>
			<div class="linha top20">
				<div class="input"><input type="submit" class="btnsalva" id="logar" value="Logar" /></div>
			</div>
		</div>
	</form>
</body>

</html>