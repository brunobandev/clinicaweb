<div class="modal modalplanos">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar plano de saúde</div>
        <div class="linha top20"><div class="btnovoplano">Novo plano de saúde</div></div>
        <div class="corpomodal">
        	<div class="linha top20">
            	<div class="div50">
                    <div class="label">Plano de saúde</div>
                    <div class="inputform"><input type="text" id="cadplano" /></div>
                </div>
            </div>
            <div class="linha top20">
            	<div class="div10">
                    <div class="label">Valor Total</div>
                        <div class="inputform"><input type="tel" id="valorplano"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Valor Médico</div>
                        <div class="inputform"><input type="tel" id="valormedicoplano"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Valor Clínica</div>
                        <div class="inputform"><input type="tel" id="valorclinicaplano"></div>
                    </div>
                    
                    <div class="div10">
                        <div class="label">Honorário</div>
                        <div class="inputform">
                            <input type="tel" id="honorarioplano" value="">
                        </div>
                    </div>
                    <div class="div10">
                        <div class="label">Coparticipação</div>
                        <div class="chkjustifica"><input type="tel" id="copartplano" /></div>
                    </div>
                    <div class="div10">
                        <div class="label">Cop. Clínica</div>
                        <div class="chkjustifica"><input type="tel" id="copartplanoclinica" /></div>
                    </div>
                    <div class="div10">
                        <div class="label">Cop. Médico</div>
                        <div class="chkjustifica"><input type="tel" id="copartplanomedico" /></div>
                    </div>
                </div>
            </div>
   		</div>
        <div class="linha top20">
        	<input type="button" id="salvarplano" class="btnsalva" value="Salvar" idaltera="" />
        </div>
        
        <div class="linha top10">
            <table>
                <tr>
                    <td class="cabectabela">Plano</td>
                    <td class="cabectabela">Total</td>
                    <td class="cabectabela">Médico</td>
                    <td class="cabectabela">Clínica</td>
                    <td class="cabectabela">Honorário</td>
                    <td class="cabectabela">Coparticipação</td>
                    <td class="cabectabela">Cop. Clínica</td>
                    <td class="cabectabela">Cop. Médico</td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                    $sql = "SELECT * FROM planos_saude order by plano";
                    //echo $sql;
                    $resultado = mysqli_query($conexao, $sql);
                    while ($res = mysqli_fetch_assoc($resultado)) {
                        $id = $res['id'];
                        $plano = $res['plano'];
                        $total = $res['total'];
                        $medico = $res['medico'];
                        $clinica = $res['clinica'];
                        $honorario = $res['honorario'];
                        $copart = $res['coparticipacao'];
                        $copartclinica = $res['coparticipacaoclinica'];
                        $copartmedico = $res['coparticipacaomedico']; ?>
                        <tr>
                            <td class="tblusu"><?php echo utf8_encode($plano); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($total); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($medico); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($clinica); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($honorario); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($copart); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($copartclinica); ?></td>
                            <td class="tblusu"><?php echo utf8_encode($copartmedico); ?></td>
                            
                            <td class="alteraplano" idplano="<?php echo $id; ?>" plano="<?php echo utf8_encode($plano); ?>" total="<?php echo utf8_encode($total); ?>" medico="<?php echo utf8_encode($medico); ?>" clinica="<?php echo utf8_encode($clinica); ?>" honorario="<?php echo utf8_encode($honorario); ?>" coparticipacao="<?php echo $copart; ?>" coparticipacaoclinica="<?php echo $copartclinica; ?>" coparticipacaomedico="<?php echo $copartmedico; ?>" style="width:20px;">A</td>
                            <td class="deletaplano" idplano="<?php echo $id; ?>" style="width:20px;">X</td>
                        </tr>
                        <?php
                    }
                ?>
            </table>
        </div>
        
    </div>
</div>

<script>
$('.btnovoplano').click(function(){
		$('.modalplanos input').val('');
		$('#salvarplano').val('Salvar');
		$('#salvarplano').attr('idaltera','');
	});
	
	$('.alteraplano').click(function(){
		$('#salvarplano').val('Alterar');
		$('#salvarplano').attr('idaltera',$(this).attr('idplano'));
		$('#cadplano').val($(this).attr('plano'));
		$('#valorplano').val($(this).attr('total'));
		$('#valormedicoplano').val($(this).attr('medico'));
		$('#valorclinicaplano').val($(this).attr('clinica'));
		$('#honorarioplano').val($(this).attr('honorario'));
		$('#copartplano').val($(this).attr('coparticipacao'));
		$('#copartplanoclinica').val($(this).attr('coparticipacaoclinica'));
		$('#copartplanomedico').val($(this).attr('coparticipacaomedico'));
	});
</script>