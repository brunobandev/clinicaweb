<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Clínica da cirurgia geral LTDA - Agenda</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
	<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
	<link href="css/estilo.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.mask.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/agenda.js"></script>


</head>

<body>
	<div class="linha top20 centro"><img src="img/logo.png" /></div>
	<div class="linha top20 centro titulopagina">Consulta</div>
	<div class="foraprontuario top20">
		<?php
        include('conexao.php');
        $idc = $_GET['idc'];
        $sql = "SELECT co.*, l.nome as nomemedico, l.crm, c.nome as nomepaciente, fp.forma_pagamento 
FROM consulta as co 
inner join login as l on co.id_medico = l.id 
inner join clientes as c on co.id_cliente = c.id
inner join formas_pagamento as fp on fp.id = co.forma_pagamento
WHERE co.id = ".$idc;
    $sql = "SELECT co.*, l.nome as nomemedico, l.crm, c.nome as nomepaciente 
	FROM consulta as co 
	inner join login as l on co.id_medico = l.id 
	inner join clientes as c on co.id_cliente = c.id
	
	WHERE co.id = ".$idc;
        $resultado = mysqli_query($conexao, $sql);
        while ($res = mysqli_fetch_assoc($resultado)) {
            $data = substr($res['data'], 8, 2)."/".substr($res['data'], 5, 2)."/".substr($res['data'], 0, 4);
            if ($res['nota_fiscal']==1) {
                $escnota = "Sim";
            }
            if ($res['nota_fiscal']==0) {
                $escnota = "Não";
            } ?>
		<div class="intprontuario">
			<div class="dataprontuário"><?php echo $data . " (" . $res['hini'] . " a " . $res['hfim'] . ")"; ?>
			</div>
			<div class="nomeprontuario">Paciente: <?php echo $res['nome']; ?>
			</div>
			<div class="doencaprontuario top20">Médico: <?php echo $res['nomemedico'] . " (" . $res['crm'] . ")"; ?>
			</div>
			<div class="doencaprontuario top10">Forma de pagamento: <?php //echo utf8_encode($res['forma_pagamento']);?>
			</div>
			<div class="doencaprontuario top10">Parcelamento: <?php echo $res['parcelamento']; ?>x
			</div>
			<div class="doencaprontuario top10">Valor: <?php echo $res['valor']; ?>
			</div>
			<div class="doencaprontuario top10">Nota Fiscal: <?php echo $escnota; ?>
			</div>
			<div class="doencaprontuario top10">Observação: <?php echo $res['obs']; ?>
			</div>
		</div>
		<?php
        } ?>
	</div>
</body>

</html>