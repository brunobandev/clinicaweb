<div class="modal modalcliente" style="padding-bottom: 90px;">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">
        	<div class="nomepac">Paciente</div>
            <?php if($_COOKIE['tipo']=='1' || $_COOKIE['tipo']=='3'){ ?>
        	<div class="btprontuario">Novo prontuário</div>
            <div class="btreceituario">Novo receituário</div>
            <div class="btdelconsulta">Deletar consulta</div>
            <?php } ?>
            <div class="btwhats">Contatar por WhatsApp</div>
        </div>
        <div class="corpomodal">
        	<div class="qdadoscliente">
                <div class="linha top10 fotocliente" style="height:130px;">
                	
                    <div class="dadospac">
                        <div class="qnumpaciente qtotalconsultas">
                            <div class="numtotalconsultas">4</div>
                            <div class="legendanum">Consultas</div>
                        </div>
                        <div class="legendaultimas">Últimas consultas:</div>
                        <div class="ultimascons"></div>
                    </div>
                    
                    <img src="" class="imgpaciente" style="height:130px;" />
                </div>
            
                <div class="linha top10">
                	<?php if($_COOKIE['tipo']=='1'||$_COOKIE['tipo']=='3'){ ?>
                    <div class="div20">
                        <div class="label">CPF</div>
                        <div class="inputform"><input type="text" id="cpf" /></div>
                    </div>
                    <div class="div20">
                        <div class="label">RG</div>
                        <div class="inputform"><input type="text" id="rg" /></div>
                    </div>
                    <?php } ?>
                    <div class="div40">
                        <div class="label">Paciente</div>
                        <div class="inputform"><input type="text" id="paciente" idcliente="" /></div>
                        <div class="sugestoespac"></div>
                    </div>
                
                    <?php if($_COOKIE['tipo']=='1'||$_COOKIE['tipo']=='3'){ ?>
                    <div class="div20">
                        <div class="label">Origem</div>
                        <div class="inputform">
                            <select id="origemcliente">
                                <option value="">Selecione</option>
                                <?php
                                    $sql = "SELECT * FROM clientes_origem ORDER BY origem";
                                    $resultado = mysqli_query($conexao, $sql);
                                    while($res = mysqli_fetch_assoc($resultado)){
                                        $id = $res['id'];
                                        $origem = $res['origem'];
                                        echo "<option value='".$id."'>".$origem."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                
                <?php if($_COOKIE['tipo']=='1'||$_COOKIE['tipo']=='3'){ ?>
                <div class="linha top10">
                    <div class="div25">
                        <div class="label">Data de Nascimento</div>
                        <div class="inputform"><input type="text" id="datanascimento" /></div>
                    </div>
                    <div class="div25">
                        <div class="label">Cel / Whats</div>
                        <div class="inputform"><input type="tel" id="telefone" placeholder="(__)_________" required></div>
                    </div>
                    <div class="div25">
                        <div class="label">Telefone 2</div>
                        <div class="inputform"><input type="tel" id="telefone2" placeholder="(__)_________"></div>
                    </div>
                    <div class="div25">
                        <div class="label">E-mail</div>
                        <div class="inputform"><input type="email" id="email" required/></div>
                    </div>
                </div>
                <div class="linha top10">
                    <div class="div15 top10">
                        <div class="label">Cep</div>
                        <div class="inputform"><input type="text" id="cep" required/></div>
                    </div>
                    <div class="div15 top10">
                        <div class="label">Cidade</div>
                        <div class="inputform"><input type="text" id="cidade" required/></div>
                    </div>
                    <div class="div15 top10">
                        <div class="label">Rua</div>
                        <div class="inputform"><input type="text" id="endereco" required/></div>
                        <div class="opcoescep">
                        	<!--<div class="opcaocep" rua="" cep="">Tomaz Edison, Porto Alegre - 90640-100</div>-->
                        </div>
                    </div>
                    <div class="div10 top10">
                        <div class="label">Número</div>
                        <div class="inputform"><input type="text" id="numeroend" required/></div>
                    </div>
                    <div class="div20 top10">
                        <div class="label">Complemento</div>
                        <div class="inputform"><input type="text" id="complemento" required/></div>
                    </div>
                    <div class="div25 top10">
                        <div class="label">Bairro</div>
                        <div class="inputform"><input type="text" id="bairro" required/></div>
                    </div>
                    
                </div>
                <?php } ?>
            </div>
            
            <?php if($_COOKIE['tipo']=='1'||$_COOKIE['tipo']=='3'){ ?>
            <div class="linha top10">
                <div class="linha divfoto">
                    <div class="linha top10 enviafoto">
                        <div class="btenviar btenviafoto">Enviar foto</div>
                        <form id="formulariofoto" method="post" enctype="multipart/form-data" action="upload/upload_foto.php">
                            <input type="file" id="inputfoto" name="imagem" />
                        </form>
                        <div class="btenviar bttirafoto" id="bttirafoto" style="margin-left:20px;">Ligar câmera</div>
                    </div>
                    <div class="video1"><video id="video" width="320" height="240" autoplay></video></div>
                    <div class="video2"><canvas id="canvas" width="320" height="240"></canvas></div>
                    <div class="botoesfoto">
                        <div id="snap"><img src="img/icones/camera.png" /></div>
                       <div class="legendatirafoto">Tirar foto</div>
                    </div>
                </div>         
            </div>
        </div>
        
        
        
        <!-- CONSULTA -->
        
        <div class="linhasepara"></div>
        <div class="linha top10">
                
                
                <div class="div25">
                    <div class="label">Sala</div>
                    <div class="inputform">
                    	<select id="sala">
                        	
                        </select>
                    </div>
                </div>
                
                <div class="div25">
                    <div class="label">Médico</div>
                    <div class="inputform">
                    	<select id="medico">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$nome = $res['nome'];
									echo "<option value='".$id."'>".$nome."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
                
                <div class="div25">
                	<div class="label">Tipo da consulta</div>
                    <div class="inputform">
                    	<select id="tipoconsulta">
                        	<option value="">Selecione</option>
                        	<option value="NOVA">Nova</option>
                            <option value="RETORNO">Retorno</option>
                            <option value="EXAME">Exame</option>
                        </select>
                    </div>
                </div>
                
                <div class="div25">
                	<div class="label">Status</div>
                    <div class="inputform">
                    	<select id="status">
                        	<?php
								$sql = "SELECT * FROM status_consulta ORDER BY ordem";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$status = $res['status'];
									echo "<option value='".$id."'>".$status."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="linha top10">
            	<div class="div25">
                    <div class="label">Data</div>
                    <div class="inputform"><input type="text" id="data" /></div>
                </div>
                <div class="div25">
                    <div class="label">Início</div>
                    <div class="inputform"><input type="time" id="hini"  min="06:00" max="22:00" required></div>
                </div>
                <div class="div25">
                    <div class="label">Fim</div>
                    <div class="inputform"><input type="time" id="hfim" min="06:00" max="22:00" required/></div>
                </div>
            </div>
            
            <div class="linha top10">
            	<div class="div50">
                    <div class="label">Forma de pagamento</div>
                    <div class="inputform">
                    	<select id="forma_pagamento">
                        	<option value="">Selecione</option>
                        	<?php
                                $sql = "SELECT * FROM formas_pagamento ORDER BY forma_pagamento";
                                $resultado = mysqli_query($conexao, $sql);
                                //echo $sql;
                                while($res = mysqli_fetch_array($resultado)) {
                                    $id = $res['id'];
                                    $forma = $res['forma_pagamento'];
                                    
                                    ?>
                                    <option value="<?php echo $id; ?>"><?php echo $forma; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="linha top10 linhaplanos">
            	<div class="label">Plano de Saúde</div>
                <div class="inputform">
                    <select id="plano_saude">
                        <option value="">Selecione</option>
                        
                    </select>
                </div>
            </div>
            
            <div class="linha top20">
                <div class="linha">
                    <div class="div10">
                        <div class="label">Valor Total</div>
                        <div class="inputform"><input type="text" id="valor" disabled="disabled"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Valor Médico</div>
                        <div class="inputform"><input type="text" id="valormedico" disabled="disabled"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Valor Clínica</div>
                        <div class="inputform"><input type="text" id="valorclinica" disabled="disabled"></div>
                    </div>
                    
                    <div class="div10" style="margin-right:0px;">
                        <div class="label">Em dinheiro</div>
                        <div class="inputform">
                        	<input type="tel" id="dinheiro" value="">
                        </div>
                    </div>
                    <div class="div10" style="width: 40px; margin:0px;">
                        <div class="label">Nota</div>
                        <div class="chkjustifica"><input type="checkbox" id="nota"  value="1" style="width: 20px; height: 20px;" /></div>
                    </div>
                    <div class="div10">
                        <div class="label">No cartão</div>
                        <div class="inputform"><input type="tel" id="cartao"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Parcelado em</div>
                        <div class="inputform"><input type="tel" id="parcelamento" style="width:65px;"> x</div>
                    </div>
                    
                    <div class="div10">
                        <div class="label">Alterar valor</div>
                        <div class="chkjustifica"><input type="checkbox" id="chkjustificaconsulta" style="width: 20px; height: 20px;" /></div>
                    </div>
                    <div class="div25 qjustconsulta">
                        <div class="label">Motivo valor alterado</div>
                        <textarea id="justificaconsulta"></textarea>
                    </div>
                </div>
            </div>            
           
            <div class="qprocedimentos">
                <div class="div25 top20">
                    <div class="label">Procedimentos</div>
                    <div class="inputform">
                        
                        <select id="listaprocedimentos">
                            <option value="">Selecionar procedimento</option>
                            
                            <?php
                                $sql = "SELECT * FROM procedimentos WHERE ativo = 1 ORDER BY procedimento";
                                $resultado = mysqli_query($conexao, $sql);
                                //echo $sql;
                                while($res = mysqli_fetch_array($resultado)) {
                                    $id = $res['id'];
                                    $procedimento = utf8_encode($res['procedimento']);
                                    $total = $res['total'];
                                    $medico = $res['medico'];
                                    $clinica = $res['clinica'];
                                    ?>
                                    <option value="<?php echo $id; ?>"><?php echo $procedimento; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="linhavaloresprocedimentos">
                    <div class="div10">
                    <div class="label">Valor Total</div>
                        <div class="inputform"><input type="text" id="valorproc" disabled="disabled"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Valor Médico</div>
                        <div class="inputform"><input type="text" id="valormedicoproc" disabled="disabled"></div>
                    </div>
                    <div class="div10">
                        <div class="label">Valor Clínica</div>
                        <div class="inputform"><input type="text" id="valorclinicaproc" disabled="disabled"></div>
                    </div>
                    
                    <div class="div10" style="margin-right:0px;">
                        <div class="label">Em dinheiro</div>
                        <div class="inputform">
                        	<input type="tel" id="dinheiroproc" value="">
                        </div>
                    </div>
                    <div class="div10" style="width: 40px; margin:0px;">
                        <div class="label">Nota</div>
                        <div class="chkjustifica"><input type="checkbox" id="notaproc"  value="1" style="width: 20px; height: 20px;" /></div>
                    </div>
                    <div class="div10">
                        <div class="label">No cartão</div>
                        <div class="inputform"><input type="tel" id="cartaoproc"></div>
                    </div>
                    
                    <div class="div10">
                        <div class="label">Alterar valor</div>
                        <div class="chkjustifica"><input type="checkbox" id="chkjustificaproc" style="width: 20px; height: 20px;" /></div>
                    </div>
                    <div class="div25 qjustproc">
                        <div class="label">motivo valor alterado</div>
                        <textarea id="justificaproc"></textarea>
                    </div>
                    <input type="button" id="btaddprocedimento" value="Adicionar Procedimento" />
                </div>
            </div>
            
            <div class="linha procscadastrar"></div>
            
            
            
            
        </div>
        <div class="linha top10">
        	<div class="label">Motivo da consulta</div>
        	<input type="text" id="obsconsulta" value="" >
        </div>
        
        <?php
			$sql = "SELECT permite_orcamento FROM login WHERE id = " . $_COOKIE['idusu'];
			$resultado = mysqli_query($conexao, $sql);
			//echo $sql;
			if($res = mysqli_fetch_array($resultado)) {
        		$permiteorc = $res['permite_orcamento'];
			}
			if($permiteorc==1){
		?>
        <div class="linha top10">
        	<div class="label">Orçamento Cirugico</div>
        	<textarea id="orcamentocirurgico" disabled="disabled"></textarea>
        </div>
        <?php } ?>
       
        <div class="linha top10 linhabtsalvar">
        	<input type="hidden" id="fotocliente" value="" />
            
        	<input type="button" id="salvarcliente" class="btnsalva" value="Salvar" idc="" idconsulta="" acao="cadastrar" acaoconsulta="" />
        </div>
        <?php } ?>
        <div class="linha top10">
        	<div class="historico">
            	
            </div>
        </div>
        
    </div>
</div>

</div>
<script type="text/javascript" src="js/webcam.js?3"></script>
<?php if($_COOKIE['tipo']=='2'||$_COOKIE['tipo']=='4'){ ?>
<script>$('.modalcliente input').attr('disabled','disabled');</script>
<?php } ?>