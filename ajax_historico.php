<?php

include('conexao.php');

if(isset($_POST['acao'])){
	

	
	//session_start();
	if($_POST['acao']=="listarhistorico"){
		$idc = $_POST['idc'];
		if($_COOKIE['tipo']==1||$_COOKIE['tipo']==2){
			?>
			<div class="linha top20">
				<div class="tipohist">Prontuários:</div>
			</div>
			<?php	
			$sql = "SELECT *, p.datacadastro as datacad FROM prontuario as p inner join consulta as c on p.id_consulta = c.id WHERE c.id_cliente = ".$idc." order by p.datacadastro DESC";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$data = substr($res['datacad'],8,2)."/".substr($res['datacad'],5,2)."/".substr($res['datacad'],0,4);
				
				?>
				<div class="listahistorico abreprontuario" idp="<?php echo $idc; ?>">
					<span class='datahist'><?php echo $data; ?></span>
					<span class='itemhist'>Prontuário cadastrado</span>
				</div>
				<?php
			 }
		}
		
		
		if($_COOKIE['tipo']==1||$_COOKIE['tipo']==2){
			?>
			<div class="linha top20">
				<div class="tipohist">Receituários:</div>
			</div>
			<?php		
			$sql = "SELECT * FROM receituarios WHERE id_paciente =".$idc." order by data DESC";
			//echo $sql;
			$resultado = mysqli_query($conexao, $sql);
			while($res = mysqli_fetch_assoc($resultado)){
				$data = substr($res['data'],8,2)."/".substr($res['data'],5,2)."/".substr($res['data'],0,4);
				$escpront = '';
				if($res['tipo']==1){ $escpront = 'Simples'; }
				if($res['tipo']==2){ $escpront = 'Controle Especial'; }
				?>
				<div class="listahistorico abrereceituario" idr="<?php echo $res['id']; ?>" tipo="<?php echo $res['tipo']; ?>">
					<span class='datahist'><?php echo $data; ?></span>
                    <span class='itemhist'>Receituário <?php echo $escpront; ?> cadastrado</span>
				</div>
				<?php
			 }
		}
		 
		 
		 ?>
        <div class="linha top20">
            <div class="tipohist">Consultas:</div>
        </div>
        <?php		
		$sql = "SELECT c.*, s.status as statusconsulta, l.nome as nomemed FROM consulta as c inner join status_consulta as s on c.status = s.id inner join login as l on c.id_medico = l.id WHERE id_cliente = ".$idc." order by data DESC";
		//echo $sql;
		$resultado = mysqli_query($conexao, $sql);
		while($res = mysqli_fetch_assoc($resultado)){
			$data = substr($res['data'],8,2)."/".substr($res['data'],5,2)."/".substr($res['data'],0,4);
			
			?>
            <div class="listahistorico abreconsulta" idc="<?php echo $res['id']; ?>">
            	<span class='datahist'><?php echo $data; ?></span>
                <span class='itemhist'>Consulta (<?php echo $res['statusconsulta']; ?>) - <?php echo $res['nomemed'] . " - " . $res['obs']; ?></span>
            </div>
            <?php
		 }
		 
		 
		 ?>
        <div class="linha top20">
            <div class="tipohist">Laudos de ecografia:</div>
        </div>
        <?php		
		$sql = "SELECT * FROM ecografia_pacientes WHERE id_paciente = ".$idc." order by datacadastro DESC";
		//echo $sql;
		$resultado = mysqli_query($conexao, $sql);
		while($res = mysqli_fetch_assoc($resultado)){
			$id = $res['id'];
			$data = substr($res['datacadastro'],8,2)."/".substr($res['datacadastro'],5,2)."/".substr($res['datacadastro'],0,4);
			$nome = $res['nome'];
			$cod = str_replace('/','',$data) . $id;
			
			?>
            <div class="listahistorico abrelaudo" cod="<?php echo $cod; ?>">
            	<span class='datahist'><?php echo $data; ?></span>
                <span class='itemhist'><?php echo $nome; ?></span>
            </div>
            <?php
		 }
		 
		 
	}
	
	
}

?>