<?php

require __DIR__.'/vendor/autoload.php';

use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use Service\Container;

$configuration = [
    'db_host' => 'mysql:host=clinica-web-mysql;dbname=clinicaweb',
    'db_user' => 'root',
    'db_pass' => 'root'
];

$loader = new FilesystemLoader(__DIR__.'/templates');
$twig = new Environment($loader);
$container = new Container($configuration);
