<div class="modal modalestoque">
	<div class="fecharmodal">X</div>
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar itens de estoque</div>
        <div class="corpomodal">
        	<div class="linha top10">
            	<div class="div25">
                    <div class="label">Item</div>
                    <div class="inputform"><input type="text" id="itemest" /></div>
                </div>
                <div class="div25">
                    <div class="label">Categoria</div>
                    <div class="inputform">
                    	<select id="categoriaitemest">
                        	<option value="">Selecione</option>
                        	<?php
								$sql = "SELECT * FROM estoque_categorias order by categoria";
								$resultado = mysqli_query($conexao, $sql);
								while($res = mysqli_fetch_assoc($resultado)){
									$id = $res['id'];
									$categoria = $res['categoria'];
									echo "<option value='".$id."'>".utf8_encode($categoria)."</option>";
								}
							?>
                        </select>
                    </div>
                </div>
     
            </div>
            
   		</div>
        <div class="linha top10">
        <input type="hidden" id="iditemestaltera" value="" />
        	<input type="button" id="salvaritemest" class="btnsalva" value="Salvar" />
        </div>
        <div class="linha top40">
        	<div class="label">Itens cadastrados</div>
            <div class="linha top20"><div class="btnovoitem">Novo item</div></div>
            <div class="div50 top10">
            	<table>
                	<tr>
                        <td class="cabectabela">Item</td>
                    	<td class="cabectabela">Categoria</td>
                    	<td></td>
                        <td></td>
                    </tr>
                    <?php
						$sql = "SELECT i.*, c.categoria, c.id as idcat FROM estoque_itens as i inner join estoque_categorias as c on i.categoria = c.id order by c.categoria, i.nome";
						$resultado = mysqli_query($conexao, $sql);
						while($res = mysqli_fetch_assoc($resultado)){
							$id = $res['id'];
							$nome = $res['nome'];
							$categoria = $res['categoria'];
							$idcat = $res['idcat'];
							?>
                            <tr>
                            	<td class="tblusu"><?php echo utf8_encode($nome); ?></td>
                                <td class="tblusu"><?php echo utf8_encode($categoria); ?></td>
                                
                                	<td class="alteraitemest" iditem="<?php echo $id; ?>" categoria="<?php echo utf8_encode($idcat); ?>" item="<?php echo utf8_encode($nome); ?>" style="width:20px;">A</td>
                                	<td class="deletaitemest" iditem="<?php echo $id; ?>" style="width:20px;">X</td>
                            </tr>
                            <?php
						}
					?>
                </table>
            </div>
        </div>
    </div>
</div>

