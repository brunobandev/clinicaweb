<?php

require_once __DIR__.'/bootstrap.php';

$healthPlanLoader = $container->getHealthPlanLoader();
$healthPlans = $healthPlanLoader->getAll();

echo $twig->render('health-plans/index.html.twig', ['healthPlans' => $healthPlans]);
