<?php
    date_default_timezone_set("America/Sao_Paulo");
    setlocale(LC_ALL, 'pt_BR');
    include('conexao.php');
    if (isset($_GET['data'])) {
        $data = $_GET['data'];
    } else {
        $data = date('Y-m-d');
    }
    $idconsulta = $_GET['idc'];
    $idpaciente = $_GET['idp'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>Clínica da cirurgia geral LTDA - Pacientes</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script>
setTimeout(function(){
	$('.mascara').fadeOut(400);
},2000);
</script>

<style>
.logofoto{
	position:relative;
	float:left;
	width: 50%;
	margin: 20px 25%;	
}
.logofoto img{
	width: 100%;	
}
.video1,.video2{
 position: relative;
 left: 50%;
 width: 240px;
 margin-left: -120px;
}

.botoesfoto{
    position: absolute;
    bottom: 10px;
    left: 50%;
    top: auto;
    margin-left: -85px;
    z-index: 50;
}

</style>

</head>

<body>

<div class="logofoto"><img src="img/logo.png" /></div>
<div class="tudo">

</div>
<div class="linha top10">
    <div class="linha divfoto">
        <div class="linha top10 enviafoto">
            <div class="btenviar bttirafoto" id="bttirafoto" style="margin-left:20px;">Capturar foto</div>
        </div>
        <div class="linha top10">
        	<form name="upload" enctype="multipart/form-data" method="post" action="fotossalvar_prontuario_env.php">
            	<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
               <input type="file" name="arquivo[]" multiple="multiple" />
               <input type="hidden" name="idconsulta" value="<?php echo $idconsulta; ?>" />
				<input type="hidden" name="idpaciente" value="<?php echo $idpaciente; ?>" />
               <input name="enviar" type="submit" value="Enviar">
            </form>
        </div>
        
            
        <div class="linha top20">
            <div class="video1"><video id="video" width="240" height="320" autoplay></video></div>
            <div class="video2"><canvas id="canvas" width="240" height="320"></canvas></div>
        </div>
        
    </div>         
</div>

<div class="botoesfoto">
    <div id="snap"><img src="img/icones/camera.png" /></div> 
</div>

<script type="text/javascript" src="js/webcam_prontuario.js"></script>
<input type="hidden" id="idconsulta" value="<?php echo $idconsulta; ?>" />
<input type="hidden" id="idpaciente" value="<?php echo $idpaciente; ?>" />
</body>
</html>
