-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05-Fev-2021 às 14:56
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clinica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cep`
--

CREATE TABLE IF NOT EXISTS `cep` (
  `id_cidade` varchar(12) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `rua` varchar(51) DEFAULT NULL,
  `complemento` varchar(62) DEFAULT NULL,
  `bairro` varchar(33) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_med` int(11) NOT NULL,
  `id_sec` int(11) NOT NULL,
  `enviadopor` int(11) NOT NULL,
  `mensagem` varchar(800) NOT NULL,
  `lido` int(11) NOT NULL,
  `datamsg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE IF NOT EXISTS `cidade` (
  `id` int(5) DEFAULT NULL,
  `cidade` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `rg` varchar(15) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `telefone2` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `data_nascimento` date NOT NULL,
  `cidade` varchar(150) NOT NULL,
  `cep` varchar(20) NOT NULL,
  `endereco` varchar(500) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `complemento` varchar(200) NOT NULL,
  `bairro` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  `origem` int(11) NOT NULL,
  `foto` varchar(30) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes_origem`
--

CREATE TABLE IF NOT EXISTS `clientes_origem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origem` varchar(100) NOT NULL,
  `datacadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `consulta`
--

CREATE TABLE IF NOT EXISTS `consulta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `id_sede` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `hini` varchar(5) NOT NULL,
  `hfim` varchar(5) NOT NULL,
  `doenca` int(11) NOT NULL,
  `obs` varchar(1600) NOT NULL,
  `orcamento_cirurgico` varchar(800) NOT NULL,
  `forma_pagamento` int(11) NOT NULL,
  `plano_saude` int(11) NOT NULL,
  `parcelamento` int(11) NOT NULL,
  `nota_fiscal` int(11) NOT NULL,
  `valor` float NOT NULL,
  `valor_dinheiro` float NOT NULL,
  `valor_cartao` float NOT NULL,
  `valor_parcelado` float NOT NULL,
  `valor_clinica` float NOT NULL,
  `valor_medico` float NOT NULL,
  `justificativa` varchar(600) NOT NULL,
  `tipoconsulta` varchar(10) NOT NULL,
  `datacadastro` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `consultas_valores`
--

CREATE TABLE IF NOT EXISTS `consultas_valores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(12) NOT NULL,
  `valor` float NOT NULL,
  `medico` float NOT NULL,
  `clinica` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contato` varchar(800) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `doencas`
--

CREATE TABLE IF NOT EXISTS `doencas` (
  `cid` varchar(6) DEFAULT NULL,
  `doenca` varchar(281) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ecografia_pacientes`
--

CREATE TABLE IF NOT EXISTS `ecografia_pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `datacadastro` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_categorias`
--

CREATE TABLE IF NOT EXISTS `estoque_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_itens`
--

CREATE TABLE IF NOT EXISTS `estoque_itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_itens_quantidade`
--

CREATE TABLE IF NOT EXISTS `estoque_itens_quantidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `sede` int(11) NOT NULL,
  `minimo` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `etapa_cliente_whats`
--

CREATE TABLE IF NOT EXISTS `etapa_cliente_whats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(10) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `formas_pagamento`
--

CREATE TABLE IF NOT EXISTS `formas_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento` varchar(100) NOT NULL,
  `parcelamento` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos_pacientes`
--

CREATE TABLE IF NOT EXISTS `fotos_pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `datacadastro` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos_pacientes_capturadas`
--

CREATE TABLE IF NOT EXISTS `fotos_pacientes_capturadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos_prontuarios`
--

CREATE TABLE IF NOT EXISTS `fotos_prontuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_consulta` int(11) NOT NULL,
  `datacadastro` datetime NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `tipo` int(11) NOT NULL,
  `especialidade` varchar(150) NOT NULL,
  `crm` varchar(10) NOT NULL,
  `permite_orcamento` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mascaras_ecografia_padroes`
--

CREATE TABLE IF NOT EXISTS `mascaras_ecografia_padroes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_medico` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `nomeimpressao` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `datacadastro` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `medicacoes_cronicas`
--

CREATE TABLE IF NOT EXISTS `medicacoes_cronicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `medicamento` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `medicos_planos`
--

CREATE TABLE IF NOT EXISTS `medicos_planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_medico` int(11) NOT NULL,
  `id_plano` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissoes`
--

CREATE TABLE IF NOT EXISTS `permissoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) NOT NULL,
  `login` int(11) NOT NULL,
  `clientes` int(11) NOT NULL,
  `medicos` int(11) NOT NULL,
  `planossaude` int(11) NOT NULL,
  `doencas` int(11) NOT NULL,
  `procedimentos` int(11) NOT NULL,
  `financeiro` int(11) NOT NULL,
  `estoque` int(11) NOT NULL,
  `agenda` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pesquisa_satisfacao`
--

CREATE TABLE IF NOT EXISTS `pesquisa_satisfacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(10) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `nota` int(11) NOT NULL,
  `mensagem` varchar(500) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `planos_saude`
--

CREATE TABLE IF NOT EXISTS `planos_saude` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plano` varchar(140) NOT NULL,
  `total` float NOT NULL,
  `medico` float NOT NULL,
  `clinica` float NOT NULL,
  `honorario` float NOT NULL,
  `coparticipacao` float NOT NULL,
  `coparticipacaoclinica` float NOT NULL,
  `coparticipacaomedico` float NOT NULL,
  `datacadastro` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `procedimentos`
--

CREATE TABLE IF NOT EXISTS `procedimentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedimento` varchar(200) NOT NULL,
  `total` float NOT NULL,
  `medico` float NOT NULL,
  `clinica` float NOT NULL,
  `ativo` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `procedimentos_realizados`
--

CREATE TABLE IF NOT EXISTS `procedimentos_realizados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_consulta` int(11) NOT NULL,
  `id_procedimento` int(11) NOT NULL,
  `total` float NOT NULL,
  `dinheiro` float NOT NULL,
  `cartao` float NOT NULL,
  `nota` int(11) NOT NULL,
  `medico` float NOT NULL,
  `clinica` float NOT NULL,
  `justificativa` varchar(600) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prontuario`
--

CREATE TABLE IF NOT EXISTS `prontuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_consulta` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `motivo` text NOT NULL,
  `cid` varchar(11) NOT NULL,
  `laudo_ecografia` text NOT NULL,
  `subjetivo` text NOT NULL,
  `objetivo` text NOT NULL,
  `exames` text NOT NULL,
  `impressao` text NOT NULL,
  `conduta` text NOT NULL,
  `inicio` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `tempo` varchar(10) NOT NULL,
  `datacadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `receituario`
--

CREATE TABLE IF NOT EXISTS `receituario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `receita` text NOT NULL,
  `tipo` int(11) NOT NULL,
  `datacadastro` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `receituarios`
--

CREATE TABLE IF NOT EXISTS `receituarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `receituarios_itens`
--

CREATE TABLE IF NOT EXISTS `receituarios_itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_receituario` int(11) NOT NULL,
  `medicamento` varchar(150) NOT NULL,
  `quantidade` varchar(50) NOT NULL,
  `frequencia` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `receituarios_padroes`
--

CREATE TABLE IF NOT EXISTS `receituarios_padroes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_receituario` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `salas`
--

CREATE TABLE IF NOT EXISTS `salas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `id_sede` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sala_mes_medico`
--

CREATE TABLE IF NOT EXISTS `sala_mes_medico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ano` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `turno` int(11) NOT NULL,
  `diasemana` int(11) NOT NULL,
  `sala` int(11) NOT NULL,
  `medico` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sala_turno_medico`
--

CREATE TABLE IF NOT EXISTS `sala_turno_medico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sala` int(11) NOT NULL,
  `nomedia` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `diasemana` int(11) NOT NULL,
  `medico` int(11) NOT NULL,
  `turno` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `sede` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sedes`
--

CREATE TABLE IF NOT EXISTS `sedes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `status_consulta`
--

CREATE TABLE IF NOT EXISTS `status_consulta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_usuarios`
--

CREATE TABLE `tipo_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tipo_usuarios` (`id`, `tipo`, `data`) VALUES
(1, 'Admin', now()),
(2, 'Médico', now()),
(3, 'Secretária', now()),
(4, 'Técnico', now());

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
