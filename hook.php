<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

include('conexao.php');

$numero = $_POST['contact_phone_number'];
$nums = str_replace('55','',$numero);
$ddd = substr($nums, 0, 2);
$numsemddd = substr($nums, 2, 15);
if(strlen($numsemddd)==8){ $numsistema = $ddd . '9' . $numsemddd; }
if(strlen($numsemddd)==9){ $numsistema = $ddd . $numsemddd; }
$msg = $_POST['message_body'];

$continua = 1;
//$msg_final = $msg . " - " . $nums;

if(trim($numero)==""){ $continua = 0; }

/////////////////////// VERIFICA SE O CLIENTE JÁ ESTÁ NA BASE. SE NÃO ESTÁ, CANCELA A OPERAÇÃO.
$sql = "SELECT telefone FROM clientes WHERE telefone like '%".$numsemddd."%'";

$resultado = mysqli_query($conexao, $sql);
	if($res = mysqli_fetch_assoc($resultado)){
		
	}else{
		$continua = 0;
	}

/////////////////////// VERIFICA SE O CLIENTE JÁ TEM STATUS WHATS, E QUAL O STATUS, SE NÃO TIVER, ETAPA = 0 E DA O INSERT
if($continua == 1){
	$sql = "SELECT status FROM etapa_cliente_whats WHERE numero like '%".$numsemddd."%'";
	$resultado = mysqli_query($conexao, $sql);
	if($res = mysqli_fetch_assoc($resultado)){
		$etapa = $res['status'];
	}else{
		$etapa = 0;
		$continua = 0;
		$sql2 = "INSERT INTO etapa_cliente_whats (numero, status, data) VALUES ('".$numsistema."','0',Now())";
		$resultado2 = mysqli_query($conexao, $sql2);
	}
}

//////////////////////// SE ETAPA = 1 (CONFIRMAÇÃO DE CONSULTA)

if($etapa == 1 && $continua == 1){
////////////pega última consulta do cliente com status 1
	$sql = "SELECT cons.id, cli.telefone from consulta as cons inner join clientes as cli on cons.id_cliente = cli.id where cli.telefone LIKE '%".$numsemddd."%' order by cons.id DESC LIMIT 1";
	$resultado = mysqli_query($conexao, $sql);
	if($res = mysqli_fetch_assoc($resultado)){
		$idcons = $res['id'];
		$telefone = $res['telefone'];
		$mens = "Desculpe, não entendi sua resposta. Responda apenas digitando os números 1️⃣  (para confirmar sua presença na clínica) ou 2️⃣  (para cancelar seu agendamento).";
		$pos = strpos($msg, '1');
		$pos2 = strpos($msg, 'Sim');
		$pos3 = strpos($msg, 'SIM');
		$pos4 = strpos($msg, 'sim');
		$pos5 = strpos($msg, 'onfirm');
		if ($pos === false && $pos2 === false && $pos3 === false && $pos4 === false && $pos5 === false) {}
		else{
			$sql2 = "UPDATE consulta SET status = 9 WHERE id = ".$idcons;				
			$resultado2 = mysqli_query($conexao, $sql2);
			$mens = "Ok, seu agendamento está confirmado. Aguardamos você na clínica.";
			//$mens = $sql . " - " . $sql2;
		}
		
		$pos6 = strpos($msg, '2');
		$pos7 = strpos($msg, 'Nao');
		$pos8 = strpos($msg, 'NÃO');
		$pos9 = strpos($msg, 'NAO');
		$pos10 = strpos($msg, 'Não');
		$pos10 = strpos($msg, 'nao');
		if ($pos6 === false && $pos7 === false && $pos8 === false && $pos9 === false && $pos10 === false) {}
		else{
			$sql2 = "UPDATE consulta SET status = 8 WHERE id = ".$idcons;				
			$resultado2 = mysqli_query($conexao, $sql2);
			$mens = "Ok, seu agendamento foi cancelado. Para um novo agendamento, entre em contato pelo telefone (51) 3212.2048. |||| Mensagem automática. Não responda. ||||";
		}		
	}
}

///////////////// SE ETAPA = 2, OU SEJA, RESPONDEU A PESQUISA DE SATISFAÇÃO
if($etapa == 2 && $continua == 1){
////////////pega última consulta do cliente com status 4
	$sql = "SELECT cons.id, cli.telefone from consulta as cons inner join clientes as cli on cons.id_cliente = cli.id where cons.status = 4 AND cli.telefone LIKE '%".$numsemddd."%' order by cons.id DESC LIMIT 1";
	$resultado = mysqli_query($conexao, $sql);
	if($res = mysqli_fetch_assoc($resultado)){
		$idcons = $res['id'];
		$telefone = $res['telefone'];
		
		if(intval($msg)>0&&intval($msg)<=10){
			$mens = "Obrigado. O seu retorno é muito importante para a melhoria dos nossos serviços! Agora, se desejar, justifique sua avaliação em uma mensagem para nós.";
			$sql2 = "UPDATE etapa_cliente_whats SET status = 3 WHERE numero like '%".$numsemddd."%'";
			$resultado2 = mysqli_query($conexao, $sql2);
			$sql3 = "INSERT INTO pesquisa_satisfacao (numero, id_consulta, nota, data) VALUES ('".$numsistema."', ".$idcons.",".$msg.",Now())";
			$resultado3 = mysqli_query($conexao, $sql3);
		}
		else{
			$mens = "Desculpa, não entendi a sua resposta. Responda um número de 1 a 10 para avaliar a sua consulta";
		}
	}
}


///////////////// SE ETAPA = 3, OU SEJA, RESPONDEU A PESQUISA DE SATISFAÇÃO QUALITATIVA
if($etapa == 3 && $continua == 1){
////////////pega última consulta do cliente com status 4
	$sql = "SELECT cons.id, cli.telefone from consulta as cons inner join clientes as cli on cons.id_cliente = cli.id where cons.status = 4 AND cli.telefone LIKE '%".$numsemddd."%' order by cons.id DESC LIMIT 1";
	$resultado = mysqli_query($conexao, $sql);
	if($res = mysqli_fetch_assoc($resultado)){
		$idcons = $res['id'];
		$telefone = $res['telefone'];
			$mens = "Recebemos a sua mensagem, a qual será avaliada e, se necessário, entraremos em contato. Obrigado!";
			$sql3 = "INSERT INTO pesquisa_satisfacao (numero, id_consulta, mensagem, data) VALUES ('".$numsistema."', ".$idcons.",'".utf8_decode($msg)."',Now())";
			$resultado3 = mysqli_query($conexao, $sql3);
	}
}

$msg_final = urlencode($mens);


if($continua == 1){
	$curl = curl_init();
		
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://app.whatsgw.com.br/api/WhatsGw/Send",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  //CURLOPT_POSTFIELDS => "apikey=ea635457-be01-4e03-86c9-111cc0c1641f&phone_number=555184491866&contact_phone_number=".$numero."&message_custom_id=1&message_type=text&message_body=".$msg_final,
	  CURLOPT_POSTFIELDS => "apikey=eb92d540-e856-4411-a879-db2c1c0a8b15&phone_number=555199943921&contact_phone_number=55".$telefone."&message_custom_id=1&message_type=text&message_body=".$msg_final,
	  CURLOPT_HTTPHEADER => array(
		"Content-Type: application/x-www-form-urlencoded"
	  ),
	));
	
	$response = curl_exec($curl);
	
	curl_close($curl);
	echo $response;
	
	$sql2 = "INSERT INTO teste (teste) VALUES ('".$_POST['message_body']."')";				
	$resultado2 = mysqli_query($conexao, $sql2);
}
?>