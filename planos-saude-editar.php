<?php

require_once __DIR__.'/bootstrap.php';

$method = $_SERVER['REQUEST_METHOD'];
$healthPlanLoader = $container->getHealthPlanLoader();

switch ($method) {
    case 'GET':
        if (isset($_GET['healthPlanId']) && !empty($_GET['healthPlanId'])) {
            $healthPlanId = $_GET['healthPlanId'];
            $healthPlan = $healthPlanLoader->getById($healthPlanId);
            echo $twig->render('health-plans/edit.html.twig', ['hp' => $healthPlan]);
        }

        header("Location: planos-saude.php");
        break;

    case 'POST':
        $createdHealthPlan = $healthPlanLoader->update($_POST, $_POST['id']);
        header("Location: planos-saude.php");
        break;

    default:
        header("Location: planos-saude.php");
        break;
}
