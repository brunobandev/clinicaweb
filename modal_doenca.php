<div class="modal modaldoenca">
	<div class="topomodal">
    	<div class="linha titulomodal">Cadastrar doença</div>
        <div class="corpomodal">
        	<div class="linha top10">
            	<div class="div50">
                    <div class="label">Doença</div>
                    <div class="inputform"><input type="text" id="caddoenca" /></div>
                </div>
                <div class="div25">
                    <div class="label">CID</div>
                    <div class="inputform"><input type="text" id="cadcid" /></div>
                </div>
            </div>
   		</div>
        <div class="linha top10">
        	<input type="button" id="salvardoenca" class="btnsalva" value="Salvar" />
        </div>
    </div>
</div>