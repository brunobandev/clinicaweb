<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="css/estilo.css?54" rel="stylesheet">
<style>
td{
padding: 5px;
border: 1px solid #eee;	
}
.mascara{
position:fixed;
width:100%;
height: 100%;
background-color:#FFF;
opacity:0.8;
z-index:10;	
display: none;
}
select{
width: 150px;	
}
</style>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
$(document).ready(function(){
	$('.salamed').append($('#option_medicos').html());
	
	
	conta = 0;
	
	function carrega(){
		$('.mascara').show();
		setTimeout(function(){
		$('.salamed').each(function(){
			dia = $(this).attr('dia');
			turno = $(this).attr('turno');
			mes = $('#selmes').val();
			ano = $('#selano').val();
			sala = $('#selsala').val();
			alvo = $(this);
			conta++;
			$.ajax({
				url : "ajax_salas_medicos.php",
				data : {
					acao : 'consultames',
					dia : dia,
					turno : turno,
					mes : mes,
					ano : ano,
					sala : sala
				},
				type : "POST",
				//dataType:"json",
				async:false,
				success : function(data) {
					alvo.val(data);
					$('.mascara').hide();
					console.log(conta);
				}
			});
			
		});
		},500);
	}
	
	$('.salamed').change(function(){
		dia = $(this).attr('dia');
		turno = $(this).attr('turno');
		mes = $('#selmes').val();
			ano = $('#selano').val();
			sala = $('#selsala').val();
		
		medico = $(this).val();
		$.ajax({
			url : "ajax_salas_medicos.php",
			data : {
				acao : 'gravadiames',
				dia : dia,
				turno : turno,
				mes : mes,
				ano : ano,
				sala : sala,
				medico : medico
			},
			type : "POST",
			//dataType:"json",
			async:false,
			success : function(data) {
				console.log(data);
				alert('salvo');
			}
		});
	});
	
	$('#carrega').click(function(){
		carrega();
	});
	
});
</script>

</head>

<body>
<div class="mascara"></div>
<div style="display:none;" id="option_medicos">
	<?php
    include('conexao.php');

	$sql2 = "SELECT * FROM login WHERE tipo = 2 ORDER BY nome";
	$resultado2 = mysqli_query($conexao, $sql2);
	while($res = mysqli_fetch_assoc($resultado2)){
	?>
		<option value="<?php echo $res['id'];?>"><?php echo utf8_encode($res['nome']);?></option>        
    <?php	
	}
	
	?>
</div>

<select id="selsala">
	<option value=""> Sala </option>
	<?php
		$sql2 = "SELECT * FROM salas ORDER BY nome";
		$resultado2 = mysqli_query($conexao, $sql2);
		while($res = mysqli_fetch_assoc($resultado2)){
		?>
			<option value="<?php echo $res['id'];?>"><?php echo utf8_encode($res['nome']);?></option>        
		<?php	
		}
	?>
</select>

<select id="selmes">
	<option value=""> Mês </option>
    <option value="1"> Jan </option>
    <option value="2"> Fev </option>
    <option value="3"> Mar </option>
    <option value="4"> Abr </option>
    <option value="5"> Mai </option>
    <option value="6"> Jun </option>
    <option value="7"> Jul </option>
    <option value="8"> Ago </option>
    <option value="9"> Set </option>
    <option value="10"> Out </option>
    <option value="11"> Nov </option>
    <option value="12"> Dez </option>
</select>

<select id="selano">
	<option value=""> Ano </option>
    <?php 
		$anoat = 2018;
		while($anoat < 2035){
			$anoat++;
	?>
    <option value="<?php echo $anoat; ?>"> <?php echo $anoat; ?> </option>
    <?php } ?>
</select>

<input type="button" id="carrega" value="Carregar" />
    

<table>
<tr>
	<td></td>
	<td>Segunda</td>
    <td>Terça</td>
    <td>Quarta</td>
    <td>Quinta</td>
    <td>Sexta</td>
</tr>

<tr>
	<td>Manhã</td>
	<td><select class="salamed" dia="1" turno="1" nomedia="Mon"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="2" turno="1" nomedia="Tue"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="3" turno="1" nomedia="Wed"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="4" turno="1" nomedia="Thu"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="5" turno="1" nomedia="Fri"><option value="">Selecionar médico</option></select></td>
</tr>

<tr>
	<td>Tarde</td>
	<td><select class="salamed" dia="1" turno="2" nomedia="Mon"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="2" turno="2" nomedia="Tue"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="3" turno="2" nomedia="Wed"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="4" turno="2" nomedia="Thu"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="5" turno="2" nomedia="Fri"><option value="">Selecionar médico</option></select></td>
</tr>

<tr>
	<td>Noite</td>
	<td><select class="salamed" dia="1" turno="3" nomedia="Mon"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="2" turno="3" nomedia="Tue"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="3" turno="3" nomedia="Wed"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="4" turno="3" nomedia="Thu"><option value="">Selecionar médico</option></select></td>
    <td><select class="salamed" dia="5" turno="3" nomedia="Fri"><option value="">Selecionar médico</option></select></td>
</tr>


</table>

<?php



$mes = 1;
$ano = 2020;
$sala = 4;

$dia = 0;
$conta = 0;


		/*$numero = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		
		
			while($dia < $numero){
				if($ano < 2031){
					$turno = 0;
					$dia++;
					$data = $dia."-".$mes."-".$ano;
					$nomedia = date('D', strtotime($data));
					if($nomedia!='Sun'&&$nomedia!='Sat'){
						while($turno<3){
							$conta++;
							$turno++;
							if($dia < 10){ $escdia = '0'.$dia; }
							else{ $escdia = $dia; }
							//echo $conta . ") ". $escdia."-".$mes."-".$ano." (".$nomedia.") - Turno:".$turno."<br>";
							$diacont = $ano."-".$mes."-".$escdia;
							
							$sql2 = "INSERT INTO sala_turno_medico (data,sala,turno,medico,nomedia) VALUES ('".$diacont."',".$sala.",'".$turno."','','".$nomedia."')";	
							echo $sql2;		
							$resultado2 = mysqli_query($conexao, $sql2);
							
						}
					}
					if($turno==3){ $turno = 0; }
					if($dia==$numero){
						$dia = 0;
						if($mes==12){ $mes = 1; $ano++; }
						else{ $mes++; }
					}
				}
			}
		
		//}*/
?>

<!--
id
sala
data
nomedia
turno
medico

Alterar uma data específica da sala específica

DELETE FROM sala_turno_medico WHERE data = '2019-23-12' AND sala = '1' AND turno = 1
 - descobrir nomedia
INSERT INTO sala_turno_medico (data,sala,turno,medico,nomedia), ('2019-23-12',1,'Nome médico','Nome Dia')

update tabela SET medico = 'Mateus' WHERE nomedia = 'Mon' AND (MONTH(data)==1 AND YEAR(data)==2020) AND turno = 1
-->

</body>
</html>