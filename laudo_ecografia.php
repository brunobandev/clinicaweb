<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
$idl = $_GET['cod'];
		$idl = substr($idl, 8,10);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clínica da cirurgia geral LTDA - Laudo de Ecografia Nº <?php echo $_GET['cod']; ?></title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link href="js/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
<link href="css/estilo.css" rel="stylesheet">
<link href="css/imgbox.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/qrcode.js"></script>
<script>
	$(document).ready(function(){
		setTimeout(function(){
			
			var qrcode = new QRCode(document.getElementById("qrcode"), {
				width : 90,
				height : 90
			});
			
			function makeCode () {		
				qrcode.makeCode('https://www.systemcdc.com.br/laudo_ecografia.php?cod='+$('#codlaudo').val());;
			}
			makeCode();
				
				
			altdoc = $('html').height();
			pg = 1122;
			console.log(altdoc);
			
			coef = altdoc / pg;
			console.log(coef);
			if(coef <= 1){ coef = 1; alt = 28.7; }
			if(coef > 1 && coef <= 2){ coef = 2; }
			if(coef > 2 && coef <= 3){ coef = 3; }
			if(coef > 3 && coef <= 4){ coef = 4; }
			if(coef > 4 && coef <= 5){ coef = 5; }
			if(coef > 5 && coef <= 6){ coef = 6; }
			if(coef > 6 && coef <= 7){ coef = 7; }
			if(coef > 7 && coef <= 8){ coef = 8; }
			if(coef > 8 && coef <= 9){ coef = 9; }
			if(coef > 9 && coef <= 10){ coef = 10; }
			if(coef > 10 && coef <= 11){ coef = 11; }
			if(coef > 1 ){ alt = ((coef - 1) * 29.7) + 28.7; }
			$('.enderecoclinicalaudoeco').css({'top':alt+'cm'});
		
			/////////////////INFO LATERAL
			totalpag = coef;
			contapag = 0;
			while(contapag<totalpag){
				console.log(contapag);
				if(contapag == 0){ altlat = 10; }
				if(contapag > 0){ 
					altlat = 10 + (29.7*(contapag)); 
				}
				nomelateral = "<div class='nomelateral' style='top:"+altlat+"cm;'>Laudo Nº "+$('#codlaudo').val() + " - " + $('.nomepacientelaudo').text()+" | P."+(contapag+1)+"</div>";
				$('body').append(nomelateral);
				
				contapag++;	
			}
		
		
		},2000);
		
		
	});
</script>


<style>

@media print{
	@page {margin: 0.8cm;}
  body { margin: 30px 0px; }
  .cabeclaudoeco{
		margin-top: 0.2cm;  
  }
  .linhasalvapadrao, #imprimerec{ display: none; }
  
	}
	.enderecoclinicalaudoeco{
	     position: absolute;
    width: 100%;
    top: 1020px; 
  }

body { margin: 30px 0px; }
.cabeclaudoeco{
		margin: 1.4cm;  
		margin-top: 0.2cm;
  }

.cabeclaudoeco{
	position:absolute;	
	text-transform:uppercase;
}
.idlaudo{
	color:#999;	
}
.txtlaudoeco{
	    position: relative;
    float: left;
    width: 600px;
    left: 50%;
    margin-left: -300px;
    text-align: justify;
}
.logolaudoeco{
	margin-top: 170px;
}
#qrcode{
	position:absolute;
	right: 20px;
	top:20px;	
}
#idlaudo{
	position: absolute;
    right: 20px;
    top: 113px;
    font-size: 0.9em;
    color: #444;
	text-align:center;
	width: 90px;	
}
.nomelateral{
	position: absolute;
    width: 9.7%;
    top: 612px;
	right: -50px;
    height: 612px;
    overflow: hidden;
    writing-mode: vertical-lr;
    text-align: center;
    font-size: 0.9em;
    color: #8e8e8e;	
}
.paginacaolaudo{
	position:absolute;
	right: 20px;
	padding: 6px;
	background-color:#CCC;	
}
</style>

</head>

<body>
<?php
		include('conexao.php');
		
		$sql = "SELECT e.*, c.nome as nome_paciente, c.data_nascimento, l.nome as nome_medico, l.especialidade, l.crm FROM ecografia_pacientes as e inner join clientes as c on e.id_paciente = c.id inner join login as l on l.id = e.id_medico WHERE e.id = " . $idl;
		//echo $sql;
		$resultado = mysqli_query($conexao, $sql);
		if($res = mysqli_fetch_assoc($resultado)){
			$data = substr($res['datacadastro'],8,2)."/".substr($res['datacadastro'],5,2)."/".substr($res['datacadastro'],0,4);
			$datanascimento = substr($res['data_nascimento'],8,2)."/".substr($res['data_nascimento'],5,2)."/".substr($res['data_nascimento'],0,4);
			$codlaudo = str_replace('/','',$data) . $idl;
			$horario = substr($res['datacadastro'],11,5);
			$nomelaudo = $res['nome'];
			$nomepaciente = $res['nome_paciente'];
			$nomemedico = $res['nome_medico'];
			$especialidade = $res['especialidade'];
			$texto = $res['texto'];
			$idmed = $res['id_medico'];
			$crm = $res['crm'];
			?>
            <div id="qrcode"></div>
            <div id="idlaudo">Laudo nº<br /><?php echo $codlaudo; ?></div>
            <div class="cabeclaudoeco">
            	
            	<div class="linha idlaudo"><?php echo $nomelaudo; ?></div>
                
                <div class="linha nomepacientelaudo" style="font-weight:600;"><?php echo $nomepaciente; ?></div>
                <div class="linha">Data de nascimento: <?php echo $datanascimento; ?></div>
                <div class="linha"><?php echo $data . " - " . $horario; ?></div>
            </div>
            <div class="linha top40 centro logolaudoeco"><img src="img/logo.png" /></div>
            <div class="linha top10 centro"><?php echo $nomelaudo; ?></div>
            <div class="top30 txtlaudoeco"><?php echo $texto; ?></div>
            
            <div class="linha centro top20 blocoass">
            	<div class="linha assinaturalaudo"><img src="upload/assinaturas/<?php echo $idmed; ?>.jpg" style="width: 250px;" /></div>
                <div class="linha centro">Dr. <?php echo $nomemedico; ?></div>
                <div class="linha centro">CRM <?php echo $crm; ?></div>
                <div class="linha centro"><?php echo $especialidade; ?></div>
            </div>
            
            
            <?php
		 }
?>
	
	<div class="linha enderecoclinicalaudoeco centro">Clínica Mayo Surgery, Praça XV de Novembro, 21 / 902, Centro Histórico, Porto Alegre, RS<br />CEP : 90020-080, CNPJ : 35.399.441/0001-92 
, Telefone: (51) 32122048</div>
        </div>
		<input type="hidden" id="codlaudo" value="<?php echo $codlaudo; ?>" />
</body>
</html>